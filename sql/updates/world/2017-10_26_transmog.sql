CREATE TABLE IF NOT EXISTS `transmogvendor_data` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Auto generated unique id',
  `ItemId` int(11) NOT NULL COMMENT 'Item Id',
  `SlotId` tinyint(3) unsigned NOT NULL COMMENT 'Slot Id for the given item',
  `Comment` varchar(500) NOT NULL DEFAULT '' COMMENT 'Use this field to leave a comment for other developers',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
