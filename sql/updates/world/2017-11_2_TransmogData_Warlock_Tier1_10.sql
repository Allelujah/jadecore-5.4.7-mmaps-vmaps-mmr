-- **TIER 1 WARLOCK**
DELETE FROM `transmogvendor_data` WHERE `ItemId` IN (16804, 16805, 16806, 16807, 16808, 16809);
INSERT INTO `transmogvendor_data` (`ItemId`, `SlotId`, `Comment`) VALUES (16808, 1, 'Felheart Horns');
INSERT INTO `transmogvendor_data` (`ItemId`, `SlotId`, `Comment`) VALUES (16806, 6, 'Felheart Belt');
INSERT INTO `transmogvendor_data` (`ItemId`, `SlotId`, `Comment`) VALUES (16807, 3, 'Felheart Shoulder Pads');
INSERT INTO `transmogvendor_data` (`ItemId`, `SlotId`, `Comment`) VALUES (16805, 10, 'Felheart Gloves');
INSERT INTO `transmogvendor_data` (`ItemId`, `SlotId`, `Comment`) VALUES (16804, 9, 'Felheart Bracers');
INSERT INTO `transmogvendor_data` (`ItemId`, `SlotId`, `Comment`) VALUES (16809, 5, 'Felheart Robes');


-- **TIER 2 WARLOCK**
DELETE FROM `transmogvendor_data` WHERE `ItemId` IN (16933, 16927, 16934, 16930, 16931, 16929, 16932, 16928);
INSERT INTO `transmogvendor_data` (`ItemId`, `SlotId`, `Comment`) VALUES (16933, 6, 'Nemesis Belt');
INSERT INTO `transmogvendor_data` (`ItemId`, `SlotId`, `Comment`) VALUES (16927, 8, 'Nemesis Boots');
INSERT INTO `transmogvendor_data` (`ItemId`, `SlotId`, `Comment`) VALUES (16934, 9, 'Nemesis Bracers');
INSERT INTO `transmogvendor_data` (`ItemId`, `SlotId`, `Comment`) VALUES (16930, 7, 'Nemesis Leggings');
INSERT INTO `transmogvendor_data` (`ItemId`, `SlotId`, `Comment`) VALUES (16931, 5, 'Nemesis Robes');
INSERT INTO `transmogvendor_data` (`ItemId`, `SlotId`, `Comment`) VALUES (16929, 1, 'Nemesis Skullcap');
INSERT INTO `transmogvendor_data` (`ItemId`, `SlotId`, `Comment`) VALUES (16932, 3, 'Nemesis Spaulders');
INSERT INTO `transmogvendor_data` (`ItemId`, `SlotId`, `Comment`) VALUES (16928, 10, 'Nemesis Gloves');



-- **TIER 3 WARLOCK**
DELETE FROM `transmogvendor_data` WHERE `ItemId` IN (22510, 22511, 22506, 22509, 22505, 22504, 22508, 22507);
INSERT INTO `transmogvendor_data` (`ItemId`, `SlotId`, `Comment`) VALUES (22510, 6, 'Plagueheart Belt');
INSERT INTO `transmogvendor_data` (`ItemId`, `SlotId`, `Comment`) VALUES (22511, 9, 'Plagueheart Bindings');
INSERT INTO `transmogvendor_data` (`ItemId`, `SlotId`, `Comment`) VALUES (22506, 1, 'Plagueheart Circlet');
INSERT INTO `transmogvendor_data` (`ItemId`, `SlotId`, `Comment`) VALUES (22509, 10, 'Plagueheart Gloves');
INSERT INTO `transmogvendor_data` (`ItemId`, `SlotId`, `Comment`) VALUES (22505, 7, 'Plagueheart Leggings');
INSERT INTO `transmogvendor_data` (`ItemId`, `SlotId`, `Comment`) VALUES (22504, 5, 'Plagueheart Robes');
INSERT INTO `transmogvendor_data` (`ItemId`, `SlotId`, `Comment`) VALUES (22508, 8, 'Plagueheart Sandals');
INSERT INTO `transmogvendor_data` (`ItemId`, `SlotId`, `Comment`) VALUES (22507, 3, 'Plagueheart Shoulderpads');



-- **TIER 4 WARLOCK**
DELETE FROM `transmogvendor_data` WHERE `ItemId` IN (28963, 28968, 28966, 28967, 28964);
INSERT INTO `transmogvendor_data` (`ItemId`, `SlotId`, `Comment`) VALUES (28963, 1, 'Voidheart Crown');
INSERT INTO `transmogvendor_data` (`ItemId`, `SlotId`, `Comment`) VALUES (28968, 10, 'Voidheart Gloves');
INSERT INTO `transmogvendor_data` (`ItemId`, `SlotId`, `Comment`) VALUES (28966, 7, 'Voidheart Leggings');
INSERT INTO `transmogvendor_data` (`ItemId`, `SlotId`, `Comment`) VALUES (28967, 3, 'Voidheart Mantle');
INSERT INTO `transmogvendor_data` (`ItemId`, `SlotId`, `Comment`) VALUES (28964, 5, 'Voidheart Robes');



-- **TIER 5 WARLOCK**
DELETE FROM `transmogvendor_data` WHERE `ItemId` IN (30050, 30064, 30211, 30212, 30213, 30214, 30215);
INSERT INTO `transmogvendor_data` (`ItemId`, `SlotId`, `Comment`) VALUES (30050, 8, 'Boots of the Shifting Nightmare');
INSERT INTO `transmogvendor_data` (`ItemId`, `SlotId`, `Comment`) VALUES (30064, 6, 'Cord of Screaming Terrors');
INSERT INTO `transmogvendor_data` (`ItemId`, `SlotId`, `Comment`) VALUES (30211, 10, 'Gloves of the Corruptor');
INSERT INTO `transmogvendor_data` (`ItemId`, `SlotId`, `Comment`) VALUES (30212, 1, 'Hood of the Corruptor');
INSERT INTO `transmogvendor_data` (`ItemId`, `SlotId`, `Comment`) VALUES (30213, 7, 'Leggings of the Corruptor');
INSERT INTO `transmogvendor_data` (`ItemId`, `SlotId`, `Comment`) VALUES (30214, 5, 'Robe of the Corruptor');
INSERT INTO `transmogvendor_data` (`ItemId`, `SlotId`, `Comment`) VALUES (30215, 3, 'Mantle of the Corruptor');



-- **TIER 6 WARLOCK**
DELETE FROM `transmogvendor_data` WHERE `ItemId` IN (31050, 31051, 31053, 31054, 31052, 34564, 34436, 34541);
INSERT INTO `transmogvendor_data` (`ItemId`, `SlotId`, `Comment`) VALUES (31050, 10, 'Gloves of the Malefic');
INSERT INTO `transmogvendor_data` (`ItemId`, `SlotId`, `Comment`) VALUES (31051, 1, 'Hood of the Malefic');
INSERT INTO `transmogvendor_data` (`ItemId`, `SlotId`, `Comment`) VALUES (31053, 7, 'Leggings of the Malefic');
INSERT INTO `transmogvendor_data` (`ItemId`, `SlotId`, `Comment`) VALUES (31054, 3, 'Mantle of the Malefic');
INSERT INTO `transmogvendor_data` (`ItemId`, `SlotId`, `Comment`) VALUES (31052, 5, 'Robe of the Malefic');
INSERT INTO `transmogvendor_data` (`ItemId`, `SlotId`, `Comment`) VALUES (34564, 8, 'Boots of the Malefic');
INSERT INTO `transmogvendor_data` (`ItemId`, `SlotId`, `Comment`) VALUES (34436, 9, 'Bracers of the Malefic');
INSERT INTO `transmogvendor_data` (`ItemId`, `SlotId`, `Comment`) VALUES (34541, 6, 'Belt of the Malefic');



-- **TIER 7 WARLOCK(HEROES)**
DELETE FROM `transmogvendor_data` WHERE `ItemId` IN (39496, 39497, 39498, 39499, 39500);
INSERT INTO `transmogvendor_data` (`ItemId`, `SlotId`, `Comment`) VALUES (39496, 1, 'Heroes Plagueheart Circlet');
INSERT INTO `transmogvendor_data` (`ItemId`, `SlotId`, `Comment`) VALUES (39497, 5, 'Heroes Plagueheart Robes');
INSERT INTO `transmogvendor_data` (`ItemId`, `SlotId`, `Comment`) VALUES (39498, 7, 'Heroes Plagueheart Leggings');
INSERT INTO `transmogvendor_data` (`ItemId`, `SlotId`, `Comment`) VALUES (39499, 3, 'Heroes Plagueheart Shoulderpads');
INSERT INTO `transmogvendor_data` (`ItemId`, `SlotId`, `Comment`) VALUES (39500, 10, 'Heroes Plagueheart Gloves');



-- **TIER 7 WARLOCK (VALOROUS)**
DELETE FROM `transmogvendor_data` WHERE `ItemId` IN (40421, 40423, 40422, 40424, 40420);
INSERT INTO `transmogvendor_data` (`ItemId`, `SlotId`, `Comment`) VALUES (40421, 1, 'Valorous Plagueheart Circlet');
INSERT INTO `transmogvendor_data` (`ItemId`, `SlotId`, `Comment`) VALUES (40423, 5, 'Valorous Plagueheart Robes');
INSERT INTO `transmogvendor_data` (`ItemId`, `SlotId`, `Comment`) VALUES (40422, 7, 'Valorous Plagueheart Leggings');
INSERT INTO `transmogvendor_data` (`ItemId`, `SlotId`, `Comment`) VALUES (40424, 3, 'Valorous Plagueheart Shoulderpads');
INSERT INTO `transmogvendor_data` (`ItemId`, `SlotId`, `Comment`) VALUES (40420, 10, 'Valorous Plagueheart Gloves');



-- **TIER 8 WARLOCK(VALOROUS)**
DELETE FROM `transmogvendor_data` WHERE `ItemId` IN (45417, 45421, 45420, 45422, 45419);
INSERT INTO `transmogvendor_data` (`ItemId`, `SlotId`, `Comment`) VALUES (45417, 1, 'Valorous Deathbringer Hood');
INSERT INTO `transmogvendor_data` (`ItemId`, `SlotId`, `Comment`) VALUES (45421, 5, 'Valorous Deathbringer Robes');
INSERT INTO `transmogvendor_data` (`ItemId`, `SlotId`, `Comment`) VALUES (45420, 7, 'Valorous Deathbringer Leggings');
INSERT INTO `transmogvendor_data` (`ItemId`, `SlotId`, `Comment`) VALUES (45422, 3, 'Valorous Deathbringer Shoulderpads');
INSERT INTO `transmogvendor_data` (`ItemId`, `SlotId`, `Comment`) VALUES (45419, 10, 'Valorous Deathbringer Gloves');



-- **TIER 8 WARLOCK (CONQUEROR)**
DELETE FROM `transmogvendor_data` WHERE `ItemId` IN (46140, 46137, 46139, 46136, 46135);
INSERT INTO `transmogvendor_data` (`ItemId`, `SlotId`, `Comment`) VALUES (46140, 1, 'Conquerors Deathbringer Hood');
INSERT INTO `transmogvendor_data` (`ItemId`, `SlotId`, `Comment`) VALUES (46137, 5, 'Conquerors Deathbringer Robes');
INSERT INTO `transmogvendor_data` (`ItemId`, `SlotId`, `Comment`) VALUES (46139, 7, 'Conquerors Deathbringer Leggings');
INSERT INTO `transmogvendor_data` (`ItemId`, `SlotId`, `Comment`) VALUES (46136, 3, 'Conquerors Deathbringer Shoulderpads');
INSERT INTO `transmogvendor_data` (`ItemId`, `SlotId`, `Comment`) VALUES (46135, 10, 'Conquerors Deathbringer Gloves');



-- **TIER 9 WARLOCK (TRIUMPH)**
DELETE FROM `transmogvendor_data` WHERE `ItemId` IN (47778, 47779, 47780, 47781, 47782);
INSERT INTO `transmogvendor_data` (`ItemId`, `SlotId`, `Comment`) VALUES (47778, 1, 'Kel\'Thuzad\'s hood of Triumph');
INSERT INTO `transmogvendor_data` (`ItemId`, `SlotId`, `Comment`) VALUES (47779, 5, 'Kel\'Thuzad\'s Robe of Triumph');
INSERT INTO `transmogvendor_data` (`ItemId`, `SlotId`, `Comment`) VALUES (47780, 7, 'Kel\'Thuzad\'s Leggings of Triumph');
INSERT INTO `transmogvendor_data` (`ItemId`, `SlotId`, `Comment`) VALUES (47781, 3, 'Kel\'Thuzads\'s Shoulderpads of Triumph');
INSERT INTO `transmogvendor_data` (`ItemId`, `SlotId`, `Comment`) VALUES (47782, 10, 'Kel\'Thuzads\'s Gloves of Triumph');



-- **TIER 9 WARLOCK (CONQUEST)**
DELETE FROM `transmogvendor_data` WHERE `ItemId` IN (47784, 47786, 47785, 47787, 47783);
INSERT INTO `transmogvendor_data` (`ItemId`, `SlotId`, `Comment`) VALUES (47784, 1, 'Kel\'Thuzad\'s hood of Conquest');
INSERT INTO `transmogvendor_data` (`ItemId`, `SlotId`, `Comment`) VALUES (47786, 5, 'Kel\'Thuzad\'s Robe of Conquest');
INSERT INTO `transmogvendor_data` (`ItemId`, `SlotId`, `Comment`) VALUES (47785, 7, 'Kel\'Thuzad\'s Leggings of Conquest');
INSERT INTO `transmogvendor_data` (`ItemId`, `SlotId`, `Comment`) VALUES (47787, 3, 'Kel\'Thuzads\'s Shoulderpads of Conquest');
INSERT INTO `transmogvendor_data` (`ItemId`, `SlotId`, `Comment`) VALUES (47783, 10, 'Kel\'Thuzads\'s Gloves of Conquest');



-- **TIER 10 WARLOCK(NORMAL)**
DELETE FROM `transmogvendor_data` WHERE `ItemId` IN (60249, 60251, 60250, 60252, 60248);
INSERT INTO `transmogvendor_data` (`ItemId`, `SlotId`, `Comment`) VALUES (60249, 1, 'Shadowflame Hood');
INSERT INTO `transmogvendor_data` (`ItemId`, `SlotId`, `Comment`) VALUES (60251, 5, 'Shadowflame Robes');
INSERT INTO `transmogvendor_data` (`ItemId`, `SlotId`, `Comment`) VALUES (60250, 7, 'Shadowflame Leggings');
INSERT INTO `transmogvendor_data` (`ItemId`, `SlotId`, `Comment`) VALUES (60252, 3, 'Shadowflame Mantle');
INSERT INTO `transmogvendor_data` (`ItemId`, `SlotId`, `Comment`) VALUES (60248, 10, 'Shadowflame Handwraps');



-- TIER 10 WARLOCK (HEROIC)**
DELETE FROM `transmogvendor_data` WHERE `ItemId` IN (65260, 65262, 65261, 65263, 65259);
INSERT INTO `transmogvendor_data` (`ItemId`, `SlotId`, `Comment`) VALUES (65260, 1, 'Shadowflame Hood');
INSERT INTO `transmogvendor_data` (`ItemId`, `SlotId`, `Comment`) VALUES (65262, 5, 'Shadowflame Robes');
INSERT INTO `transmogvendor_data` (`ItemId`, `SlotId`, `Comment`) VALUES (65261, 7, 'Shadowflame Leggings');
INSERT INTO `transmogvendor_data` (`ItemId`, `SlotId`, `Comment`) VALUES (65263, 3, 'Shadowflame Mantle');
INSERT INTO `transmogvendor_data` (`ItemId`, `SlotId`, `Comment`) VALUES (65259, 10, 'Shadowflame Handwraps');
