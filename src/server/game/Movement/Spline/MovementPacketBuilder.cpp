/*
* Copyright (C) 2011-2014 Project SkyFire <http://www.projectskyfire.org/>
* Copyright (C) 2008-2014 TrinityCore <http://www.trinitycore.org/>
* Copyright (C) 2005-2014 MaNGOS <http://getmangos.com/>
*
* This program is free software; you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation; either version 3 of the License, or (at your
* option) any later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
* more details.
*
* You should have received a copy of the GNU General Public License along
* with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "MovementPacketBuilder.h"
#include "MoveSpline.h"
#include "WorldPacket.h"
#include "Object.h"

namespace Movement
{
	inline void operator << (ByteBuffer& b, const Vector3& v)
	{
		b << v.x << v.y << v.z;
	}

	inline void operator >> (ByteBuffer& b, Vector3& v)
	{
		b >> v.x >> v.y >> v.z;
	}

	MonsterMoveType PacketBuilder::GetMonsterMoveType(MoveSpline const& moveSpline)
	{
		switch (moveSpline.splineflags & MoveSplineFlag::Mask_Final_Facing)
		{
		case MoveSplineFlag::Final_Target:
			return MonsterMoveFacingTarget;
		case MoveSplineFlag::Final_Angle:
			return MonsterMoveFacingAngle;
		case MoveSplineFlag::Final_Point:
			return MonsterMoveFacingSpot;
		default:
			return MonsterMoveNormal;
		}
	}

	void PacketBuilder::WriteStopMovement(Vector3 const& pos, uint32 splineId, ByteBuffer& data, Unit* unit)
	{

		ObjectGuid guid = unit->GetGUID();
		ObjectGuid transport = unit->GetTransGUID();

		data << float(0.f); // Most likely transport Y
		data << uint32(splineId);
		data << float(0.f); // Most likely transport Z
		data << float(0.f); // Most likely transport X
		data << float(pos.x);
		data << float(pos.y);
		data << float(pos.z);

		data.WriteBit(guid[3]);
		data.WriteBit(1);
		data.WriteBit(guid[6]);

		data.WriteBit(1);
		data.WriteBit(1);

		data.WriteBits(MonsterMoveStop, 3);
		data.WriteBit(1);
		data.WriteBit(guid[2]);
		data.WriteBit(guid[7]);
		data.WriteBit(guid[5]);
		data.WriteBit(1);
		data.WriteBit(guid[4]);

		data.WriteBits(0, 22); // WP count
		data.WriteBit(1);
		data.WriteBit(0);

		data.WriteBit(guid[0]);
		data.WriteBit(transport[3]);
		data.WriteBit(transport[6]);
		data.WriteBit(transport[5]);
		data.WriteBit(transport[0]);
		data.WriteBit(transport[1]);
		data.WriteBit(transport[2]);
		data.WriteBit(transport[4]);
		data.WriteBit(transport[7]);

		data.WriteBit(1);
		data.WriteBit(1); // Parabolic speed // esi+4Ch
		data.WriteBit(1);

		data.WriteBits(0, 20);

		data.WriteBit(guid[1]);
		data.WriteBit(0);
		data.WriteBit(0);
		data.WriteBit(1);

		//        data.FlushBits();

		data.WriteByteSeq(guid[3]);
		data.WriteByteSeq(transport[7]);
		data.WriteByteSeq(transport[3]);
		data.WriteByteSeq(transport[2]);
		data.WriteByteSeq(transport[0]);
		data.WriteByteSeq(transport[6]);
		data.WriteByteSeq(transport[4]);
		data.WriteByteSeq(transport[5]);
		data.WriteByteSeq(transport[1]);
		data.WriteByteSeq(guid[7]);
		data.WriteByteSeq(guid[5]);
		data.WriteByteSeq(guid[1]);
		data.WriteByteSeq(guid[2]);
		data.WriteByteSeq(guid[6]);
		data.WriteByteSeq(guid[0]);
		data.WriteByteSeq(guid[4]);
	}

	void WriteLinearPath(Spline<int32> const& spline, ByteBuffer& data)
	{
		uint32 last_idx = spline.getPointCount() - 3;
		Vector3 const* real_path = &spline.getPoint(1);

		if (last_idx > 0)
		{
			Vector3 middle = (real_path[0] + real_path[last_idx]) / 2.f;
			Vector3 offset;
			// first and last points already appended
			for (uint32 i = 0; i < last_idx; ++i)
			{
				offset = middle - real_path[i];
				data.appendPackXYZ(offset.x, offset.y, offset.z);
			}
		}
	}

	void WriteCatmullRomPath(const Spline<int32>& spline, ByteBuffer& data)
	{
		uint32 count = spline.getPointCount() - 2;

		for (uint32 i = 0; i < count; i++)
			data << spline.getPoint(i + 2).x << spline.getPoint(i + 2).y << spline.getPoint(i + 2).z;

		//data.append<Vector3>(&spline.getPoint(2), count);
	}

	void WriteCatmullRomCyclicPath(const Spline<int32>& spline, ByteBuffer& data)
	{
		uint32 count = spline.getPointCount() - 2;
		data << spline.getPoint(1).x << spline.getPoint(1).y << spline.getPoint(1).z; // fake point, client will erase it from the spline after first cycle done
		for (uint32 i = 0; i < count; i++)
			data << spline.getPoint(i + 1).x << spline.getPoint(i + 1).y << spline.getPoint(i + 1).z;
		//data.append<Vector3>(&spline.getPoint(1), count);
	}

	void WriteUncompressedPath(Spline<int32> const& spline, ByteBuffer& data)
	{
		data.append<Vector3>(&spline.getPoint(2), spline.getPointCount() - 3);
	}

	void WriteUncompressedCyclicPath(Spline<int32> const& spline, ByteBuffer& data)
	{
		data << spline.getPoint(1); // Fake point, client will erase it from the spline after first cycle done
		data.append<Vector3>(&spline.getPoint(1), spline.getPointCount() - 3);
	}

	void PacketBuilder::WriteMonsterMove(const MoveSpline& moveSpline, WorldPacket& data, Unit* unit)
	{
		ObjectGuid moverGUID = unit->GetGUID();
		ObjectGuid transportGUID = unit->GetTransGUID();
		MoveSplineFlag splineflags = moveSpline.splineflags;
		splineflags.enter_cycle = moveSpline.isCyclic();
		uint32 sendSplineFlags = splineflags & ~MoveSplineFlag::Mask_No_Monster_Move;
		int8 seat = unit->GetTransSeat();

		uint32 WPcount = moveSpline.spline.getPointCount() - 2;
		if (splineflags.cyclic)
			WPcount += 1;

		bool hasUnk1 = false;
		bool hasUnk2 = false;
		bool hasUnk3 = false;
		bool unk4 = false;
		uint32 unkCounter = 0;

		MonsterMoveType splineType = GetMonsterMoveType(moveSpline);

		data << float(0.0f);
		data << uint32(getMSTime());
		data << float(0.0f);
		data << float(0.0f);

		const G3D::Vector3 point = moveSpline.spline.getPoint(moveSpline.spline.first());
		data << float(point.x);
		data << float(point.y);
		data << float(point.z);

		data.WriteBit(moverGUID[3]);
		data.WriteBit(sendSplineFlags == 0);        // !hasFlags
		data.WriteBit(moverGUID[6]);
		data.WriteBit(!splineflags.animation);      // animation state
		data.WriteBit(!hasUnk2);                    // !hasUnk2, unk byte
		data.WriteBits(splineType, 3);              // splineType
		data.WriteBit(seat == -1);                  // !has seat
		data.WriteBit(moverGUID[2]);
		data.WriteBit(moverGUID[7]);
		data.WriteBit(moverGUID[5]);

		if (splineType == MonsterMoveFacingTarget)
		{
			ObjectGuid facingTargetGUID = moveSpline.facing.target;
			uint8 facingTargetBitsOrder[8] = { 6, 7, 0, 5, 2, 3, 4, 1 };
			data.WriteBitInOrder(facingTargetGUID, facingTargetBitsOrder);
		}

		data.WriteBit(!splineflags.parabolic);      // !hasParabolicTime
		data.WriteBit(moverGUID[4]);
		int32 compressedSplineCount = moveSpline.splineflags & MoveSplineFlag::UncompressedPath ? 0 : moveSpline.spline.getPointCount() - 3;
		data.WriteBits(compressedSplineCount, 22); // WP count
		data.WriteBit(true);                        // !unk, send uint32
		data.WriteBit(false);                       // fake bit
		data.WriteBit(moverGUID[0]);

		uint8 transportBitsOrder[8] = { 3, 6, 5, 0, 1, 2, 4, 7 };
		data.WriteBitInOrder(transportGUID, transportBitsOrder);

		data.WriteBit(!hasUnk3);                    // !hasUnk3
		data.WriteBit(!splineflags.parabolic);      // !hasParabolicSpeed
		data.WriteBit(!splineflags.animation);      // !hasAnimationTime

		uint32 uncompressedSplineCount = moveSpline.splineflags & MoveSplineFlag::UncompressedPath ? moveSpline.splineflags.cyclic ? moveSpline.spline.getPointCount() - 2 : moveSpline.spline.getPointCount() - 3 : 1;
		data.WriteBits(uncompressedSplineCount, 20);
		data.WriteBit(moverGUID[1]);
		data.WriteBit(hasUnk1);                     // unk, has counter + 2 bits & somes uint16/float

		if (hasUnk1)
		{
			data.WriteBits(unkCounter, 22);
			data.WriteBits(0, 2); // Random guess? Bits9C
		}

		data.WriteBit(unk4);
		data.WriteBit(false);                       // !has duration

		if (splineType == MonsterMoveFacingTarget)
		{
			ObjectGuid facingTargetGUID = moveSpline.facing.target;
			uint8 facingTargetBitsOrder[8] = { 5, 3, 6, 1, 4, 2, 0, 7 };
			data.WriteBytesSeq(facingTargetGUID, facingTargetBitsOrder);
		}

		data.WriteByteSeq(moverGUID[3]);
		data.WriteByteSeq(transportGUID[7]);
		data.WriteByteSeq(transportGUID[3]);
		data.WriteByteSeq(transportGUID[2]);
		data.WriteByteSeq(transportGUID[0]);
		data.WriteByteSeq(transportGUID[6]);
		data.WriteByteSeq(transportGUID[4]);
		data.WriteByteSeq(transportGUID[5]);
		data.WriteByteSeq(transportGUID[1]);
		// Write bytes
		if (hasUnk1)
		{
			data << float(0.0f);

			for (uint32 i = 0; i < unkCounter; i++)
			{
				data << uint16(0);
				data << uint16(0);
			}
			data << float(0.0f);
			data << uint16(0);
			data << uint16(0);
		}

		if (hasUnk2)
			data << uint8(0);                   // unk byte

		if (splineType == MonsterMoveFacingAngle)
			data << (float)moveSpline.facing.angle;

		if (sendSplineFlags)
			data << uint32(sendSplineFlags);
		
		data.WriteByteSeq(moverGUID[7]);
		if (seat != -1)
			data << int8(seat);

		if (false)
			data << uint32(0);                  // unk uint32

		if (splineflags.animation)
			data << uint8(splineflags.getAnimationId());

		if (compressedSplineCount)
			WriteLinearPath(moveSpline.spline, data);

		data.WriteByteSeq(moverGUID[5]);
		data.WriteByteSeq(moverGUID[1]);
		data.WriteByteSeq(moverGUID[2]);

		if (splineflags.animation)
			data << int32(moveSpline.effect_start_time);

		if (moveSpline.splineflags & MoveSplineFlag::UncompressedPath)
		{
			if (moveSpline.splineflags.cyclic)
				WriteUncompressedCyclicPath(moveSpline.spline, data);
			else
				WriteUncompressedPath(moveSpline.spline, data);
		}
		else
			data << moveSpline.spline.getPoint(moveSpline.spline.getPointCount() - 2);
		data.WriteByteSeq(moverGUID[6]);


		data << uint32(moveSpline.Duration());

		if (splineType == MonsterMoveFacingSpot)
			data << moveSpline.facing.f.x << moveSpline.facing.f.y << moveSpline.facing.f.z;

		if (splineflags.parabolic)
			data << moveSpline.vertical_acceleration;

		if (hasUnk3)
			data << uint8(0);                   // unk byte

		data.WriteByteSeq(moverGUID[0]);

		if (splineflags.parabolic)
			data << moveSpline.effect_start_time;

		data.WriteByteSeq(moverGUID[4]);

	}

	void PacketBuilder::WriteCreateBits(MoveSpline const& moveSpline, ByteBuffer& data)
	{
		if (!data.WriteBit(!moveSpline.Finalized()))
			return;

		data.WriteBit(moveSpline.splineflags & (MoveSplineFlag::Parabolic | MoveSplineFlag::Animation));
		data.WriteBits(moveSpline.spline.mode(), 2);
		data.WriteBits(moveSpline.getPath().size(), 20);
		data.WriteBits(moveSpline.splineflags.raw(), 25);
		data.WriteBit((moveSpline.splineflags & MoveSplineFlag::Parabolic) && moveSpline.effect_start_time < moveSpline.Duration());
		data.WriteBit(0); // NYI Block
	}

	void PacketBuilder::WriteCreateData(MoveSpline const& moveSpline, ByteBuffer& data)
	{
		if (!moveSpline.Finalized())
		{
			MoveSplineFlag const& splineFlags = moveSpline.splineflags;
			MonsterMoveType type = GetMonsterMoveType(moveSpline);

			data << float(1.0f);                             // splineInfo.duration_mod_next; added in 3.1

			uint32 nodes = moveSpline.getPath().size();
			for (uint32 i = 0; i < nodes; ++i)
			{
				data << float(moveSpline.getPath()[i].z);
				data << float(moveSpline.getPath()[i].y);
				data << float(moveSpline.getPath()[i].x);
			}

			data << uint8(type);
			data << float(1.0f);                             // splineInfo.duration_mod; added in 3.1

			//    NYI block here

			if (type == MonsterMoveFacingSpot)
				data << moveSpline.facing.f.x << moveSpline.facing.f.y << moveSpline.facing.f.z;

			if ((splineFlags & MoveSplineFlag::Parabolic) && moveSpline.effect_start_time < moveSpline.Duration())
				data << moveSpline.vertical_acceleration;   // added in 3.1

			if (type == MonsterMoveFacingAngle)
				data << moveSpline.facing.angle;

			data << moveSpline.Duration();

			if (splineFlags & (MoveSplineFlag::Parabolic | MoveSplineFlag::Animation))
				data << moveSpline.effect_start_time;       // added in 3.1

			data << moveSpline.timePassed();
		}

		data << moveSpline.GetId();
		Vector3 destination = moveSpline.isCyclic() ? Vector3::zero() : moveSpline.FinalDestination();
		data << destination;
		
	}

	void PacketBuilder::WriteCreateGuid(MoveSpline const& moveSpline, ByteBuffer& data)
	{
		if ((moveSpline.splineflags & MoveSplineFlag::Mask_Final_Facing) == MoveSplineFlag::Final_Target)
		{
			ObjectGuid facingGuid = moveSpline.facing.target;

			uint8 bitOrder[8] = { 6, 7, 3, 0, 5, 1, 4, 2 };
			data.WriteBitInOrder(facingGuid, bitOrder);

			uint8 byteOrder[8] = { 4, 2, 5, 6, 0, 7, 1, 3 };
			data.WriteBytesSeq(facingGuid, byteOrder);
		}
	}
}
