/*######
## Arena Watcher by FrozenSouL
######*/

#include "CreatureTextMgr.h"
#include "Player.h"
#include "BattlegroundMgr.h"
#include "Battleground.h"
#include "Config.h"

#define MSG_HAS_BG_QUEUE "[Error] : You are currently in queue for BG"

enum WatcherData
{
	GOSSIP_OFFSET = GOSSIP_ACTION_INFO_DEF + 10,
};

enum WatcherTexts
{
	SAY_NOT_FOUND_BRACKET = 0,
	SAY_ARENA_NOT_IN_PROGRESS = 1,
	SAY_TARGET_NOT_IN_WORLD = 2,
	SAY_TARGET_NOT_IN_ARENA = 3,
	SAY_TARGET_IS_GM = 4,
};

enum WatcherStrings
{
	STRING_ARENA_2v2 = 111200,
	STRING_ARENA_3v3 = 111201,
	STRING_ARENA_5v5 = 111202, // 1v1
	STRING_FOLLOW = 111203,
};

// queste sono le opzioni disponbili - dopo le leggo con worldscript
bool ArenaSpectatorEnable = false;
bool ArenaSpectatorOnlyGM = false;
bool ArenaSpectatorOnlyRated = false;
bool ArenaSpectatorToPlayers = false;
bool ArenaSpectatorSilence = false;
float ArenaSpectatorSpeed = 3.0f;

//Questo c il mutetime, mi serve se decido che durante le arene gli spectator non possono parlare.
struct ArenaSpectator
{
	time_t mutetime;
	uint32 faction;
};

//La solita vecchia  map (questa parte c identica al bountyhunter)
typedef std::map<uint32, ArenaSpectator> ArenaSpectatorMap;
ArenaSpectatorMap ArenaSpectatorPlayers;

//La funzione iswatcher mi dice se un player sta spectando, ovvero se appartiene alla ArenaSpectator map
bool IsWatcher(uint32 guid)
{
	ArenaSpectatorMap::iterator itr = ArenaSpectatorPlayers.find(guid);
	if (itr != ArenaSpectatorPlayers.end())
		return true;
	return false;
}


void ArenaSpectatorStart(Player* player)
{
	//Pre-teletrasporto in arena: divento invisibile 
	player->SetVisible(false);
	uint32 guid = player->GetGUIDLow();
	player->SetSpectate(true);

	// se esisto gia come watcher non posso riwatchare due volte.
	// in questo caso sarr successo qualche baco, se il player fa logout c OK
	if (IsWatcher(guid))
		return;

	ArenaSpectator data;
	//Mi salvo il mutetime attuale: se ho un mute dato da un giemme, mi permarrr quando esco.
	data.mutetime = player->GetSession()->m_muteTime;
	data.faction = player->getFaction();
	//inserisco il player nella mappa
	ArenaSpectatorPlayers[guid] = data;

}

// Operazioni da fare dopo il teletrasporto
void ArenaSpectatorAfterTeleport(Player* player)
{
	player->AddUnitMovementFlag(MOVEMENTFLAG_WATERWALKING);
	player->setFaction(14);

	// Se abilitato inserisco il silence
	if (ArenaSpectatorSilence)
		player->GetSession()->m_muteTime = time(NULL) + 120 * MINUTE;

	// Abilito altri blocchi al player
	player->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NON_ATTACKABLE | UNIT_FLAG_NOT_SELECTABLE | UNIT_FLAG_PACIFIED);
	player->SetSpeed(MOVE_WALK, ArenaSpectatorSpeed, true);
	player->SetSpeed(MOVE_RUN, ArenaSpectatorSpeed * 2, true);
	player->SetSpeed(MOVE_SWIM, ArenaSpectatorSpeed, true);
	player->SetAcceptWhispers(false);
	//Cosi non posso castare niente neanche per sbaglio
	player->SetDeathState(CORPSE);
}

void ArenaSpectatorEnd(Player* player)
{
	if (player){
		uint32 guid = player->GetGUIDLow();

		// Se sono un watcher, ripristino il mio stato normale dopo essere stato teletrasportato fuori dall'arena
		ArenaSpectatorMap::iterator itr = ArenaSpectatorPlayers.find(guid);
		if (itr != ArenaSpectatorPlayers.end())
		{
			// Eseguo prima i re-setup con le variabili salvate dentro alla mappa
			if (ArenaSpectatorSilence)
				player->GetSession()->m_muteTime = ArenaSpectatorPlayers[guid].mutetime;

			player->setFaction(ArenaSpectatorPlayers[guid].faction);

			// Cancello l'occorrenza nella mappa
			ArenaSpectatorPlayers.erase(itr);

			// Operazioni di riabilitazione player
			player->ResurrectPlayer(100.0f, false);
			player->SetAcceptWhispers(true);
			player->RemoveUnitMovementFlag(MOVEMENTFLAG_WATERWALKING);
			player->SetSpeed(MOVE_WALK, 2.0f, true);
			player->SetSpeed(MOVE_RUN, 2.0f, true);
			player->SetSpeed(MOVE_SWIM, 1.0f, true);
			player->RemoveFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NON_ATTACKABLE | UNIT_FLAG_NOT_SELECTABLE | UNIT_FLAG_PACIFIED);
			player->SetVisible(true);
			player->SetSpectate(false);
		}
	}
}

uint8 GetArenaBySlot(uint8 slot)
{
	switch (slot)
	{
	case 0: return ARENA_TEAM_2v2;
	case 1: return ARENA_TEAM_3v3;
	case 2: return ARENA_TEAM_5v5;
	default:
		break;
	}
	return 0;
}