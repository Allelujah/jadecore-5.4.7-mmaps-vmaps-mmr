#pragma once

class RestrictedTmog
{
public:
    static RestrictedTmog* instance()
    {
        static RestrictedTmog instance;
        return &instance;
    }

    void LoadFromDB()
    {
        uint32 msTime = getMSTime();
        
        // For reload
        RestrictedTmogContainer.clear();

        QueryResult result = WorldDatabase.Query("SELECT * FROM restricted_tmog");

        if (!result)
            return;

        uint32 count = 0;
        do
        {
            Field* fields = result->Fetch();
            RestrictedTmogContainer[fields[0].GetUInt32()];
            ++count;
        } 
        while (result->NextRow());
        
        sLog->outInfo(LOG_FILTER_SERVER_LOADING, "Loaded %u restricted tmogs in %u ms", count, GetMSTimeDiffToNow(msTime));
    }

private:
    std::unordered_map<uint32> RestrictedTmogContainer; // itemid
};

#define sRestrictedTmog RestrictedTmog::instance()

class RestrictedTmog : public WorldScript
{
public:
    RestrictedTmogLoader() : WorldScript("RestrictedTmogLoader") {}

    void OnStartup() override
    {
        sRestrictedTmog->LoadFromDB();
    }
};