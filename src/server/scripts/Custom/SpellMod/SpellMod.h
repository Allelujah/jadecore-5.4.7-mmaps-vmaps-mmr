
#pragma once

#include "Log.h"
#include "ScriptMgr.h"
#include "DatabaseEnv.h"

class SpellMod
{
public:
	static SpellMod* instance()
	{
		static SpellMod instance;
		return &instance;
	}

	/* Spell Meta Data */
	bool GetSpellExistConfirmation(uint32 spellId)
	{
		if (spellId == 0 || (SpellModUIntContainer.find(spellId) == SpellModUIntContainer.end() && SpellModIntContainer.find(spellId) == SpellModIntContainer.end() && 
			SpellModUInt8Container.find(spellId) == SpellModUInt8Container.end() && SpellModFloatContainer.find(spellId) == SpellModFloatContainer.end()))
			return false;

		return true;
	}

	bool GetSpellMetaDataInt(uint32 spellId, std::string meta_key, int& output)
	{
		if (spellId == 0 || SpellModIntContainer.find(spellId) == SpellModIntContainer.end())
			return false;

		if (SpellModIntContainer[spellId].find(meta_key) == SpellModIntContainer[spellId].end())
			return false;

		output = SpellModIntContainer[spellId][meta_key];
		return true;
	}
	bool GetSpellMetaDataUInt(uint32 spellId, std::string meta_key, uint32& output)
	{
		if (spellId == 0 || SpellModUIntContainer.find(spellId) == SpellModUIntContainer.end())
			return false;

		if (SpellModUIntContainer[spellId].find(meta_key) == SpellModUIntContainer[spellId].end())
			return false;

		output = SpellModUIntContainer[spellId][meta_key];
		return true;
	}
	bool GetSpellMetaDataUInt8(uint32 spellId, std::string meta_key, uint8& output)
	{
		if (spellId == 0 || SpellModUInt8Container.find(spellId) == SpellModUInt8Container.end())
			return false;

		if (SpellModUInt8Container[spellId].find(meta_key) == SpellModUInt8Container[spellId].end())
			return false;

		output = SpellModUInt8Container[spellId][meta_key];
		return true;
	}
	bool GetSpellMetaDataFloat(uint32 spellId, std::string meta_key, float& output)
	{
		if (spellId == 0 || SpellModFloatContainer.find(spellId) == SpellModFloatContainer.end())
			return false;

		if (SpellModFloatContainer[spellId].find(meta_key) == SpellModFloatContainer[spellId].end())
			return false;

		output = SpellModFloatContainer[spellId][meta_key];
		return true;
	}

	/* Spell Effect Meta Data */
	bool GetSpellEffectExistConfirmation(uint32 spellId, uint8 count)
	{
		if (spellId == 0 || (SpellModEffectUIntContainer.find(spellId) == SpellModEffectUIntContainer.end() && SpellModEffectIntContainer.find(spellId) == SpellModEffectIntContainer.end() &&
			SpellModEffectUInt8Container.find(spellId) == SpellModEffectUInt8Container.end() && SpellModEffectFloatContainer.find(spellId) == SpellModEffectFloatContainer.end()))
			return false;

		if (SpellModEffectUIntContainer[spellId].find(count) == SpellModEffectUIntContainer[spellId].end() && SpellModEffectIntContainer[spellId].find(count) == SpellModEffectIntContainer[spellId].end() &&
			SpellModEffectUInt8Container[spellId].find(count) == SpellModEffectUInt8Container[spellId].end() && SpellModEffectFloatContainer[spellId].find(count) == SpellModEffectFloatContainer[spellId].end())
			return false;

		return true;
	}

	bool GetSpellEffectMetaDataInt(uint32 spellId, uint8 count, std::string meta_key, int& output)
	{
		if (spellId == 0 || SpellModEffectIntContainer.find(spellId) == SpellModEffectIntContainer.end())
			return false;

		if (SpellModEffectIntContainer[spellId].find(count) == SpellModEffectIntContainer[spellId].end())
			return false;

		if (SpellModEffectIntContainer[spellId][count].find(meta_key) == SpellModEffectIntContainer[spellId][count].end())
			return false;

		output = SpellModEffectIntContainer[spellId][count][meta_key];
		return true;
	}
	bool GetSpellEffectMetaDataUInt(uint32 spellId, uint8 count, std::string meta_key, uint32& output)
	{
		if (spellId == 0 || SpellModEffectUIntContainer.find(spellId) == SpellModEffectUIntContainer.end())
			return false;

		if (SpellModEffectUIntContainer[spellId].find(count) == SpellModEffectUIntContainer[spellId].end())
			return false;

		if (SpellModEffectUIntContainer[spellId][count].find(meta_key) == SpellModEffectUIntContainer[spellId][count].end())
			return false;

		output = SpellModEffectUIntContainer[spellId][count][meta_key];
		return true;
	}
	bool GetSpellEffectMetaDataUInt8(uint32 spellId, uint8 count, std::string meta_key, uint8& output)
	{
		if (spellId == 0 || SpellModEffectUInt8Container.find(spellId) == SpellModEffectUInt8Container.end())
			return false;

		if (SpellModEffectUInt8Container[spellId].find(count) == SpellModEffectUInt8Container[spellId].end())
			return false;

		if (SpellModEffectUInt8Container[spellId][count].find(meta_key) == SpellModEffectUInt8Container[spellId][count].end())
			return false;

		output = SpellModEffectUInt8Container[spellId][count][meta_key];
		return true;
	}
	bool GetSpellEffectMetaDataFloat(uint32 spellId, uint8 count, std::string meta_key, float& output)
	{
		if (spellId == 0 || SpellModEffectFloatContainer.find(spellId) == SpellModEffectFloatContainer.end())
			return false;

		if (SpellModEffectFloatContainer[spellId].find(count) == SpellModEffectFloatContainer[spellId].end())
			return false;

		if (SpellModEffectFloatContainer[spellId][count].find(meta_key) == SpellModEffectFloatContainer[spellId][count].end())
			return false;

		output = SpellModEffectFloatContainer[spellId][count][meta_key];
		return true;
	}

	/*
	Note: We should probably load all the sub table data into a container and dynamically load them into the SpellInfo during LoadFromDB.
	The change would only really change that we have access to that information rather than just querying upon loading, and it will also increase performance when loading
	However we're talking < 1 second
	*/
	void LoadFromDB()
	{
		SpellModUIntContainer.clear();
		SpellModIntContainer.clear();
		SpellModFloatContainer.clear();

		SpellModEffectUIntContainer.clear();
		SpellModEffectIntContainer.clear();
		SpellModEffectFloatContainer.clear();

		uint32 msTime = getMSTime();
		QueryResult spellmod_metadata = WorldDatabase.Query("SELECT Spellid, Count, meta_key, meta_value from spellmod_metadata");

		uint32 count = 0;
		if (spellmod_metadata)
		{
			do
			{
				Field* fields = spellmod_metadata->Fetch();
				uint32 Spellid = fields[0].GetUInt32();
				uint8 Count = fields[1].GetUInt8();
				std::string meta_key = fields[2].GetString();
				std::string meta_value = fields[3].GetString();

				// Validate the row
				if (Spellid == 0 || meta_key == "" || meta_value == "")
					continue;

				// Spell Effect Mod
				if (meta_key == "DieSides" || meta_key == "BasePoints" || meta_key == "MiscValue" ||
					meta_key == "MiscValueB")
				{
					// INT32 Convertion
					int32 value = std::stoi(meta_value);
					SpellModEffectIntContainer[Spellid][Count][meta_key] = value;
				}
				else if (meta_key == "Mechanic" || meta_key == "PositionFacing" || meta_key == "TargetA" ||
					meta_key == "TargetB")
				{
					// UINT8 Convertion
					uint8 value = std::stoul(meta_value);
					SpellModEffectUInt8Container[Spellid][Count][meta_key] = value;
				}
				else if (meta_key == "RealPointsPerLevel" || meta_key == "PointsPerComboPoint" || meta_key == "ValueMultiplier" ||
					meta_key == "DamageMultiplier" || meta_key == "EffectSpellPowerBonus" || meta_key == "ScalingMultiplier" ||
					meta_key == "DeltaScalingMultiplier" || meta_key == "ComboScalingMultiplier")
				{
					// FLOAT Convertion
					float value = std::stof(meta_value);
					SpellModEffectFloatContainer[Spellid][Count][meta_key] = value;
				}
				else if (meta_key == "Effect" || meta_key == "ApplyAuraName" || meta_key == "Amplitude" ||
					meta_key == "RadiusEntry" || meta_key == "ChainTarget" || meta_key == "ItemType" ||
					meta_key == "TriggerSpell" || meta_key == "SpellClassMask")
				{
					// UINT Convertion
					uint32 value = std::stoul(meta_value);
					SpellModEffectUIntContainer[Spellid][Count][meta_key] = value;
				}

				// Spell Mod
				if (meta_key == "EquippedItemClass" || meta_key == "EquippedItemSubClassMask" || meta_key == "EquippedItemInventoryTypeMask" ||
					meta_key == "AreaGroupId" || meta_key == "CastTimeMin" || meta_key == "CastTimeMax" ||
					meta_key == "CastTimeMaxLevel" || meta_key == "ScalingClass" || meta_key == "CoefLevelBase" ||
					meta_key == "CoefLevelBase" || meta_key == "MaxScalingLevel")
				{
					// INT32 Convertion
					int32 value = std::stoi(meta_value);
					SpellModIntContainer[Spellid][meta_key] = value;
				} 
				else if (meta_key == "EffectCount" || meta_key == "m_IsScaled")
				{
					// UINT8 Convertion
					uint8 value = std::stoul(meta_value);
					SpellModUInt8Container[Spellid][meta_key] = value;
				} 
				else if (meta_key == "Speed" || meta_key == "ProcsPerMinute" || meta_key == "CoefBase" ||
					meta_key == "AttackPowerBonus")
				{
					// FLOAT Convertion
					float value = std::stof(meta_value);
					SpellModFloatContainer[Spellid][meta_key] = value;
				}
				else
				{
					try
					{
						// UINT Convertion
						uint32 value = std::stoul(meta_value);
						SpellModUIntContainer[Spellid][meta_key] = value;
					}
					catch (int e)
					{
						continue;
					}
				}

				++count;
			} 
			while (spellmod_metadata->NextRow());
		}
		sLog->outInfo(LOG_FILTER_SERVER_LOADING, "Loaded %u spell mod(s) in %u ms", count, GetMSTimeDiffToNow(msTime));
	}
private:
	std::unordered_map<uint32, std::unordered_map<std::string, uint32>> SpellModUIntContainer; // entry, Meta_key, Meta_value
	std::unordered_map<uint32, std::unordered_map<std::string, uint8>> SpellModUInt8Container; // entry, Meta_key, Meta_value
	std::unordered_map<uint32, std::unordered_map<std::string, int32>> SpellModIntContainer; // entry, Meta_key, Meta_value
	std::unordered_map<uint32, std::unordered_map<std::string, float>> SpellModFloatContainer; // entry, Meta_key, Meta_value

	std::unordered_map<uint32, std::unordered_map<uint8, std::unordered_map<std::string, uint32>>> SpellModEffectUIntContainer; // entry, Effect
	std::unordered_map<uint32, std::unordered_map<uint8, std::unordered_map<std::string, uint8>>> SpellModEffectUInt8Container; // entry, Effect
	std::unordered_map<uint32, std::unordered_map<uint8, std::unordered_map<std::string, int32>>> SpellModEffectIntContainer; // entry, Effect
	std::unordered_map<uint32, std::unordered_map<uint8, std::unordered_map<std::string, float>>> SpellModEffectFloatContainer; // entry, Effect
};

#define sSpellMod SpellMod::instance()

class SpellModLoader : public WorldScript
{
public:
	SpellModLoader() : WorldScript("SpellModLoader") {}

};