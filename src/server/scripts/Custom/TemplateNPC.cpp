#include "ScriptPCH.h"
#include "Chat.h"
#include "ScriptMgr.h"
#include "TemplateNPC.h"

void sTemplateNPC::LearnPlateMailSpells(Player* player) {
    switch (player->getClass()) {
    case CLASS_WARRIOR:
    case CLASS_PALADIN:
    case CLASS_DEATH_KNIGHT:
        player->LearnSpell(PLATE_MAIL, true);
        break;
    case CLASS_SHAMAN:
    case CLASS_HUNTER:
        player->LearnSpell(MAIL, true);
        break;
    default:
        break;
    }
}

void sTemplateNPC::ApplyBonus(Player* player, Item* item, EnchantmentSlot slot, uint32 bonusEntry) {
    if (!item)
        return;

    if (!bonusEntry || bonusEntry == 0)
        return;

    player->ApplyEnchantment(item, slot, false);
    item->SetEnchantment(slot, bonusEntry, 0, 0);
    player->ApplyEnchantment(item, slot, true);
	//player->ApplyReforgeEnchantment(item, false);
}

void sTemplateNPC::ApplyGlyph(Player* player, uint8 slot, uint32 glyphID)
{
	if (GlyphPropertiesEntry const* gp = sGlyphPropertiesStore.LookupEntry(glyphID))
	{
		if (uint32 oldGlyph = player->GetGlyph(player->GetActiveSpecialization(), slot))
		{
			player->RemoveAurasDueToSpell(sGlyphPropertiesStore.LookupEntry(oldGlyph)->SpellId);
			player->SetGlyph(slot, 0);
		}
		player->CastSpell(player, gp->SpellId, true);
		player->SetGlyph(slot, glyphID);
	}
	player->SendTalentsInfoData(false);
}

void sTemplateNPC::LearnTemplateTalents(Player* player)
{
	for (TalentContainer::const_iterator itr = m_TalentContainer.begin(); itr != m_TalentContainer.end(); ++itr)
	{
		if ((*itr)->playerClass == GetClassString(player).c_str() && (*itr)->playerSpec == sTalentsSpec)
		{
			// learn highest rank of talent and learn all non-talent spell ranks (recursive by tree)
			player->LearnSpell((*itr)->talentId, false);
			player->AddTalent((*itr)->talentId, player->GetActiveSpecialization(), true);
		} 
	}
	player->SetFreeTalentPoints(0);
	player->SendTalentsInfoData(false);
	//player->UpdateMastery();
}

void sTemplateNPC::LearnTemplateGlyphs(Player* player)
{
	for (GlyphContainer::const_iterator itr = m_GlyphContainer.begin(); itr != m_GlyphContainer.end(); ++itr)
	{
		if ((*itr)->playerClass == GetClassString(player).c_str() && (*itr)->playerSpec == sTalentsSpec)
			ApplyGlyph(player, (*itr)->slot, (*itr)->glyph);
	}
	player->SendTalentsInfoData(false);
}

void sTemplateNPC::EquipTemplateGear(Player* player) {
    if (player->getRace() == RACE_HUMAN) {
        for (HumanGearContainer::const_iterator itr = m_HumanGearContainer.begin(); itr != m_HumanGearContainer.end(); ++itr) {
            if ((*itr)->playerClass == GetClassString(player).c_str() && (*itr)->playerSpec == sTalentsSpec) {
                player->EquipNewItem((*itr)->pos, (*itr)->itemEntry, true); // Equip the item and apply enchants and gems
                ApplyBonus(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, (*itr)->pos), PERM_ENCHANTMENT_SLOT, (*itr)->enchant);
                ApplyBonus(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, (*itr)->pos), SOCK_ENCHANTMENT_SLOT, (*itr)->socket1);
                ApplyBonus(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, (*itr)->pos), SOCK_ENCHANTMENT_SLOT_2, (*itr)->socket2);
                ApplyBonus(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, (*itr)->pos), SOCK_ENCHANTMENT_SLOT_3, (*itr)->socket3);
                ApplyBonus(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, (*itr)->pos), BONUS_ENCHANTMENT_SLOT, (*itr)->bonusEnchant);
                ApplyBonus(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, (*itr)->pos), PRISMATIC_ENCHANTMENT_SLOT, (*itr)->prismaticEnchant);
				//ApplyBonus(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, (*itr)->pos), REFORGE_ENCHANTMENT_SLOT, (*itr)->reforgeEnchant);
            }
        }
    } else if (player->GetTeam() == ALLIANCE && player->getRace() != RACE_HUMAN) {
        for (AllianceGearContainer::const_iterator itr = m_AllianceGearContainer.begin(); itr != m_AllianceGearContainer.end(); ++itr) {
            if ((*itr)->playerClass == GetClassString(player).c_str() && (*itr)->playerSpec == sTalentsSpec) {
                player->EquipNewItem((*itr)->pos, (*itr)->itemEntry, true); // Equip the item and apply enchants and gems
                ApplyBonus(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, (*itr)->pos), PERM_ENCHANTMENT_SLOT, (*itr)->enchant);
                ApplyBonus(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, (*itr)->pos), SOCK_ENCHANTMENT_SLOT, (*itr)->socket1);
                ApplyBonus(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, (*itr)->pos), SOCK_ENCHANTMENT_SLOT_2, (*itr)->socket2);
                ApplyBonus(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, (*itr)->pos), SOCK_ENCHANTMENT_SLOT_3, (*itr)->socket3);
                ApplyBonus(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, (*itr)->pos), BONUS_ENCHANTMENT_SLOT, (*itr)->bonusEnchant);
                ApplyBonus(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, (*itr)->pos), PRISMATIC_ENCHANTMENT_SLOT, (*itr)->prismaticEnchant);
				//ApplyBonus(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, (*itr)->pos), REFORGE_ENCHANTMENT_SLOT, (*itr)->reforgeEnchant);
            }
        }
    } else if (player->GetTeam() == HORDE) {
        for (HordeGearContainer::const_iterator itr = m_HordeGearContainer.begin(); itr != m_HordeGearContainer.end(); ++itr) {
            if ((*itr)->playerClass == GetClassString(player).c_str() && (*itr)->playerSpec == sTalentsSpec) {
                player->EquipNewItem((*itr)->pos, (*itr)->itemEntry, true); // Equip the item and apply enchants and gems
                ApplyBonus(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, (*itr)->pos), PERM_ENCHANTMENT_SLOT, (*itr)->enchant);
                ApplyBonus(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, (*itr)->pos), SOCK_ENCHANTMENT_SLOT, (*itr)->socket1);
                ApplyBonus(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, (*itr)->pos), SOCK_ENCHANTMENT_SLOT_2, (*itr)->socket2);
                ApplyBonus(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, (*itr)->pos), SOCK_ENCHANTMENT_SLOT_3, (*itr)->socket3);
                ApplyBonus(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, (*itr)->pos), BONUS_ENCHANTMENT_SLOT, (*itr)->bonusEnchant);
                ApplyBonus(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, (*itr)->pos), PRISMATIC_ENCHANTMENT_SLOT, (*itr)->prismaticEnchant);
				//ApplyBonus(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, (*itr)->pos), REFORGE_ENCHANTMENT_SLOT, (*itr)->reforgeEnchant);
            }
        }
    }
}

void sTemplateNPC::LoadTalentsContainer() 
{
    for (TalentContainer::const_iterator itr = m_TalentContainer.begin(); itr != m_TalentContainer.end(); ++itr)
        delete *itr;

    m_TalentContainer.clear();

    uint32 oldMSTime = getMSTime();
    uint32 count = 0;

    QueryResult result = CharacterDatabase.PQuery("SELECT playerClass, playerSpec, talentId FROM template_npc_talents;");

    if (!result) {
		sLog->outInfo(LOG_FILTER_SERVER_LOADING, ">> Loaded 0 talent templates. DB table `template_npc_talents` is empty!");
        return;
    }

    do {
        Field* fields = result->Fetch();

        TalentTemplate* pTalent = new TalentTemplate;

        pTalent->playerClass = fields[0].GetString();
        pTalent->playerSpec = fields[1].GetString();
        pTalent->talentId = fields[2].GetUInt32();

        m_TalentContainer.push_back(pTalent);
        ++count;
    } while (result->NextRow());
	sLog->outInfo(LOG_FILTER_SERVER_LOADING, ">> Loaded %u talent templates in %u ms.", count, GetMSTimeDiffToNow(oldMSTime));
}

void sTemplateNPC::LoadGlyphsContainer() {
    for (GlyphContainer::const_iterator itr = m_GlyphContainer.begin(); itr != m_GlyphContainer.end(); ++itr)
        delete *itr;

    m_GlyphContainer.clear();

    QueryResult result = CharacterDatabase.PQuery("SELECT playerClass, playerSpec, slot, glyph FROM template_npc_glyphs;");

    uint32 oldMSTime = getMSTime();
    uint32 count = 0;

    if (!result) {
		sLog->outInfo(LOG_FILTER_SERVER_LOADING, ">> Loaded 0 glyph templates. DB table `template_npc_glyphs` is empty!");
        return;
    }

    do {
        Field* fields = result->Fetch();

        GlyphTemplate* pGlyph = new GlyphTemplate;

        pGlyph->playerClass = fields[0].GetString();
        pGlyph->playerSpec = fields[1].GetString();
        pGlyph->slot = fields[2].GetUInt8();
        pGlyph->glyph = fields[3].GetUInt32();

        m_GlyphContainer.push_back(pGlyph);
        ++count;
    } while (result->NextRow());
	sLog->outInfo(LOG_FILTER_SERVER_LOADING, ">> Loaded %u glyph templates in %u ms.", count, GetMSTimeDiffToNow(oldMSTime));
}

void sTemplateNPC::LoadHumanGearContainer() {
    for (HumanGearContainer::const_iterator itr = m_HumanGearContainer.begin(); itr != m_HumanGearContainer.end(); ++itr)
        delete *itr;

    m_HumanGearContainer.clear();

	QueryResult result = CharacterDatabase.PQuery("SELECT playerClass, playerSpec, pos, itemEntry, enchant, socket1, socket2, socket3, bonusEnchant, prismaticEnchant FROM template_npc_human;");

    uint32 oldMSTime = getMSTime();
    uint32 count = 0;

    if (!result) {
		sLog->outInfo(LOG_FILTER_SERVER_LOADING, ">> Loaded 0 'gear templates. DB table `template_npc_human` is empty!");
        return;
    }

    do {
        Field* fields = result->Fetch();

        HumanGearTemplate* pItem = new HumanGearTemplate;

        pItem->playerClass = fields[0].GetString();
        pItem->playerSpec = fields[1].GetString();
        pItem->pos = fields[2].GetUInt8();
        pItem->itemEntry = fields[3].GetUInt32();
        pItem->enchant = fields[4].GetUInt32();
        pItem->socket1 = fields[5].GetUInt32();
        pItem->socket2 = fields[6].GetUInt32();
        pItem->socket3 = fields[7].GetUInt32();
        pItem->bonusEnchant = fields[8].GetUInt32();
        pItem->prismaticEnchant = fields[9].GetUInt32();
		pItem->reforgeEnchant = fields[10].GetUInt32();

        m_HumanGearContainer.push_back(pItem);
        ++count;
    } while (result->NextRow());
	sLog->outInfo(LOG_FILTER_SERVER_LOADING, ">> Loaded %u gear templates for Humans in %u ms.", count, GetMSTimeDiffToNow(oldMSTime));
}

void sTemplateNPC::LoadAllianceGearContainer() {
    for (AllianceGearContainer::const_iterator itr = m_AllianceGearContainer.begin(); itr != m_AllianceGearContainer.end(); ++itr)
        delete *itr;

    m_AllianceGearContainer.clear();

	QueryResult result = CharacterDatabase.PQuery("SELECT playerClass, playerSpec, pos, itemEntry, enchant, socket1, socket2, socket3, bonusEnchant, prismaticEnchant FROM template_npc_alliance;");

    uint32 oldMSTime = getMSTime();
    uint32 count = 0;

    if (!result) {
		sLog->outInfo(LOG_FILTER_SERVER_LOADING, ">> Loaded 0 'gear templates. DB table `template_npc_alliance` is empty!");
        return;
    }

    do {
        Field* fields = result->Fetch();

        AllianceGearTemplate* pItem = new AllianceGearTemplate;

        pItem->playerClass = fields[0].GetString();
        pItem->playerSpec = fields[1].GetString();
        pItem->pos = fields[2].GetUInt8();
        pItem->itemEntry = fields[3].GetUInt32();
        pItem->enchant = fields[4].GetUInt32();
        pItem->socket1 = fields[5].GetUInt32();
        pItem->socket2 = fields[6].GetUInt32();
        pItem->socket3 = fields[7].GetUInt32();
        pItem->bonusEnchant = fields[8].GetUInt32();
        pItem->prismaticEnchant = fields[9].GetUInt32();

        m_AllianceGearContainer.push_back(pItem);
        ++count;
    } while (result->NextRow());
	sLog->outInfo(LOG_FILTER_SERVER_LOADING, ">> Loaded %u gear templates for Alliances in %u ms.", count, GetMSTimeDiffToNow(oldMSTime));
}

void sTemplateNPC::LoadHordeGearContainer() {
    for (HordeGearContainer::const_iterator itr = m_HordeGearContainer.begin(); itr != m_HordeGearContainer.end(); ++itr)
        delete *itr;

    m_HordeGearContainer.clear();

    QueryResult result = CharacterDatabase.PQuery("SELECT playerClass, playerSpec, pos, itemEntry, enchant, socket1, socket2, socket3, bonusEnchant, prismaticEnchant FROM template_npc_horde;");

    uint32 oldMSTime = getMSTime();
    uint32 count = 0;

    if (!result) {
		sLog->outInfo(LOG_FILTER_SERVER_LOADING, ">> Loaded 0 'gear templates. DB table `template_npc_horde` is empty!");
        return;
    }

    do {
        Field* fields = result->Fetch();

        HordeGearTemplate* pItem = new HordeGearTemplate;

        pItem->playerClass = fields[0].GetString();
        pItem->playerSpec = fields[1].GetString();
        pItem->pos = fields[2].GetUInt8();
        pItem->itemEntry = fields[3].GetUInt32();
        pItem->enchant = fields[4].GetUInt32();
        pItem->socket1 = fields[5].GetUInt32();
        pItem->socket2 = fields[6].GetUInt32();
        pItem->socket3 = fields[7].GetUInt32();
        pItem->bonusEnchant = fields[8].GetUInt32();
        pItem->prismaticEnchant = fields[9].GetUInt32();
		pItem->reforgeEnchant = fields[10].GetUInt32();

        m_HordeGearContainer.push_back(pItem);
        ++count;
    } while (result->NextRow());
	sLog->outInfo(LOG_FILTER_SERVER_LOADING, ">> Loaded %u gear templates for Hordes in %u ms.", count, GetMSTimeDiffToNow(oldMSTime));
}

std::string sTemplateNPC::GetClassString(Player* player) {
    switch (player->getClass()) {
    case CLASS_PRIEST:
        return "Priest";
        break;
    case CLASS_PALADIN:
        return "Paladin";
        break;
    case CLASS_WARRIOR:
        return "Warrior";
        break;
    case CLASS_MAGE:
        return "Mage";
        break;
    case CLASS_WARLOCK:
        return "Warlock";
        break;
    case CLASS_SHAMAN:
        return "Shaman";
        break;
    case CLASS_DRUID:
        return "Druid";
        break;
    case CLASS_HUNTER:
        return "Hunter";
        break;
    case CLASS_ROGUE:
        return "Rogue";
        break;
    case CLASS_DEATH_KNIGHT:
        return "DeathKnight";
        break;
    default:
        break;
    }
    return "Unknown"; // Fix warning, this should never happen
}

bool sTemplateNPC::OverwriteTemplate(Player* player, std::string& playerSpecStr) {
    // Delete old talent and glyph templates before extracting new ones
    CharacterDatabase.PExecute("DELETE FROM template_npc_talents WHERE playerClass = '%s' AND playerSpec = '%s';", GetClassString(player).c_str(), playerSpecStr.c_str());
    CharacterDatabase.PExecute("DELETE FROM template_npc_glyphs WHERE playerClass = '%s' AND playerSpec = '%s';", GetClassString(player).c_str(), playerSpecStr.c_str());

    // Delete old gear templates before extracting new ones
    if (player->getRace() == RACE_HUMAN) {
        CharacterDatabase.PExecute("DELETE FROM template_npc_human WHERE playerClass = '%s' AND playerSpec = '%s';", GetClassString(player).c_str(), playerSpecStr.c_str());
        player->GetSession()->SendAreaTriggerMessage("Template successfuly created!");
        return false;
    } else if (player->GetTeam() == ALLIANCE && player->getRace() != RACE_HUMAN) {
        CharacterDatabase.PExecute("DELETE FROM template_npc_alliance WHERE playerClass = '%s' AND playerSpec = '%s';", GetClassString(player).c_str(), playerSpecStr.c_str());
        player->GetSession()->SendAreaTriggerMessage("Template successfuly created!");
        return false;
    } else if (player->GetTeam() == HORDE) {
        // ????????????? sTemplateNpcMgr here??
        CharacterDatabase.PExecute("DELETE FROM template_npc_horde WHERE playerClass = '%s' AND playerSpec = '%s';", GetClassString(player).c_str(), playerSpecStr.c_str());
        player->GetSession()->SendAreaTriggerMessage("Template successfuly created!");
        return false;
    }
    return true;
}

void sTemplateNPC::ExtractGearTemplateToDB(Player* player, std::string& playerSpecStr) 
{
    for (uint8 i = EQUIPMENT_SLOT_START; i < EQUIPMENT_SLOT_END; ++i) {
        Item* equippedItem = player->GetItemByPos(INVENTORY_SLOT_BAG_0, i);

        if (equippedItem) {
            if (player->getRace() == RACE_HUMAN) {
				CharacterDatabase.PExecute("INSERT INTO template_npc_human (`playerClass`, `playerSpec`, `pos`, `itemEntry`, `enchant`, `socket1`, `socket2`, `socket3`, `bonusEnchant`, `prismaticEnchant` VALUES ('%s', '%s', '%u', '%u', '%u', '%u', '%u', '%u', '%u', '%u');"
                                           , GetClassString(player).c_str(), playerSpecStr.c_str(), equippedItem->GetSlot(), equippedItem->GetEntry(), equippedItem->GetEnchantmentId(PERM_ENCHANTMENT_SLOT),
                                           equippedItem->GetEnchantmentId(SOCK_ENCHANTMENT_SLOT), equippedItem->GetEnchantmentId(SOCK_ENCHANTMENT_SLOT_2), equippedItem->GetEnchantmentId(SOCK_ENCHANTMENT_SLOT_3),
										   equippedItem->GetEnchantmentId(BONUS_ENCHANTMENT_SLOT), equippedItem->GetEnchantmentId(PRISMATIC_ENCHANTMENT_SLOT)/*, equippedItem->GetEnchantmentId(REFORGE_ENCHANTMENT_SLOT)*/);
            } else if (player->GetTeam() == ALLIANCE && player->getRace() != RACE_HUMAN) {
				CharacterDatabase.PExecute("INSERT INTO template_npc_alliance (`playerClass`, `playerSpec`, `pos`, `itemEntry`, `enchant`, `socket1`, `socket2`, `socket3`, `bonusEnchant`, `prismaticEnchant`) VALUES ('%s', '%s', '%u', '%u', '%u', '%u', '%u', '%u', '%u', '%u');"
                                           , GetClassString(player).c_str(), playerSpecStr.c_str(), equippedItem->GetSlot(), equippedItem->GetEntry(), equippedItem->GetEnchantmentId(PERM_ENCHANTMENT_SLOT),
                                           equippedItem->GetEnchantmentId(SOCK_ENCHANTMENT_SLOT), equippedItem->GetEnchantmentId(SOCK_ENCHANTMENT_SLOT_2), equippedItem->GetEnchantmentId(SOCK_ENCHANTMENT_SLOT_3),
										  equippedItem->GetEnchantmentId(BONUS_ENCHANTMENT_SLOT), equippedItem->GetEnchantmentId(PRISMATIC_ENCHANTMENT_SLOT)/*, equippedItem->GetEnchantmentId(REFORGE_ENCHANTMENT_SLOT)*/);
            } else if (player->GetTeam() == HORDE) {
				CharacterDatabase.PExecute("INSERT INTO template_npc_horde (`playerClass`, `playerSpec`, `pos`, `itemEntry`, `enchant`, `socket1`, `socket2`, `socket3`, `bonusEnchant`, `prismaticEnchant`) VALUES ('%s', '%s', '%u', '%u', '%u', '%u', '%u', '%u', '%u', '%u');"
                                           , GetClassString(player).c_str(), playerSpecStr.c_str(), equippedItem->GetSlot(), equippedItem->GetEntry(), equippedItem->GetEnchantmentId(PERM_ENCHANTMENT_SLOT),
                                           equippedItem->GetEnchantmentId(SOCK_ENCHANTMENT_SLOT), equippedItem->GetEnchantmentId(SOCK_ENCHANTMENT_SLOT_2), equippedItem->GetEnchantmentId(SOCK_ENCHANTMENT_SLOT_3),
                                           equippedItem->GetEnchantmentId(BONUS_ENCHANTMENT_SLOT), equippedItem->GetEnchantmentId(PRISMATIC_ENCHANTMENT_SLOT) /*equippedItem->GetEnchantmentId(REFORGE_ENCHANTMENT_SLOT)*/);
 			}
            }
        }
}

void sTemplateNPC::ExtractTalentTemplateToDB(Player* player, std::string& playerSpecStr)
{
	QueryResult result = CharacterDatabase.PQuery("SELECT spell FROM character_talent WHERE guid = '%u';", player->GetGUID());

	if (!result || player->GetFreeTalentPoints() > 6)
	{
		player->GetSession()->SendAreaTriggerMessage("You have unspend talent points. Please spend all your talent points and re-extract the template.");
		return;
	}

	do
	{
		Field* fields = result->Fetch();
		uint32 spell = fields[0].GetUInt32();

		CharacterDatabase.PExecute("INSERT INTO template_npc_talents (playerClass, playerSpec, talentId) "
			"VALUES ('%s', '%s', '%u');", GetClassString(player).c_str(), playerSpecStr.c_str(), spell);
	} while (result->NextRow());

}

void sTemplateNPC::ExtractGlyphsTemplateToDB(Player* player, std::string& playerSpecStr) {
    QueryResult result = CharacterDatabase.PQuery("SELECT glyph1, glyph2, glyph3, glyph4, glyph5, glyph6 "
                         "FROM character_glyphs WHERE guid = '%u' AND spec = '%u';", player->GetGUID(), player->GetActiveSpecialization());

    for (uint8 slot = 0; slot < MAX_GLYPH_SLOT_INDEX; ++slot) {
        if (!result) {
            player->GetSession()->SendAreaTriggerMessage("Get glyphs and re-extract the template!");
            continue;
        }

        Field* fields = result->Fetch();
        uint32 glyph1 = fields[0].GetUInt32();
        uint32 glyph2 = fields[1].GetUInt32();
        uint32 glyph3 = fields[2].GetUInt32();
        uint32 glyph4 = fields[3].GetUInt32();
        uint32 glyph5 = fields[4].GetUInt32();
        uint32 glyph6 = fields[5].GetUInt32();
        uint32 glyph7 = fields[6].GetUInt32();

        uint32 storedGlyph;

        switch (slot) {
        case 0:
            storedGlyph = glyph1;
            break;
        case 1:
            storedGlyph = glyph2;
            break;
        case 2:
            storedGlyph = glyph3;
            break;
        case 3:
            storedGlyph = glyph4;
            break;
        case 4:
            storedGlyph = glyph5;
            break;
        case 5:
            storedGlyph = glyph6;
            break;
        default:
            break;
        }

        CharacterDatabase.PExecute("INSERT INTO template_npc_glyphs (playerClass, playerSpec, slot, glyph) "
                                   "VALUES ('%s', '%s', '%u', '%u');", GetClassString(player).c_str(), playerSpecStr.c_str(), slot, storedGlyph);
    }
}

bool sTemplateNPC::CanEquipTemplate(Player* player, std::string& playerSpecStr) {
    if (player->getRace() == RACE_HUMAN) {
        QueryResult result = CharacterDatabase.PQuery("SELECT playerClass, playerSpec FROM template_npc_human "
                             "WHERE playerClass = '%s' AND playerSpec = '%s';", GetClassString(player).c_str(), playerSpecStr.c_str());

        if (!result)
            return false;
    } else if (player->GetTeam() == ALLIANCE && player->getRace() != RACE_HUMAN) {
        QueryResult result = CharacterDatabase.PQuery("SELECT playerClass, playerSpec FROM template_npc_alliance "
                             "WHERE playerClass = '%s' AND playerSpec = '%s';", GetClassString(player).c_str(), playerSpecStr.c_str());

        if (!result)
            return false;
    } else if (player->GetTeam() == HORDE) {
        QueryResult result = CharacterDatabase.PQuery("SELECT playerClass, playerSpec FROM template_npc_horde "
                             "WHERE playerClass = '%s' AND playerSpec = '%s';", GetClassString(player).c_str(), playerSpecStr.c_str());

        if (!result)
            return false;
    }
    return true;
}

// Command Script "TemplateNPC"
class TemplateNPC_Commands : public CommandScript
{
public:
	TemplateNPC_Commands() : CommandScript("TemplateNPC_Commands") { }

	ChatCommand* GetCommands() const
	{
		static ChatCommand createDeathKnightItemSetTable[] =
		{
			{ "blood",				SEC_ADMINISTRATOR,			true,			&HandleCreateDeathKnightBloodItemSetCommand,		"" },
			{ "frost",				SEC_ADMINISTRATOR,			true,			&HandleCreateDeathKnightFrostItemSetCommand,		"" },
			{ "unholy",				SEC_ADMINISTRATOR,			true,			&HandleCreateDeathKnightUnholyItemSetCommand,		"" },
            { NULL, 0, false, NULL, "", NULL }
		};
		static ChatCommand createRogueItemSetTable[] =
		{
			{ "assassination",		SEC_ADMINISTRATOR,			true,			&HandleCreateRogueAssassinationItemSetCommand,		"" },
			{ "combat",				SEC_ADMINISTRATOR,			true,			&HandleCreateRogueCombatItemSetCommand,				"" },
			{ "subtlety",			SEC_ADMINISTRATOR,			true,			&HandleCreateRogueSubtletyItemSetCommand,			"" },
            { NULL, 0, false, NULL, "", NULL }
		};
		static ChatCommand createHunterItemSetTable[] =
		{
			{ "marksmanship",		SEC_ADMINISTRATOR,			true,			&HandleCreateHunterMarksmanshipItemSetCommand,		"" },
			{ "beastmastery",		SEC_ADMINISTRATOR,			true,			&HandleCreateHunterBeastmasteryItemSetCommand,		"" },
			{ "survival",			SEC_ADMINISTRATOR,			true,			&HandleCreateHunterSurvivalItemSetCommand,			"" },
            { NULL, 0, false, NULL, "", NULL }
		};
		static ChatCommand createDruidItemSetTable[] =
		{
			{ "balance",			SEC_ADMINISTRATOR,			true,			&HandleCreateDruidBalanceItemSetCommand,			"" },
			{ "feral",				SEC_ADMINISTRATOR,			true,			&HandleCreateDruidFeralItemSetCommand,				"" },
			{ "restoration",		SEC_ADMINISTRATOR,			true,			&HandleCreateDruidRestorationItemSetCommand,		"" },
            { NULL, 0, false, NULL, "", NULL }
		};
		static ChatCommand createShamanItemSetTable[] =
		{
			{ "elemental",			SEC_ADMINISTRATOR,			true,			&HandleCreateShamanElementalItemSetCommand,			"" },
			{ "enhancement",		SEC_ADMINISTRATOR,			true,			&HandleCreateShamanEnhancementItemSetCommand,		"" },
			{ "restoration",		SEC_ADMINISTRATOR,			true,			&HandleCreateShamanRestorationItemSetCommand,		"" },
            { NULL, 0, false, NULL, "", NULL }
		};
		static ChatCommand createWarlockItemSetTable[] =
		{
			{ "affliction",			SEC_ADMINISTRATOR,			true,			&HandleCreateWarlockAfflictionItemSetCommand,		"" },
			{ "demonology",			SEC_ADMINISTRATOR,			true,			&HandleCreateWarlockDemonologyItemSetCommand,		"" },
			{ "destruction",		SEC_ADMINISTRATOR,			true,			&HandleCreateWarlockDestructionItemSetCommand,		"" },
            { NULL, 0, false, NULL, "", NULL }
		};
		static ChatCommand createMageItemSetTable[] =
		{
			{ "frost",				SEC_GAMEMASTER,			true,			&HandleCreateMageFrostItemSetCommand,				"" },
			{ "fire",				SEC_GAMEMASTER,			true,			&HandleCreateMageFireItemSetCommand,				"" },
			{ "arcane",				SEC_GAMEMASTER,			true,			&HandleCreateMageArcaneItemSetCommand,				"" },
            { NULL, 0, false, NULL, "", NULL }
		};
		static ChatCommand createWarriorItemSetTable[] =
		{
			{ "arms",				SEC_ADMINISTRATOR,			true,			&HandleCreateWarriorArmsItemSetCommand,				"" },
			{ "fury",				SEC_ADMINISTRATOR,			true,			&HandleCreateWarriorFuryItemSetCommand,				"" },
			{ "protection",			SEC_ADMINISTRATOR,			true,			&HandleCreateWarriorProtectionItemSetCommand,		"" },
            { NULL, 0, false, NULL, "", NULL }
		};
		static ChatCommand createPaladinItemSetTable[] =
		{
			{ "holy",				SEC_ADMINISTRATOR,			true,			&HandleCreatePaladinHolyItemSetCommand,				"" },
			{ "protection",			SEC_ADMINISTRATOR,			true,			&HandleCreatePaladinProtectionItemSetCommand,		"" },
			{ "retribution",		SEC_ADMINISTRATOR,			true,			&HandleCreatePaladinRetributionItemSetCommand,		"" },
            { NULL, 0, false, NULL, "", NULL }
		};
		static ChatCommand createPriestItemSetTable[] =
		{
			{ "discipline",			SEC_ADMINISTRATOR,			true,			&HandleCreatePriestDisciplineItemSetCommand,		"" },
			{ "shadow",				SEC_ADMINISTRATOR,			true,			&HandleCreatePriestShadowItemSetCommand,			"" },
			{ "holy",				SEC_ADMINISTRATOR,			true,			&HandleCreatePriestHolyItemSetCommand,				"" },
            { NULL, 0, false, NULL, "", NULL }
		};
		static ChatCommand createItemSetCommandTable[] =
		{
			{ "priest",				SEC_ADMINISTRATOR,			true,			NULL,		"",			createPriestItemSetTable },
			{ "paladin",			SEC_ADMINISTRATOR,			true,			NULL,		"",			createPaladinItemSetTable },
			{ "warrior",			SEC_ADMINISTRATOR,			true,			NULL,		"",			createWarriorItemSetTable },
			{ "mage",				SEC_ADMINISTRATOR,			true,			NULL,		"",			createMageItemSetTable },
			{ "warlock",			SEC_ADMINISTRATOR,			true,			NULL,		"",			createWarlockItemSetTable },
			{ "shaman",				SEC_ADMINISTRATOR,			true,			NULL,		"",			createShamanItemSetTable },
			{ "druid",				SEC_ADMINISTRATOR,			true,			NULL,		"",			createDruidItemSetTable },
			{ "hunter",				SEC_ADMINISTRATOR,			true,			NULL,		"",			createHunterItemSetTable },
			{ "rogue",				SEC_ADMINISTRATOR,			true,			NULL,		"",			createRogueItemSetTable },
			{ "deathknight",		SEC_ADMINISTRATOR,			true,			NULL,		"",			createDeathKnightItemSetTable },
		};
		static ChatCommand commandTable[] =
		{
            { "create", SEC_GAMEMASTER, true, NULL, "", createItemSetCommandTable },
            { NULL, 0, false, NULL, "", NULL }
		};
		return commandTable;
	}

	// DISCIPLINE PRIEST
	static bool HandleCreatePriestDisciplineItemSetCommand(ChatHandler* handler, const char* args)
	{
		Player* player = handler->GetSession()->GetPlayer();

		if (player->getClass() != CLASS_PRIEST)
		{
			player->GetSession()->SendAreaTriggerMessage("You're not a priest!");
			return false;
		}
		player->SaveToDB();
		sTemplateNpcMgr->sTalentsSpec = "Discipline";
		sTemplateNpcMgr->OverwriteTemplate(player, sTemplateNpcMgr->sTalentsSpec);
		sTemplateNpcMgr->ExtractGearTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
		sTemplateNpcMgr->ExtractTalentTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
		sTemplateNpcMgr->ExtractGlyphsTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
		return true;
	}

	// SHADOW PRIEST
	static bool HandleCreatePriestShadowItemSetCommand(ChatHandler* handler, const char* args)
	{
		Player* player = handler->GetSession()->GetPlayer();
		if (player->getClass() != CLASS_PRIEST)
		{
			player->GetSession()->SendAreaTriggerMessage("You're not a priest!");
			return false;
		}
		player->SaveToDB();
		sTemplateNpcMgr->sTalentsSpec = "Shadow";
		sTemplateNpcMgr->OverwriteTemplate(player, sTemplateNpcMgr->sTalentsSpec);
		sTemplateNpcMgr->ExtractGearTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
		sTemplateNpcMgr->ExtractTalentTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
		sTemplateNpcMgr->ExtractGlyphsTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
		return true;
	}

	// HOLY PRIEST
	static bool HandleCreatePriestHolyItemSetCommand(ChatHandler* handler, const char* args)
	{
		Player* player = handler->GetSession()->GetPlayer();
		if (player->getClass() != CLASS_PRIEST)
		{
			player->GetSession()->SendAreaTriggerMessage("You're not a priest!");
			return false;
		}
		player->SaveToDB();
		sTemplateNpcMgr->sTalentsSpec = "Holy";
		sTemplateNpcMgr->OverwriteTemplate(player, sTemplateNpcMgr->sTalentsSpec);
		sTemplateNpcMgr->ExtractGearTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
		sTemplateNpcMgr->ExtractTalentTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
		sTemplateNpcMgr->ExtractGlyphsTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
		return true;
	}

	// HOLY PALADIN
	static bool HandleCreatePaladinHolyItemSetCommand(ChatHandler* handler, const char* args)
	{
		Player* player = handler->GetSession()->GetPlayer();
		if (player->getClass() != CLASS_PALADIN)
		{
			player->GetSession()->SendAreaTriggerMessage("You're not a paladin!");
			return false;
		}
		player->SaveToDB();
		sTemplateNpcMgr->sTalentsSpec = "Holy";
		sTemplateNpcMgr->OverwriteTemplate(player, sTemplateNpcMgr->sTalentsSpec);
		sTemplateNpcMgr->ExtractGearTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
		sTemplateNpcMgr->ExtractTalentTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
		sTemplateNpcMgr->ExtractGlyphsTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
		return true;
	}

	// PROTECTION PALADIN
	static bool HandleCreatePaladinProtectionItemSetCommand(ChatHandler* handler, const char* args)
	{
		Player* player = handler->GetSession()->GetPlayer();
		if (player->getClass() != CLASS_PALADIN)
		{
			player->GetSession()->SendAreaTriggerMessage("You're not a paladin!");
			return false;
		}
		player->SaveToDB();
		sTemplateNpcMgr->sTalentsSpec = "Protection";
		sTemplateNpcMgr->OverwriteTemplate(player, sTemplateNpcMgr->sTalentsSpec);
		sTemplateNpcMgr->ExtractGearTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
		sTemplateNpcMgr->ExtractTalentTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
		sTemplateNpcMgr->ExtractGlyphsTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
		return true;
	}

	// RETRIBUTION PALADIN
	static bool HandleCreatePaladinRetributionItemSetCommand(ChatHandler* handler, const char* args)
	{
		Player* player = handler->GetSession()->GetPlayer();
		if (player->getClass() != CLASS_PALADIN)
		{
			player->GetSession()->SendAreaTriggerMessage("You're not a paladin!");
			return false;
		}
		player->SaveToDB();
		sTemplateNpcMgr->sTalentsSpec = "Retribution";
		sTemplateNpcMgr->OverwriteTemplate(player, sTemplateNpcMgr->sTalentsSpec);
		sTemplateNpcMgr->ExtractGearTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
		sTemplateNpcMgr->ExtractTalentTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
		sTemplateNpcMgr->ExtractGlyphsTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
		return true;
	}

	// FURY WARRIOR
	static bool HandleCreateWarriorFuryItemSetCommand(ChatHandler* handler, const char* args)
	{
		Player* player = handler->GetSession()->GetPlayer();
		if (player->getClass() != CLASS_WARRIOR)
		{
			player->GetSession()->SendAreaTriggerMessage("You're not a warrior!");
			return false;
		}
		player->SaveToDB();
		sTemplateNpcMgr->sTalentsSpec = "Fury";
		sTemplateNpcMgr->OverwriteTemplate(player, sTemplateNpcMgr->sTalentsSpec);
		sTemplateNpcMgr->ExtractGearTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
		sTemplateNpcMgr->ExtractTalentTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
		sTemplateNpcMgr->ExtractGlyphsTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
		return true;
	}

	// ARMS WARRIOR
	static bool HandleCreateWarriorArmsItemSetCommand(ChatHandler* handler, const char* args)
	{
		Player* player = handler->GetSession()->GetPlayer();
		if (player->getClass() != CLASS_WARRIOR)
		{
			player->GetSession()->SendAreaTriggerMessage("You're not a warrior!");
			return false;
		}
		player->SaveToDB();
		sTemplateNpcMgr->sTalentsSpec = "Arms";
		sTemplateNpcMgr->OverwriteTemplate(player, sTemplateNpcMgr->sTalentsSpec);
		sTemplateNpcMgr->ExtractGearTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
		sTemplateNpcMgr->ExtractTalentTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
		sTemplateNpcMgr->ExtractGlyphsTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
		return true;
	}

	// PROTECTION WARRIOR
	static bool HandleCreateWarriorProtectionItemSetCommand(ChatHandler* handler, const char* args)
	{
		Player* player = handler->GetSession()->GetPlayer();
		if (player->getClass() != CLASS_WARRIOR)
		{
			player->GetSession()->SendAreaTriggerMessage("You're not a warrior!");
			return false;
		}
		player->SaveToDB();
		sTemplateNpcMgr->sTalentsSpec = "Protection";
		sTemplateNpcMgr->OverwriteTemplate(player, sTemplateNpcMgr->sTalentsSpec);
		sTemplateNpcMgr->ExtractGearTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
		sTemplateNpcMgr->ExtractTalentTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
		sTemplateNpcMgr->ExtractGlyphsTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
		return true;
	}

	// ARCANE MAGE
	static bool HandleCreateMageArcaneItemSetCommand(ChatHandler* handler, const char* args)
	{
		Player* player = handler->GetSession()->GetPlayer();
		if (player->getClass() != CLASS_MAGE)
		{
			player->GetSession()->SendAreaTriggerMessage("You're not a mage!");
			return false;
		}
		player->SaveToDB();
		sTemplateNpcMgr->sTalentsSpec = "Arcane";
		sTemplateNpcMgr->OverwriteTemplate(player, sTemplateNpcMgr->sTalentsSpec);
		sTemplateNpcMgr->ExtractGearTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
		sTemplateNpcMgr->ExtractTalentTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
		sTemplateNpcMgr->ExtractGlyphsTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
		return true;
	}

	// FIRE MAGE
	static bool HandleCreateMageFireItemSetCommand(ChatHandler* handler, const char* args)
	{
		Player* player = handler->GetSession()->GetPlayer();
		if (player->getClass() != CLASS_MAGE)
		{
			player->GetSession()->SendAreaTriggerMessage("You're not a mage!");
			return false;
		}
		player->SaveToDB();
		sTemplateNpcMgr->sTalentsSpec = "Fire";
		sTemplateNpcMgr->OverwriteTemplate(player, sTemplateNpcMgr->sTalentsSpec);
		sTemplateNpcMgr->ExtractGearTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
		sTemplateNpcMgr->ExtractTalentTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
		sTemplateNpcMgr->ExtractGlyphsTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
		return true;
	}

	// FROST MAGE
	static bool HandleCreateMageFrostItemSetCommand(ChatHandler* handler, const char* args)
	{
		Player* player = handler->GetSession()->GetPlayer();
		if (player->getClass() != CLASS_MAGE)
		{
			player->GetSession()->SendAreaTriggerMessage("You're not a mage!");
			return false;
		}
		player->SaveToDB();
		sTemplateNpcMgr->sTalentsSpec = "Frost";
		sTemplateNpcMgr->OverwriteTemplate(player, sTemplateNpcMgr->sTalentsSpec);
		sTemplateNpcMgr->ExtractGearTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
		sTemplateNpcMgr->ExtractTalentTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
		sTemplateNpcMgr->ExtractGlyphsTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
		return true;
	}

	// AFFLICTION WARLOCK
	static bool HandleCreateWarlockAfflictionItemSetCommand(ChatHandler* handler, const char* args)
	{
		Player* player = handler->GetSession()->GetPlayer();
		if (player->getClass() != CLASS_WARLOCK)
		{
			player->GetSession()->SendAreaTriggerMessage("You're not a warlock!");
			return false;
		}
		player->SaveToDB();
		sTemplateNpcMgr->sTalentsSpec = "Affliction";
		sTemplateNpcMgr->OverwriteTemplate(player, sTemplateNpcMgr->sTalentsSpec);
		sTemplateNpcMgr->ExtractGearTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
		sTemplateNpcMgr->ExtractTalentTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
		sTemplateNpcMgr->ExtractGlyphsTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
		return true;
	}

	// DEMONOLOGY WARLOCK
	static bool HandleCreateWarlockDemonologyItemSetCommand(ChatHandler* handler, const char* args)
	{
		Player* player = handler->GetSession()->GetPlayer();
		if (player->getClass() != CLASS_WARLOCK)
		{
			player->GetSession()->SendAreaTriggerMessage("You're not a warlock!");
			return false;
		}
		player->SaveToDB();
		sTemplateNpcMgr->sTalentsSpec = "Demonology";
		sTemplateNpcMgr->OverwriteTemplate(player, sTemplateNpcMgr->sTalentsSpec);
		sTemplateNpcMgr->ExtractGearTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
		sTemplateNpcMgr->ExtractTalentTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
		sTemplateNpcMgr->ExtractGlyphsTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
		return true;
	}

	// DESTRUCTION WARLOCK
	static bool HandleCreateWarlockDestructionItemSetCommand(ChatHandler* handler, const char* args)
	{
		Player* player = handler->GetSession()->GetPlayer();
		if (player->getClass() != CLASS_WARLOCK)
		{
			player->GetSession()->SendAreaTriggerMessage("You're not a warlock!");
			return false;
		}
		player->SaveToDB();
		sTemplateNpcMgr->sTalentsSpec = "Destruction";
		sTemplateNpcMgr->OverwriteTemplate(player, sTemplateNpcMgr->sTalentsSpec);
		sTemplateNpcMgr->ExtractGearTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
		sTemplateNpcMgr->ExtractTalentTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
		sTemplateNpcMgr->ExtractGlyphsTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
		return true;
	}

	// ELEMENTAL SHAMAN
	static bool HandleCreateShamanElementalItemSetCommand(ChatHandler* handler, const char* args)
	{
		Player* player = handler->GetSession()->GetPlayer();
		if (player->getClass() != CLASS_SHAMAN)
		{
			player->GetSession()->SendAreaTriggerMessage("You're not a shaman!");
			return false;
		}
		player->SaveToDB();
		sTemplateNpcMgr->sTalentsSpec = "Elemental";
		sTemplateNpcMgr->OverwriteTemplate(player, sTemplateNpcMgr->sTalentsSpec);
		sTemplateNpcMgr->ExtractGearTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
		sTemplateNpcMgr->ExtractTalentTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
		sTemplateNpcMgr->ExtractGlyphsTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
		return true;
	}

	// ENHANCEMENT SHAMAN
	static bool HandleCreateShamanEnhancementItemSetCommand(ChatHandler* handler, const char* args)
	{
		Player* player = handler->GetSession()->GetPlayer();
		if (player->getClass() != CLASS_SHAMAN)
		{
			player->GetSession()->SendAreaTriggerMessage("You're not a shaman!");
			return false;
		}
		player->SaveToDB();
		sTemplateNpcMgr->sTalentsSpec = "Enhancement";
		sTemplateNpcMgr->OverwriteTemplate(player, sTemplateNpcMgr->sTalentsSpec);
		sTemplateNpcMgr->ExtractGearTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
		sTemplateNpcMgr->ExtractTalentTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
		sTemplateNpcMgr->ExtractGlyphsTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
		return true;
	}

	// RESTORATION SHAMAN
	static bool HandleCreateShamanRestorationItemSetCommand(ChatHandler* handler, const char* args)
	{
		Player* player = handler->GetSession()->GetPlayer();
		if (player->getClass() != CLASS_SHAMAN)
		{
			player->GetSession()->SendAreaTriggerMessage("You're not a shaman!");
			return false;
		}
		player->SaveToDB();
		sTemplateNpcMgr->sTalentsSpec = "Restoration";
		sTemplateNpcMgr->OverwriteTemplate(player, sTemplateNpcMgr->sTalentsSpec);
		sTemplateNpcMgr->ExtractGearTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
		sTemplateNpcMgr->ExtractTalentTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
		sTemplateNpcMgr->ExtractGlyphsTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
		return true;
	}

	// BALLANCE DRUID
	static bool HandleCreateDruidBalanceItemSetCommand(ChatHandler* handler, const char* args)
	{
		Player* player = handler->GetSession()->GetPlayer();
		if (player->getClass() != CLASS_DRUID)
		{
			player->GetSession()->SendAreaTriggerMessage("You're not a druid!");
			return false;
		}
		player->SaveToDB();
		sTemplateNpcMgr->sTalentsSpec = "Balance";
		sTemplateNpcMgr->OverwriteTemplate(player, sTemplateNpcMgr->sTalentsSpec);
		sTemplateNpcMgr->ExtractGearTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
		sTemplateNpcMgr->ExtractTalentTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
		sTemplateNpcMgr->ExtractGlyphsTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
		return true;
	}

	// FERAL DRUID
	static bool HandleCreateDruidFeralItemSetCommand(ChatHandler* handler, const char* args)
	{
		Player* player = handler->GetSession()->GetPlayer();
		if (player->getClass() != CLASS_DRUID)
		{
			player->GetSession()->SendAreaTriggerMessage("You're not a druid!");
			return false;
		}
		player->SaveToDB();
		sTemplateNpcMgr->sTalentsSpec = "Feral";
		sTemplateNpcMgr->OverwriteTemplate(player, sTemplateNpcMgr->sTalentsSpec);
		sTemplateNpcMgr->ExtractGearTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
		sTemplateNpcMgr->ExtractTalentTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
		sTemplateNpcMgr->ExtractGlyphsTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
		return true;
	}

	// RESTORATION DRUID
	static bool HandleCreateDruidRestorationItemSetCommand(ChatHandler* handler, const char* args)
	{
		Player* player = handler->GetSession()->GetPlayer();
		if (player->getClass() != CLASS_DRUID)
		{
			player->GetSession()->SendAreaTriggerMessage("You're not a druid!");
			return false;
		}
		player->SaveToDB();
		sTemplateNpcMgr->sTalentsSpec = "Restoration";
		sTemplateNpcMgr->OverwriteTemplate(player, sTemplateNpcMgr->sTalentsSpec);
		sTemplateNpcMgr->ExtractGearTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
		sTemplateNpcMgr->ExtractTalentTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
		sTemplateNpcMgr->ExtractGlyphsTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
		return true;
	}

	// MARKSMANSHIP HUNTER
	static bool HandleCreateHunterMarksmanshipItemSetCommand(ChatHandler* handler, const char* args)
	{
		Player* player = handler->GetSession()->GetPlayer();
		if (player->getClass() != CLASS_HUNTER)
		{
			player->GetSession()->SendAreaTriggerMessage("You're not a hunter!");
			return false;
		}
		player->SaveToDB();
		sTemplateNpcMgr->sTalentsSpec = "Marksmanship";
		sTemplateNpcMgr->OverwriteTemplate(player, sTemplateNpcMgr->sTalentsSpec);
		sTemplateNpcMgr->ExtractGearTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
		sTemplateNpcMgr->ExtractTalentTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
		sTemplateNpcMgr->ExtractGlyphsTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
		return true;
	}

	// BEASTMASTERY HUNTER
	static bool HandleCreateHunterBeastmasteryItemSetCommand(ChatHandler* handler, const char* args)
	{
		Player* player = handler->GetSession()->GetPlayer();
		if (player->getClass() != CLASS_HUNTER)
		{
			player->GetSession()->SendAreaTriggerMessage("You're not a hunter!");
			return false;
		}
		player->SaveToDB();
		sTemplateNpcMgr->sTalentsSpec = "Beastmastery";
		sTemplateNpcMgr->OverwriteTemplate(player, sTemplateNpcMgr->sTalentsSpec);
		sTemplateNpcMgr->ExtractGearTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
		sTemplateNpcMgr->ExtractTalentTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
		sTemplateNpcMgr->ExtractGlyphsTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
		return true;
	}

	// SURVIVAL HUNTER
	static bool HandleCreateHunterSurvivalItemSetCommand(ChatHandler* handler, const char* args)
	{
		Player* player = handler->GetSession()->GetPlayer();
		if (player->getClass() != CLASS_HUNTER)
		{
			player->GetSession()->SendAreaTriggerMessage("You're not a hunter!");
			return false;
		}
		player->SaveToDB();
		sTemplateNpcMgr->sTalentsSpec = "Survival";
		sTemplateNpcMgr->OverwriteTemplate(player, sTemplateNpcMgr->sTalentsSpec);
		sTemplateNpcMgr->ExtractGearTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
		sTemplateNpcMgr->ExtractTalentTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
		sTemplateNpcMgr->ExtractGlyphsTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
		return true;
	}

	// ASSASSINATION ROGUE
	static bool HandleCreateRogueAssassinationItemSetCommand(ChatHandler* handler, const char* args)
	{
		Player* player = handler->GetSession()->GetPlayer();
		if (player->getClass() != CLASS_ROGUE)
		{
			player->GetSession()->SendAreaTriggerMessage("You're not a rogue!");
			return false;
		}
		player->SaveToDB();
		sTemplateNpcMgr->sTalentsSpec = "Assassination";
		sTemplateNpcMgr->OverwriteTemplate(player, sTemplateNpcMgr->sTalentsSpec);
		sTemplateNpcMgr->ExtractGearTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
		sTemplateNpcMgr->ExtractTalentTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
		sTemplateNpcMgr->ExtractGlyphsTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
		return true;
	}

	// COMBAT ROGUE
	static bool HandleCreateRogueCombatItemSetCommand(ChatHandler* handler, const char* args)
	{
		Player* player = handler->GetSession()->GetPlayer();
		if (player->getClass() != CLASS_ROGUE)
		{
			player->GetSession()->SendAreaTriggerMessage("You're not a rogue!");
			return false;
		}
		player->SaveToDB();
		sTemplateNpcMgr->sTalentsSpec = "Combat";
		sTemplateNpcMgr->OverwriteTemplate(player, sTemplateNpcMgr->sTalentsSpec);
		sTemplateNpcMgr->ExtractGearTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
		sTemplateNpcMgr->ExtractTalentTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
		sTemplateNpcMgr->ExtractGlyphsTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
		return true;
	}

	// SUBTLETY ROGUE
	static bool HandleCreateRogueSubtletyItemSetCommand(ChatHandler* handler, const char* args)
	{
		Player* player = handler->GetSession()->GetPlayer();
		if (player->getClass() != CLASS_ROGUE)
		{
			player->GetSession()->SendAreaTriggerMessage("You're not a rogue!");
			return false;
		}
		player->SaveToDB();
		sTemplateNpcMgr->sTalentsSpec = "Subtlety";
		sTemplateNpcMgr->OverwriteTemplate(player, sTemplateNpcMgr->sTalentsSpec);
		sTemplateNpcMgr->ExtractGearTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
		sTemplateNpcMgr->ExtractTalentTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
		sTemplateNpcMgr->ExtractGlyphsTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
		return true;
	}

	// BLOOD DEATHKNIGHT
	static bool HandleCreateDeathKnightBloodItemSetCommand(ChatHandler* handler, const char* args)
	{
		Player* player = handler->GetSession()->GetPlayer();
		if (player->getClass() != CLASS_DEATH_KNIGHT)
		{
			player->GetSession()->SendAreaTriggerMessage("You're not a death knight!");
			return false;
		}
		player->SaveToDB();
		sTemplateNpcMgr->sTalentsSpec = "Blood";
		sTemplateNpcMgr->OverwriteTemplate(player, sTemplateNpcMgr->sTalentsSpec);
		sTemplateNpcMgr->ExtractGearTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
		sTemplateNpcMgr->ExtractTalentTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
		sTemplateNpcMgr->ExtractGlyphsTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
		return true;
	}

	// FROST DEATHKNIGHT
	static bool HandleCreateDeathKnightFrostItemSetCommand(ChatHandler* handler, const char* args)
	{
		Player* player = handler->GetSession()->GetPlayer();
		if (player->getClass() != CLASS_DEATH_KNIGHT)
		{
			player->GetSession()->SendAreaTriggerMessage("You're not a death knight!");
			return false;
		}
		sTemplateNpcMgr->sTalentsSpec = "Frost";
		player->SaveToDB();
		sTemplateNpcMgr->OverwriteTemplate(player, sTemplateNpcMgr->sTalentsSpec);
		sTemplateNpcMgr->ExtractGearTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
		sTemplateNpcMgr->ExtractTalentTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
		sTemplateNpcMgr->ExtractGlyphsTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
		return true;
	}

	// UNHOLY DEATHKNIGHT
	static bool HandleCreateDeathKnightUnholyItemSetCommand(ChatHandler* handler, const char* args)
	{
		Player* player = handler->GetSession()->GetPlayer();
		if (player->getClass() != CLASS_DEATH_KNIGHT)
		{
			player->GetSession()->SendAreaTriggerMessage("You're not a death knight!");
			return false;
		}
		sTemplateNpcMgr->sTalentsSpec = "Unholy";
		player->SaveToDB();
		sTemplateNpcMgr->OverwriteTemplate(player, sTemplateNpcMgr->sTalentsSpec);
		sTemplateNpcMgr->ExtractGearTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
		sTemplateNpcMgr->ExtractTalentTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
		sTemplateNpcMgr->ExtractGlyphsTemplateToDB(player, sTemplateNpcMgr->sTalentsSpec);
		return true;
	}
};

enum Specializations{
    PRIEST_DISCIPLINE,
    PRIEST_HOLY,
    PRIEST_SHADOW,
    PALADIN_HOLY,
    PALADIN_PROTECTION,
    PALADIN_RETRIBUTION,
    WARRIOR_ARMS,
    WARRIOR_FURY,
    WARRIOR_PROTECTION,
    MAGE_ARCANE,
    MAGE_FIRE,
    MAGE_FROST,
    WARLOCK_AFFLICTION,
    WARLOCK_DEMONOLOGY,
    WARLOCK_DESTRUCTION,
    SHAMAN_ELEMENTAL,
    SHAMAN_ENHANCEMENT,
    SHAMAN_RESTORATION,
    DRUID_BALANCE,
    DRUID_FERAL,
    DRUID_RESTORATION,
    HUNTER_MARKSMANSHIP,
    HUNTER_BEASTMASTERY,
    HUNTER_SURVIVAL,
    ROGUE_ASSASSINATION,
    ROGUE_COMBAT,
    ROGUE_SUBTLETY,
    DK_BLOOD,
    DK_FROST,
    DK_UNHOLY
};

// Creature Script * TemplateNPC *
class TemplateNPC : public CreatureScript {
  public:
    TemplateNPC() : CreatureScript("TemplateNPC") { }

    bool OnGossipHello(Player* player, Creature* creature) {
        switch (player->getClass()) {
        case CLASS_PRIEST: {
            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD, "|cff00ff00|TInterface\\icons\\spell_holy_wordfortitude:26:26:-13:1|t|r Use Discipline ",GOSSIP_SENDER_MAIN, PRIEST_DISCIPLINE);
			//player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD, "|cff00ff00|TInterface\\icons\\ability_marksmanship:26:26:-13:1|t|r Use Discipline (Talents)",GOSSIP_SENDER_MAIN, PRIEST_DISCIPLINE_TALENTS);
            //player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD, "|cff00ff00|TInterface\\icons\\spell_holy_holybolt:26:26:-13:1|t|r Use Holy ",GOSSIP_SENDER_MAIN, PRIEST_HOLY);
            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD, "|cff00ff00|TInterface\\icons\\spell_shadow_shadowwordpain:26:26:-13:1|t|r Use Shadow ",GOSSIP_SENDER_MAIN, PRIEST_SHADOW);
		//	player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD, "|cff00ff00|TInterface\\icons\\ability_marksmanship:26:26:-13:1|t|r Use Shadow (Talents)", GOSSIP_SENDER_MAIN, PRIEST_SHADOW_TALENTS);
        }
        break;
        case CLASS_PALADIN: {
            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD, "|cff00ff00|TInterface\\icons\\spell_holy_holybolt:26:26:-13:1|t|r Use Holy ",GOSSIP_SENDER_MAIN, PALADIN_HOLY);
		//	player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD, "|cff00ff00|TInterface\\icons\\ability_marksmanship:26:26:-13:1|t|r Use Holy (Talents)", GOSSIP_SENDER_MAIN, PALADIN_HOLY_TALENTS);
            //player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD, "|cff00ff00|TInterface\\icons\\ability_paladin_shieldofthetemplar:26:26:-13:1|t|r Use Protection ",GOSSIP_SENDER_MAIN, PALADIN_PROTECTION);
            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD, "|cff00ff00|TInterface\\icons\\spell_holy_auraoflight:26:26:-13:1|t|r Use Retribution ",GOSSIP_SENDER_MAIN, PALADIN_RETRIBUTION);
		//	player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD, "|cff00ff00|TInterface\\icons\\ability_marksmanship:26:26:-13:1|t|r Use Retribution (Talents)", GOSSIP_SENDER_MAIN, PALADIN_RETRIBUTION_TALENTS);
        }
        break;
        case CLASS_WARRIOR: {
            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD, "|cff00ff00|TInterface\\icons\\ability_warrior_innerrage:26:26:-13:1|t|r Use Fury ",GOSSIP_SENDER_MAIN, WARRIOR_FURY);
		//	player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD, "|cff00ff00|TInterface\\icons\\ability_marksmanship:26:26:-13:1|t|r Use Fury (Talents)", GOSSIP_SENDER_MAIN, WARRIOR_FURY_TALENTS);
            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD, "|cff00ff00|TInterface\\icons\\ability_warrior_savageblow:26:26:-13:1|t|r Use Arms ",GOSSIP_SENDER_MAIN, WARRIOR_ARMS);
			//player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD, "|cff00ff00|TInterface\\icons\\ability_marksmanship:26:26:-13:1|t|r Use Arms (Talents)", GOSSIP_SENDER_MAIN, WARRIOR_ARMS_TALENTS);
           // player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD, "|cff00ff00|TInterface\\icons\\ability_warrior_defensivestance:26:26:-13:1|t|r Use Protection ",GOSSIP_SENDER_MAIN, WARRIOR_PROTECTION);
        }
        break;
        case CLASS_MAGE: {
           // player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD, "|cff00ff00|TInterface\\icons\\spell_holy_magicalsentry:26:26:-13:1|t|r Use Arcane ",GOSSIP_SENDER_MAIN, MAGE_ARCANE);
            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD, "|cff00ff00|TInterface\\icons\\spell_fire_flamebolt:26:26:-13:1|t|r Use Fire ",GOSSIP_SENDER_MAIN, MAGE_FIRE);
            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD, "|cff00ff00|TInterface\\icons\\spell_frost_frostbolt02:26:26:-13:1|t|r Use Frost ",GOSSIP_SENDER_MAIN, MAGE_FROST);
        }
        break;
        case CLASS_WARLOCK: {
            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD, "|cff00ff00|TInterface\\icons\\spell_shadow_deathcoil:26:26:-13:1|t|r Use Affliction ",GOSSIP_SENDER_MAIN, WARLOCK_AFFLICTION);
           // player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD, "|cff00ff00|TInterface\\icons\\spell_shadow_metamorphosis:26:26:-13:1|t|r Use Demonology ",GOSSIP_SENDER_MAIN, WARLOCK_DEMONOLOGY);
            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD, "|cff00ff00|TInterface\\icons\\spell_shadow_rainoffire:26:26:-13:1|t|r Use Destruction ",GOSSIP_SENDER_MAIN, WARLOCK_DESTRUCTION);
        }
        break;
        case CLASS_SHAMAN: {
            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD, "|cff00ff00|TInterface\\icons\\spell_nature_lightning:26:26:-13:1|t|r Use Elemental ",GOSSIP_SENDER_MAIN, SHAMAN_ELEMENTAL);
            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD, "|cff00ff00|TInterface\\icons\\spell_nature_lightningshield:26:26:-13:1|t|r Use Enhancement ",GOSSIP_SENDER_MAIN, SHAMAN_ENHANCEMENT);
            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD, "|cff00ff00|TInterface\\icons\\spell_nature_magicimmunity:26:26:-13:1|t|r Use Restoration ",GOSSIP_SENDER_MAIN, SHAMAN_RESTORATION);
        }
        break;
        case CLASS_DRUID: {
            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD, "|cff00ff00|TInterface\\icons\\spell_nature_starfall:26:26:-13:1|t|r Use Balance ",GOSSIP_SENDER_MAIN, DRUID_BALANCE);
            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD, "|cff00ff00|TInterface\\icons\\ability_racial_bearform:26:26:-13:1|t|r Use Feral ",GOSSIP_SENDER_MAIN, DRUID_FERAL);
            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD, "|cff00ff00|TInterface\\icons\\spell_nature_healingtouch:26:26:-13:1|t|r Use Restoration ",GOSSIP_SENDER_MAIN, DRUID_RESTORATION);
        }
        break;
        case CLASS_HUNTER: {
            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD, "|cff00ff00|TInterface\\icons\\ability_hunter_focusedaim:26:26:-13:1|t|r Use Markmanship ",GOSSIP_SENDER_MAIN, HUNTER_MARKSMANSHIP);
            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD, "|cff00ff00|TInterface\\icons\\ability_hunter_bestialdiscipline:26:26:-13:1|t|r Use Beastmastery ",GOSSIP_SENDER_MAIN, HUNTER_BEASTMASTERY);
           // player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD, "|cff00ff00|TInterface\\icons\\ability_hunter_camouflage:26:26:-13:1|t|r Use Survival ",GOSSIP_SENDER_MAIN, HUNTER_SURVIVAL);
        }
        break;
        case CLASS_ROGUE: {
           // player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD, "|cff00ff00|TInterface\\icons\\ability_rogue_eviscerate:26:26:-13:1|t|r Use Assasination ",GOSSIP_SENDER_MAIN, ROGUE_ASSASSINATION);
            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD, "|cff00ff00|TInterface\\icons\\ability_backstab:26:26:-13:1|t|r Use Combat ",GOSSIP_SENDER_MAIN, ROGUE_COMBAT);
            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD, "|cff00ff00|TInterface\\icons\\ability_stealth:26:26:-13:1|t|r Use Subtlety ",GOSSIP_SENDER_MAIN, ROGUE_SUBTLETY);
        }
        break;
        case CLASS_DEATH_KNIGHT: {
           // player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD, "|cff00ff00|TInterface\\icons\\spell_deathknight_bloodpresence:26:26:-13:1|t|r Use Blood ",GOSSIP_SENDER_MAIN, DK_BLOOD);
            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD, "|cff00ff00|TInterface\\icons\\spell_deathknight_frostpresence:26:26:-13:1|t|r Use Frost ",GOSSIP_SENDER_MAIN, DK_FROST);
            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD, "|cff00ff00|TInterface\\icons\\spell_deathknight_unholypresence:36:36:-13:1|t|r Use Unholy ",GOSSIP_SENDER_MAIN, DK_UNHOLY);
        }
        break;
        }
        player->SEND_GOSSIP_MENU(60025, creature->GetGUID());
        return true;
    }

	// Merge
    void EquipFullTemplateGear(Player* player, std::string& playerSpecStr) 
	{ 
        if (sTemplateNpcMgr->CanEquipTemplate(player, playerSpecStr) == false) {
            player->GetSession()->SendAreaTriggerMessage("There's no templates for %s specialization yet.", playerSpecStr.c_str());
            return;
        }

        // Don't let players to use Template feature while wearing some gear
        for (uint8 i = EQUIPMENT_SLOT_START; i < EQUIPMENT_SLOT_END; ++i) {
            if (Item* haveItemEquipped = player->GetItemByPos(INVENTORY_SLOT_BAG_0, i)) {
                if (haveItemEquipped) {
                    player->GetSession()->SendAreaTriggerMessage("You need to remove all your equipped items in order to use this feature!");
                    player->CLOSE_GOSSIP_MENU();
                    return;
                }
            }
        }

         //Don't let players to use Template feature after spending some talent points
        if (player->GetFreeTalentPoints() < 41) {
            player->GetSession()->SendAreaTriggerMessage("You have already spent some talent points. You need to reset your talents first!");
            player->CLOSE_GOSSIP_MENU();
            return;
        }

        sTemplateNpcMgr->LearnTemplateTalents(player);
        sTemplateNpcMgr->LearnTemplateGlyphs(player);
        sTemplateNpcMgr->EquipTemplateGear(player);
        sTemplateNpcMgr->LearnPlateMailSpells(player);

        LearnWeaponSkills(player);

        player->GetSession()->SendAreaTriggerMessage("Successfuly equipped %s %s template!", playerSpecStr.c_str(), sTemplateNpcMgr->GetClassString(player).c_str());
    }

    bool OnGossipSelect(Player* player, Creature* creature, uint32 /*uiSender*/, uint32 uiAction) {
        player->PlayerTalkClass->ClearMenus();

        if (!player || !creature)
            return true;

		switch (uiAction) {
		case PRIEST_DISCIPLINE: // Use Discipline Priest Spec
			sTemplateNpcMgr->sTalentsSpec = "Discipline";
			player->SetPrimaryTalentTree(player->GetActiveSpecialization(), SPEC_PRIEST_DISCIPLINE);
			if (!player->GetPrimaryTalentTree(player->GetActiveSpecialization() == SPEC_PRIEST_DISCIPLINE))
			{
				player->LearnSpell(17, false);
				player->LearnSpell(109964, false);
				player->LearnSpell(47515, false);
				player->LearnSpell(47540, false);
				player->LearnSpell(62618, false);
			}
			break;

		case PRIEST_HOLY: // Use Holy Priest Spec
			sTemplateNpcMgr->sTalentsSpec = "Holy";
			player->SetPrimaryTalentTree(player->GetActiveSpecialization(), SPEC_PRIEST_HOLY);
			if (!player->GetPrimaryTalentTree(player->GetActiveSpecialization() == SPEC_PRIEST_HOLY))
			{
				player->LearnSpell(34861, false);
				player->LearnSpell(81206, false);
				player->LearnSpell(126135, false);
				player->LearnSpell(64843, false);
				player->LearnSpell(2061, false);
			}
			break;

		case PRIEST_SHADOW: // Use Shadow Priest Spec
			sTemplateNpcMgr->sTalentsSpec = "Shadow";
			player->SetPrimaryTalentTree(player->GetActiveSpecialization(), SPEC_PRIEST_SHADOW);
			if (!player->GetPrimaryTalentTree(player->GetActiveSpecialization() == SPEC_PRIEST_SHADOW))
			{
				player->LearnSpell(589, false);
				player->LearnSpell(15407, false);
				player->LearnSpell(34914, false);
				player->LearnSpell(95740, false);
				player->LearnSpell(2944, false);
				player->LearnSpell(8092, false);
			}
			break;

		case PALADIN_HOLY: // Use Holy Paladin Spec
			sTemplateNpcMgr->sTalentsSpec = "Holy";
			player->SetPrimaryTalentTree(player->GetActiveSpecialization(), SPEC_PALADIN_HOLY);
			if (!player->GetPrimaryTalentTree(player->GetActiveSpecialization() == SPEC_PALADIN_HOLY))
			{
				player->LearnSpell(20473, false);
				player->LearnSpell(85673, false);
				player->LearnSpell(82327, false);
				player->LearnSpell(53563, false);
			}
			break;

		case PALADIN_PROTECTION: // Use Protection Paladin Spec
			sTemplateNpcMgr->sTalentsSpec = "Protection";
			player->SetPrimaryTalentTree(player->GetActiveSpecialization(), SPEC_PALADIN_PROTECTION);
			if (!player->GetPrimaryTalentTree(player->GetActiveSpecialization() == SPEC_PALADIN_PROTECTION))
			{
				player->LearnSpell(35395, false);
				player->LearnSpell(20271, false);
				player->LearnSpell(31935, false);
				player->LearnSpell(53600, false);
			}
			break;

		case PALADIN_RETRIBUTION: // Use Retribution Paladin Spec
			sTemplateNpcMgr->sTalentsSpec = "Retribution";
			player->SetPrimaryTalentTree(player->GetActiveSpecialization(), SPEC_PALADIN_RETRIBUTION);
			if (!player->GetPrimaryTalentTree(player->GetActiveSpecialization() == SPEC_PALADIN_RETRIBUTION))
			{
				player->LearnSpell(35395, false);
				player->LearnSpell(20271, false);
				player->LearnSpell(85256, false);
				player->LearnSpell(87138, false);
				player->LearnSpell(24275, false);
			}
			break;

		case WARRIOR_FURY: // Use Fury Warrior Spec
			sTemplateNpcMgr->sTalentsSpec = "Fury";
			player->SetPrimaryTalentTree(player->GetActiveSpecialization(), SPEC_WARRIOR_FURY);
			if (!player->GetPrimaryTalentTree(player->GetActiveSpecialization() == SPEC_WARRIOR_FURY))
			{
				player->LearnSpell(23881, false);
				player->LearnSpell(100130, false);
				player->LearnSpell(23588, false);
				player->LearnSpell(85288, false);
			}
			break;

		case WARRIOR_ARMS: // Use Arms Warrior Spec
			sTemplateNpcMgr->sTalentsSpec = "Arms";
			player->SetPrimaryTalentTree(player->GetActiveSpecialization(), SPEC_WARRIOR_ARMS);
			if (!player->GetPrimaryTalentTree(player->GetActiveSpecialization() == SPEC_WARRIOR_ARMS))
			{
				player->LearnSpell(12294, false);
				player->LearnSpell(1464, false);
				player->LearnSpell(86346, false);
				player->LearnSpell(7384, false);
			}
			break;

		case WARRIOR_PROTECTION: // Use Protection Warrior Spec
			sTemplateNpcMgr->sTalentsSpec = "Protection";
			player->SetPrimaryTalentTree(player->GetActiveSpecialization(), SPEC_WARRIOR_PROTECTION);
			if (!player->GetPrimaryTalentTree(player->GetActiveSpecialization() == SPEC_WARRIOR_PROTECTION))
			{
			// SPells toadd/
			}
			break;

		case MAGE_ARCANE: // Use Arcane Mage Spec
			sTemplateNpcMgr->sTalentsSpec = "Arcane";
			player->SetPrimaryTalentTree(player->GetActiveSpecialization(), SPEC_MAGE_ARCANE);
			if (!player->GetPrimaryTalentTree(player->GetActiveSpecialization() == SPEC_MAGE_ARCANE))
			{
				player->LearnSpell(30451, false);
				player->LearnSpell(114664, false);
				player->LearnSpell(44425, false);
				player->LearnSpell(5143, false);
			}
            break;

        case MAGE_FIRE: // Use Fire Mage Spec
            sTemplateNpcMgr->sTalentsSpec = "Fire";
			player->SetPrimaryTalentTree(player->GetActiveSpecialization(), SPEC_MAGE_FIRE);
			if (!player->GetPrimaryTalentTree(player->GetActiveSpecialization() == SPEC_MAGE_FIRE))
			{
				player->LearnSpell(133, false);
				player->LearnSpell(11366, false);
				player->LearnSpell(108853, false);
				player->LearnSpell(11129, false);
			}
            break;

        case MAGE_FROST: // Use Frost Mage Spec
            sTemplateNpcMgr->sTalentsSpec = "Frost";
			player->SetPrimaryTalentTree(player->GetActiveSpecialization(), SPEC_MAGE_FROST);
			if (!player->GetPrimaryTalentTree(player->GetActiveSpecialization() == SPEC_MAGE_FROST))
			{
				player->LearnSpell(116, false);
				player->LearnSpell(31687, false);
				player->LearnSpell(112965, false);
				player->LearnSpell(30455, false);
			}
            break;

        case WARLOCK_AFFLICTION: // Use Affliction Warlock Spec
            sTemplateNpcMgr->sTalentsSpec = "Affliction";
			player->SetPrimaryTalentTree(player->GetActiveSpecialization(), SPEC_WARLOCK_AFFLICTION);
			if (!player->GetPrimaryTalentTree(player->GetActiveSpecialization() == SPEC_WARLOCK_AFFLICTION))
			{
				player->LearnSpell(172, false);
				player->LearnSpell(30108, false);
				player->LearnSpell(1120, false);
				player->LearnSpell(980, false);
				player->LearnSpell(103103, false);
				player->LearnSpell(48181, false);
			}
            break;

        case WARLOCK_DEMONOLOGY: // Use Demonology Warlock Spec
            sTemplateNpcMgr->sTalentsSpec = "Demonology";
			player->SetPrimaryTalentTree(player->GetActiveSpecialization(), SPEC_WARLOCK_DEMONOLOGY);
			if (!player->GetPrimaryTalentTree(player->GetActiveSpecialization() == SPEC_WARLOCK_DEMONOLOGY))
			{
				player->LearnSpell(103958, false);
				player->LearnSpell(105174, false);
				player->LearnSpell(122351, false);
				player->LearnSpell(104315, false);
				player->LearnSpell(30146, false);
				player->LearnSpell(114592, false);
			}
            break;

        case WARLOCK_DESTRUCTION: // Use Destruction Warlock Spec
            sTemplateNpcMgr->sTalentsSpec = "Destruction";
			player->SetPrimaryTalentTree(player->GetActiveSpecialization(), SPEC_WARLOCK_DESTRUCTION);
			if (!player->GetPrimaryTalentTree(player->GetActiveSpecialization() == SPEC_WARLOCK_DESTRUCTION))
			{
				player->LearnSpell(348, false);
				player->LearnSpell(29722, false);
				player->LearnSpell(111546, false);
				player->LearnSpell(17962, false);
				player->LearnSpell(116858, false);
				player->LearnSpell(108647, false);
			}
            break;

        case SHAMAN_ELEMENTAL: // Use Elemental Shaman Spec
            sTemplateNpcMgr->sTalentsSpec = "Elemental";
			player->SetPrimaryTalentTree(player->GetActiveSpecialization(), SPEC_SHAMAN_ELEMENTAL);
			if (!player->GetPrimaryTalentTree(player->GetActiveSpecialization() == SPEC_SHAMAN_ELEMENTAL))
			{
				player->LearnSpell(403, false);
				player->LearnSpell(51505, false);
				player->LearnSpell(88766, false);
				player->LearnSpell(61882, false);
			}
            break;

        case SHAMAN_ENHANCEMENT: // Use Enhancement Shaman Spec
            sTemplateNpcMgr->sTalentsSpec = "Enhancement";
			player->SetPrimaryTalentTree(player->GetActiveSpecialization(), SPEC_SHAMAN_ENHANCEMENT);
			if (!player->GetPrimaryTalentTree(player->GetActiveSpecialization() == SPEC_SHAMAN_ENHANCEMENT))
			{
				player->LearnSpell(86629, false);
				player->LearnSpell(17364, false);
				player->LearnSpell(51530, false);
				player->LearnSpell(60103, false);
				player->LearnSpell(51533, false);
			}
            break;

        case SHAMAN_RESTORATION: // Use Restoration Shaman Spec
            sTemplateNpcMgr->sTalentsSpec = "Restoration";
			player->SetPrimaryTalentTree(player->GetActiveSpecialization(), SPEC_SHAMAN_RESTORATION);
			if (!player->GetPrimaryTalentTree(player->GetActiveSpecialization() == SPEC_SHAMAN_RESTORATION))
			{
				player->LearnSpell(974, false);
				player->LearnSpell(61295, false);
				player->LearnSpell(77472, false);
				player->LearnSpell(98008, false);
			}
            break;
        case DRUID_BALANCE: // Use Balance Druid Spec
            sTemplateNpcMgr->sTalentsSpec = "Balance";
			player->SetPrimaryTalentTree(player->GetActiveSpecialization(), SPEC_DRUID_BALANCE);
			if (!player->GetPrimaryTalentTree(player->GetActiveSpecialization() == SPEC_DRUID_BALANCE))
			{
				player->LearnSpell(51576, false);
				player->LearnSpell(2912, false);
				player->LearnSpell(78674, false);
				player->LearnSpell(8921, false);
				player->LearnSpell(79577, false);
			}
            break;

        case DRUID_FERAL: // Use Feral Druid Spec
            sTemplateNpcMgr->sTalentsSpec = "Feral";
			player->SetPrimaryTalentTree(player->GetActiveSpecialization(), SPEC_DRUID_CAT);
			if (!player->GetPrimaryTalentTree(player->GetActiveSpecialization() == SPEC_DRUID_CAT))
			{
				player->LearnSpell(33917, false);
				player->LearnSpell(1822, false);
				player->LearnSpell(1079, false);
				player->LearnSpell(5221, false);
				player->LearnSpell(52610, false);
			}
            break;
        case DRUID_RESTORATION: // Use Restoration Druid Spec
            sTemplateNpcMgr->sTalentsSpec = "Restoration";
			player->SetPrimaryTalentTree(player->GetActiveSpecialization(), SPEC_DRUID_RESTORATION);
			if (!player->GetPrimaryTalentTree(player->GetActiveSpecialization() == SPEC_DRUID_RESTORATION))
			{
				player->LearnSpell(774, false);
				player->LearnSpell(33763, false);
				player->LearnSpell(18562, false);
				player->LearnSpell(5185, false);
			}
            break;

        case HUNTER_MARKSMANSHIP: // Use Marksmanship Hunter Spec
            sTemplateNpcMgr->sTalentsSpec = "Marksmanship";
			player->SetPrimaryTalentTree(player->GetActiveSpecialization(), SPEC_HUNTER_MARKSMAN);
			if (!player->GetPrimaryTalentTree(player->GetActiveSpecialization() == SPEC_HUNTER_MARKSMAN))
			{
				player->LearnSpell(19434, false);
				player->LearnSpell(56641, false);
				player->LearnSpell(3044, false);
				player->LearnSpell(53209, false);
			}
            break;

        case HUNTER_BEASTMASTERY: // Use Beastmastery Hunter Spec
            sTemplateNpcMgr->sTalentsSpec = "Beastmastery";
			player->SetPrimaryTalentTree(player->GetActiveSpecialization(), SPEC_HUNTER_BEASTMASTER);
			if (!player->GetPrimaryTalentTree(player->GetActiveSpecialization() == SPEC_HUNTER_BEASTMASTER))
			{
				player->LearnSpell(34026, false);
				player->LearnSpell(77767, false);
				player->LearnSpell(3044, false);
				player->LearnSpell(19574, false);

			}
            break;

        case HUNTER_SURVIVAL: // Use Survival Hunter Spec
            sTemplateNpcMgr->sTalentsSpec = "Survival";
			player->SetPrimaryTalentTree(player->GetActiveSpecialization(), SPEC_HUNTER_SURVIVAL);
			if (!player->GetPrimaryTalentTree(player->GetActiveSpecialization() == SPEC_WARRIOR_FURY))
			{
				player->LearnSpell(53301, false);
				player->LearnSpell(77767, false);
				player->LearnSpell(3674, false);
				player->LearnSpell(63458, false);
			}
            break;

        case ROGUE_ASSASSINATION: // Use Assassination Rogue Spec
            sTemplateNpcMgr->sTalentsSpec = "Assassination";
			player->SetPrimaryTalentTree(player->GetActiveSpecialization(), SPEC_ROGUE_ASSASSINATION);
			if (!player->GetPrimaryTalentTree(player->GetActiveSpecialization() == SPEC_ROGUE_ASSASSINATION))
			{
				player->LearnSpell(1329, false);
				player->LearnSpell(79134, false);
				player->LearnSpell(32645, false);
				player->LearnSpell(79140, false);
			}
            break;

        case ROGUE_COMBAT: // Use Combat Rogue Spec
            sTemplateNpcMgr->sTalentsSpec = "Combat";
			player->SetPrimaryTalentTree(player->GetActiveSpecialization(), SPEC_ROGUE_COMBAT);
			if (!player->GetPrimaryTalentTree(player->GetActiveSpecialization() == SPEC_ROGUE_COMBAT))
			{
				player->LearnSpell(13877, false);
				player->LearnSpell(35551, false);
				player->LearnSpell(84617, false);
				player->LearnSpell(51690, false);
			}
            break;

        case ROGUE_SUBTLETY: // Use Subtlety Rogue Spec
            sTemplateNpcMgr->sTalentsSpec = "Subtlety";
			player->SetPrimaryTalentTree(player->GetActiveSpecialization(), SPEC_ROGUE_SUBTLETY);
			if (!player->GetPrimaryTalentTree(player->GetActiveSpecialization() == SPEC_ROGUE_SUBTLETY))
			{
				player->LearnSpell(53, false);
				player->LearnSpell(91023, false);
				player->LearnSpell(16511, false);
				player->LearnSpell(51713, false);
			}
            break;

        case DK_BLOOD: // Use Blood DK Spec
            sTemplateNpcMgr->sTalentsSpec = "Blood";
			player->SetPrimaryTalentTree(player->GetActiveSpecialization(), SPEC_DK_BLOOD);
			if (!player->GetPrimaryTalentTree(player->GetActiveSpecialization() == SPEC_DK_BLOOD))
			{
				player->LearnSpell(49998, false);
				player->LearnSpell(55050, false);
				player->LearnSpell(56815, false);
				player->LearnSpell(55233, false);
				player->LearnSpell(48982, false);
				player->LearnSpell(49028, false);
			}
            break;

        case DK_FROST: // Use Frost DK Spec
            sTemplateNpcMgr->sTalentsSpec = "Frost";
			player->SetPrimaryTalentTree(player->GetActiveSpecialization(), SPEC_DK_FROST);
			if (!player->GetPrimaryTalentTree(player->GetActiveSpecialization() == SPEC_DK_FROST))
			{
				player->LearnSpell(49143, false);
				player->LearnSpell(49184, false);
				player->LearnSpell(49020, false);
				player->LearnSpell(51271, false);
			}
            break;

        case DK_UNHOLY: // Use Unholy DK Spec
            sTemplateNpcMgr->sTalentsSpec = "Unholy";
			player->SetPrimaryTalentTree(player->GetActiveSpecialization(), SPEC_DK_UNHOLY);
			if (!player->GetPrimaryTalentTree(player->GetActiveSpecialization() == SPEC_DK_UNHOLY))
			{
				player->LearnSpell(55090, false);
				player->LearnSpell(85948, false);
				player->LearnSpell(49572, false);
				player->LearnSpell(63560, false);
			}
            break;
        default: // Just in case
            player->GetSession()->SendAreaTriggerMessage("Something went wrong. Please report this on this forums. Error Code: 100");
            break;
        }

		EquipFullTemplateGear(player, sTemplateNpcMgr->sTalentsSpec);
		player->CLOSE_GOSSIP_MENU();

		/*player->UpdateSkillsForLevel();
		player->UpdateMastery();*/
        return true;
    }
};

void AddSC_TemplateNPC() {
	new TemplateNPC_Commands();
    new TemplateNPC();
}