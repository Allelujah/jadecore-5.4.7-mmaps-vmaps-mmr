/*
* Copyright (C) 2008-2012 TrinityCore <http://www.trinitycore.org/>
* Copyright (C) 2006-2009 ScriptDev2 <https://scriptdev2.svn.sourceforge.net/>
*
* This program is free software; you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation; either version 2 of the License, or (at your
* option) any later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
* more details.
*
* You should have received a copy of the GNU General Public License along
* with this program. If not, see <http://www.gnu.org/licenses/>.
*/

/* ScriptData
Name: Arena Spectator
%Complete: 100
Comment: Script allow spectate arena games
Category: Custom Script
EndScriptData */

#include "ScriptPCH.h"
#include "Player.h"
#include "Chat.h"
#include "Group.h"
#include "GroupMgr.h"
#include "Battleground.h"
#include "BattlegroundMgr.h"

class arena_spectator_commands : public CommandScript
{
public:
    arena_spectator_commands() : CommandScript("arena_spectator_commands") { }

    static bool HandleSpectateCommand(ChatHandler* handler, const char *args)
    {
        Player* playerTarget;
        uint64 target_guid;
        std::string target_name;
        if (!handler->extractPlayerTarget((char*)args, &playerTarget, &target_guid, &target_name))
            return false;

        Player* player = handler->GetSession()->GetPlayer();

        // Player checks
        if (playerTarget == player)
        {
            handler->SendSysMessage(LANG_CANT_TELEPORT_SELF);
            handler->SetSentErrorMessage(true);
            return false;
        }

        if (player->isInCombat())
        {
            handler->SendSysMessage(LANG_YOU_IN_COMBAT);
            handler->SetSentErrorMessage(true);
            return false;
        }

        if (!player->isAlive())
        {
            handler->SendSysMessage("You are dead.");
            handler->SetSentErrorMessage(true);
            return false;
        }

        if (player->GetPet())
        {
            handler->PSendSysMessage("You must hide your pet.");
            handler->SetSentErrorMessage(true);
            return false;
        }

        if (player->GetSession()->GetInterRealmBG())
        {
            handler->PSendSysMessage("You are already on interrealm server.");
            handler->SetSentErrorMessage(true);
            return false;
        }

        // Target Checks
        if (!playerTarget)
        {
            handler->PSendSysMessage("Target is not online or does not exist.");
            handler->SetSentErrorMessage(true);
            return false;
        }

        if (playerTarget->HasAura(32728) || playerTarget->HasAura(32727))
        {
            handler->PSendSysMessage("You can't do that. The arena match didn't start yet.");
            handler->SetSentErrorMessage(true);
            return false;
        }

        if (playerTarget->IsSpectator())
        {
            handler->PSendSysMessage("You can't do that. Your target is a spectator.");
            handler->SetSentErrorMessage(true);
            return false;
        }

        Map* cMap = playerTarget->GetMap();
        if (!cMap->IsBattleArena())
        {
            handler->PSendSysMessage("Player is not in an Arena match.");
            handler->SetSentErrorMessage(true);
            return false;
        }
        // Quick check to give player arena access
        Map* map = playerTarget->GetMap();
        if (map->IsBattlegroundOrArena())
        {
            player->SetBattlegroundId(playerTarget->GetBattlegroundId(), playerTarget->GetBattlegroundTypeId());
            // remember current position as entry point for return at bg end teleportation
            if (!player->GetMap()->IsBattlegroundOrArena())
                player->SetBattlegroundEntryPoint();
        }

        // Add player to the arena
        float x, y, z;
        playerTarget->GetContactPoint(player, x, y, z);

        player->TeleportTo(playerTarget->GetMapId(), x, y, z, player->GetAngle(playerTarget), TELEPORT_OPTION_GM_MODE);
        player->SetPhaseMask(playerTarget->GetPhaseMask(), true);
        player->SetSpectate(true);
        // Doesn't execute on join for some reason
        playerTarget->GetBattleground()->AddSpectator(player);
        player->SetSpeed(MOVE_RUN, 2.5);
        return true;

    }

    static bool HandleSpectateCancelCommand(ChatHandler* handler, const char* /*args*/)
    {
        Player* player = handler->GetSession()->GetPlayer();

        if (!player->IsSpectator())
        {
            handler->PSendSysMessage("You are not spectator.");
            handler->SetSentErrorMessage(true);
            return false;
        }

        if (!player->GetBattleground())
        {
            handler->PSendSysMessage("You are not spectator.");
            handler->SetSentErrorMessage(true);
            return false;
        }

        player->GetBattleground()->RemoveSpectator(player);
        player->CancelSpectate();
        player->TeleportToBGEntryPoint();

        return true;
    }

    static bool HandleSpectateFromCommand(ChatHandler* handler, const char *args)
    {
        Player* target;
        uint64 target_guid;
        std::string target_name;
        if (!handler->extractPlayerTarget((char*)args, &target, &target_guid, &target_name))
            return false;

        Player* player = handler->GetSession()->GetPlayer();

        // Was keeping GMs from resetting their spectate
        /*if (!target || target->HasGameMasterMode())
        {
        handler->PSendSysMessage("Cant find player.");
        handler->SetSentErrorMessage(true);
        return false;
        }*/

        if (!player->IsSpectator())
        {
            handler->PSendSysMessage(LANG_SPECTATOR_NOT_SPECTATOR);
            handler->SetSentErrorMessage(true);
            return false;
        }

        if (target->IsSpectator() && target != player)
        {
            handler->PSendSysMessage("Can`t do that. Your target is spectator.");
            handler->SetSentErrorMessage(true);
            return false;
        }

        if (player->GetMap() != target->GetMap())
        {
            handler->PSendSysMessage("Cant do that. Different arenas?");
            handler->SetSentErrorMessage(true);
            return false;
        }

        // check for arena preperation
        // if exists than battle didn`t begin
        if (target->HasAura(32728) || target->HasAura(32727))
        {
            handler->PSendSysMessage(LANG_SPECTATOR_BG_NOT_STARTED);
            handler->SetSentErrorMessage(true);
            return false;
        }

        if (target->HasAuraType(SPELL_AURA_MOD_STEALTH) || target->HasAuraType(SPELL_AURA_MOD_INVISIBILITY))
        {
            handler->PSendSysMessage(LANG_SPECTATOR_CANNT_SEE);
            handler->SetSentErrorMessage(true);
            return false;
        }

        (target == player && player->GetSpectator()) ? player->SetViewpoint(player->GetSpectator(), false) :
            player->SetViewpoint(target, true);
        return true;
    }

    static bool HandleSpectateResetCommand(ChatHandler* handler, const char *args)
    {
        Player* player = handler->GetSession()->GetPlayer();

        if (!player)
        {
            handler->PSendSysMessage("Cant find player.");
            handler->SetSentErrorMessage(true);
            return false;
        }

        if (!player->IsSpectator())
        {
            handler->PSendSysMessage(LANG_SPECTATOR_NOT_SPECTATOR);
            handler->SetSentErrorMessage(true);
            return false;
        }

        Battleground *bGround = player->GetBattleground();
        if (!bGround)
            return false;

        if (bGround->GetStatus() != STATUS_IN_PROGRESS)
            return true;

        player->SetViewpoint(player, true);

        /*for (Battleground::BattlegroundScoreMap::const_iterator itr = bGround->GetPlayerScoresBegin(); itr != bGround->GetPlayerScoresEnd(); ++itr)
        if (Player* tmpPlayer = ObjectAccessor::FindPlayer(itr->first))
        {
        uint32 tmpID = bGround->GetPlayerTeam(tmpPlayer->GetGUID());

        // generate addon massage
        uint64 pGuid = tmpPlayer->GetGUID();
        std::string pName = tmpPlayer->GetName();
        uint64 tGuid = NULL;

        if (Player* target = tmpPlayer->GetSelectedPlayer())
        tGuid = target->GetGUID();

        SpectatorAddonMsg msg;
        msg.SetPlayer(pGuid);
        msg.SetName(pName);
        if (tGuid != NULL)
        msg.SetTarget(tGuid);
        if (Pet* pPet = tmpPlayer->GetPet())
        {
        msg.SetPet(pPet->GetCreatureTemplate()->family);
        msg.SetPetHP(pPet->GetHealthPct());
        }
        else
        {
        msg.SetPet(0);
        msg.SetPetHP(0);
        }
        msg.SetStatus(tmpPlayer->isAlive());
        msg.SetClass(tmpPlayer->getClass());
        msg.SetCurrentHP(tmpPlayer->GetHealth());
        msg.SetMaxHP(tmpPlayer->GetMaxHealth());
        Powers powerType = tmpPlayer->getPowerType();
        msg.SetMaxPower(tmpPlayer->GetMaxPower(powerType));
        msg.SetCurrentPower(tmpPlayer->GetPower(powerType));
        msg.SetPowerType(powerType);
        msg.SetTeam(tmpID);
        msg.SetEndTime(uint32(47 * MINUTE - bGround->GetElapsedTime() / IN_MILLISECONDS));
        msg.SendPacket(player->GetGUID());
        }*/

        return true;
    }

    ChatCommand* GetCommands() const
    {
        static ChatCommand spectateCommandTable[] =
        {
            { "player", SEC_PLAYER, true, &HandleSpectateCommand, "", NULL },
            { "view", SEC_PLAYER, true, &HandleSpectateFromCommand, "", NULL },
            { "reset", SEC_PLAYER, true, &HandleSpectateResetCommand, "", NULL },
            { "leave", SEC_PLAYER, true, &HandleSpectateCancelCommand, "", NULL },
            { NULL, 0, false, NULL, "", NULL }
        };

        static ChatCommand commandTable[] =
        {
            { "spectate", SEC_PLAYER, false, NULL, "", spectateCommandTable },
            { NULL, 0, false, NULL, "", NULL }
        };
        return commandTable;
    }
};

enum NpcSpectatorAtions {
    // will be used for scrolling
    NPC_SPECTATOR_ACTION_2V2_GAMES = 1000,  //NPC_SPECTATOR_ACTION_LIST_GAMES = 1000,
    NPC_SPECTATOR_ACTION_3V3_GAMES = 2000, // NPC_SPECTATOR_ACTION_LIST_TOP_GAMES = 2000,

    // Disabled till I fix 2v2 , 3v3
    //NPC_SPECTATOR_ACTION_5V5_GAMES = 3000,
    //NPC_SPECTATOR_ACTION_SPECIFIC  = 500,

    // NPC_SPECTATOR_ACTION_SELECTED_PLAYER + player.Guid()
    NPC_SPECTATOR_ACTION_SELECTED_PLAYER = 3000
};

const uint8  GamesOnPage = 15;

class npc_arena_spectator : public CreatureScript
{
public:
    npc_arena_spectator() : CreatureScript("npc_arena_spectator") { }

    bool OnGossipHello(Player* pPlayer, Creature* pCreature)
    {
        pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "|TInterface\\icons\\Achievement_Arena_2v2_7:28:28:-16|tGames: 2v2", GOSSIP_SENDER_MAIN, NPC_SPECTATOR_ACTION_2V2_GAMES);
        pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "|TInterface\\icons\\Achievement_Arena_3v3_7:28:28:-16|tGames: 3v3", GOSSIP_SENDER_MAIN, NPC_SPECTATOR_ACTION_3V3_GAMES);
        //pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "|TInterface\\icons\\Achievement_Arena_5v5_7:35:35:-30:0|tGames: 5v5", GOSSIP_SENDER_MAIN, NPC_SPECTATOR_ACTION_5V5_GAMES);
        //pPlayer->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_CHAT, "|TInterface\\icons\\Spell_Holy_DevineAegis:35:35:-30:0|tSpectate Specific Player.", GOSSIP_SENDER_MAIN, NPC_SPECTATOR_ACTION_SPECIFIC, "", 0, true);
        pPlayer->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, pCreature->GetGUID());
        return true;
    }

    bool OnGossipSelect(Player* player, Creature* creature, uint32 /*sender*/, uint32 action)
    {
        switch (action) {

        case NPC_SPECTATOR_ACTION_2V2_GAMES:
        {
            player->PlayerTalkClass->ClearMenus();
            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, " Refresh", GOSSIP_SENDER_MAIN, NPC_SPECTATOR_ACTION_2V2_GAMES);
            ShowPage(player, action - NPC_SPECTATOR_ACTION_2V2_GAMES, false);
            player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, creature->GetGUID());
        }
        break;

        case NPC_SPECTATOR_ACTION_3V3_GAMES:
        {
            player->PlayerTalkClass->ClearMenus();
            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "Refresh", GOSSIP_SENDER_MAIN, NPC_SPECTATOR_ACTION_3V3_GAMES);
            ShowPage(player, action - NPC_SPECTATOR_ACTION_3V3_GAMES, true);
            player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, creature->GetGUID());
        }
        break;

        }

        OnGossipHello(player, creature);
        return true;
    }

    std::string GetClassNameById(uint8 id)
    {
        std::string sClass = "";
        switch (id)
        {
        case CLASS_WARRIOR:         sClass = "Warrior ";        break;
        case CLASS_PALADIN:         sClass = "Paladin ";        break;
        case CLASS_HUNTER:          sClass = "Hunter ";         break;
        case CLASS_ROGUE:           sClass = "Rogue ";          break;
        case CLASS_PRIEST:          sClass = "Priest ";         break;
        case CLASS_DEATH_KNIGHT:    sClass = "DKnight ";        break;
        case CLASS_SHAMAN:          sClass = "Shaman ";         break;
        case CLASS_MAGE:            sClass = "Mage ";           break;
        case CLASS_WARLOCK:         sClass = "Warlock ";        break;
        case CLASS_DRUID:           sClass = "Druid ";          break;
        case CLASS_MONK:            sClass = "Monk ";           break;
        }
        return sClass;
    }

    std::string GetGamesStringData(Battleground* team, uint16 mmr, uint16 mmrTwo)
    {
        std::string teamsMember[BG_TEAMS_COUNT];
        uint32 firstTeamId = 0;

        for (Battleground::BattlegroundPlayerMap::const_iterator itr = team->GetPlayers().begin(); itr != team->GetPlayers().end(); ++itr)
        if (Player* player = ObjectAccessor::FindPlayer(itr->first))
        {
            if (player->IsSpectator())
                continue;

            uint32 team = itr->second.Team;
            if (!firstTeamId)
                firstTeamId = team;

            teamsMember[firstTeamId == team] += GetSpecClassNameById(player->getClass(), player->GetSpecializationId(player->GetActiveSpecialization()));
        }

        std::string data = teamsMember[0] + "(";
        std::stringstream ss;
        std::stringstream sstwo;
        ss << mmr;
        sstwo << mmrTwo;
        data += ss.str();
        data += ") - ";
        data += teamsMember[1] + "(" + sstwo.str();
        data += ")";
        return data;
    }

    ObjectGuid GetFirstPlayerGuid(Battleground* team)
    {
        for (Battleground::BattlegroundPlayerMap::const_iterator itr = team->GetPlayers().begin(); itr != team->GetPlayers().end(); ++itr)
        if (Player* player = ObjectAccessor::FindPlayer(itr->first))
            return itr->first;
        return NULL;
    }

    void ShowPage(Player* player, uint16 page, bool IsTop)
    {
        ChatHandler(player->GetSession()).PSendSysMessage("Test 0"); // METHOD TEST
        uint32 firstTeamId = 0;
        uint16 TypeOne = 0;
        uint16 TypeTwo = 0;
        uint16 TypeThree = 0;
        uint16 mmr = 0;
        uint16 mmrTwo = 0;
        bool haveNextPage = false;

        for (uint16 i = 0; i <= MAX_BATTLEGROUND_TYPE_ID; ++i)
        {
            //ChatHandler(player->GetSession()).PSendSysMessage("Test 0.1"); // METHOD TEST
            if (!sBattlegroundMgr->IsArenaType(BattlegroundTypeId(i)))
                continue;

            //BattlegroundContainer arenas = sBattlegroundMgr->GetBattlegroundsByType((BattlegroundTypeId)i);
            BattlegroundData* arenas = sBattlegroundMgr->GetAllBattlegroundsWithTypeId(BattlegroundTypeId(i));
            ChatHandler(player->GetSession()).PSendSysMessage("Test 0.2"); // METHOD TEST
            if (arenas->m_Battlegrounds.empty())
                continue;
            ChatHandler(player->GetSession()).PSendSysMessage("Test .75"); // METHOD TEST
            for (BattlegroundContainer::const_iterator itr = arenas->m_Battlegrounds.begin(); itr != arenas->m_Battlegrounds.end(); ++itr)
            {
                ChatHandler(player->GetSession()).PSendSysMessage("Test .95"); // METHOD TEST
                Battleground* arena = itr->second;
                Player* target = ObjectAccessor::FindPlayer(GetFirstPlayerGuid(arena));
                ChatHandler(player->GetSession()).PSendSysMessage("Test 1"); // METHOD TEST
                if (target && (target->HasAura(32728) || target->HasAura(32727)))
                    continue;
                ChatHandler(player->GetSession()).PSendSysMessage("Test 1.25"); // METHOD TEST
                if (!arena->GetPlayersSize())
                    continue;
                ChatHandler(player->GetSession()).PSendSysMessage("Test 1.5"); // METHOD TEST
                //uint16 mmrTwo = arena->GetArenaMatchmakerRating(0);
                //uint16 mmrThree = arena->GetArenaMatchmakerRating(1);

                // This shows the mmr of both teams on ARENA_TYPE_2v2
                if (arena->GetArenaType() == ARENA_TYPE_2v2)
                {
                    mmr = target->GetArenaMatchMakerRating(0);
                    firstTeamId = target->GetArenaTeamId(0);
                    Battleground::BattlegroundPlayerMap::const_iterator citr = arena->GetPlayers().begin();
                    for (; citr != arena->GetPlayers().end(); ++citr)
                    if (Player* plrs = ObjectAccessor::FindPlayer(citr->first))
                    if (plrs->GetArenaTeamId(0) != firstTeamId)
                        mmrTwo = target->GetArenaMatchMakerRating(citr->second.Team);
                    ChatHandler(player->GetSession()).PSendSysMessage("Test 2"); // METHOD TEST
                }
                // This shows the mmr of both teams on ARENA_TYPE_3v3
                else if (arena->GetArenaType() == ARENA_TYPE_3v3)
                {
                    mmr = target->GetArenaMatchMakerRating(1);
                    firstTeamId = target->GetArenaTeamId(1);
                    Battleground::BattlegroundPlayerMap::const_iterator citr = arena->GetPlayers().begin();
                    for (; citr != arena->GetPlayers().end(); ++citr)
                    if (Player* plrs = ObjectAccessor::FindPlayer(citr->first))
                    if (plrs->GetArenaTeamId(1) != firstTeamId)
                        mmrTwo = target->GetArenaMatchMakerRating(citr->second.Team);
                    ChatHandler(player->GetSession()).PSendSysMessage("Test 3"); // METHOD TEST
                }

                if (IsTop && arena->GetArenaType() == ARENA_TYPE_3v3)
                {
                    ChatHandler(player->GetSession()).PSendSysMessage("Test 8"); // METHOD TEST
                    TypeThree++;
                    if (TypeThree > (page + 1) * GamesOnPage)
                    {
                        haveNextPage = true;
                        break;
                    }

                    if (TypeThree >= page * GamesOnPage)
                        player->ADD_GOSSIP_ITEM(GOSSIP_ICON_BATTLE, GetGamesStringData(arena, mmr, mmrTwo), GOSSIP_SENDER_MAIN, NPC_SPECTATOR_ACTION_SELECTED_PLAYER + GetFirstPlayerGuid(arena));

                }
                else if (!IsTop && arena->GetArenaType() == ARENA_TYPE_2v2)
                {
                    ChatHandler(player->GetSession()).PSendSysMessage("Test 9"); // METHOD TEST
                    TypeTwo++;
                    if (TypeTwo > (page + 1) * GamesOnPage)
                    {
                        haveNextPage = true;
                        break;
                    }

                    if (TypeTwo >= page * GamesOnPage)
                        player->ADD_GOSSIP_ITEM(GOSSIP_ICON_BATTLE, GetGamesStringData(arena, mmr, mmrTwo), GOSSIP_SENDER_MAIN, NPC_SPECTATOR_ACTION_SELECTED_PLAYER + GetFirstPlayerGuid(arena));
                }
            }
        }

        if (page > 0)
            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "Prev..", GOSSIP_SENDER_MAIN, NPC_SPECTATOR_ACTION_2V2_GAMES + page - 1);

        if (haveNextPage)
            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_DOT, "Next..", GOSSIP_SENDER_MAIN, NPC_SPECTATOR_ACTION_2V2_GAMES + page + 1);
    }

    std::string GetSpecClassNameById(uint8 Id, uint32 Spec)
    {
        std::string sClass = "";
        switch (Id)
        {
        case CLASS_WARRIOR:
            switch (Spec)
            {
            case SPEC_WARRIOR_ARMS: sClass = "AWarrior "; break;
            case SPEC_WARRIOR_FURY: sClass = "FWarrior "; break;
            case SPEC_WARRIOR_PROTECTION: sClass = "PWarrior "; break;
            }
            break;
        case CLASS_PALADIN:
            switch (Spec)
            {
            case SPEC_PALADIN_HOLY: sClass = "HPala "; break;
            case SPEC_PALADIN_PROTECTION: sClass = "PPala "; break;
            case SPEC_PALADIN_RETRIBUTION: sClass = "RPala "; break;
            }
            break;
        case CLASS_HUNTER:
            switch (Spec)
            {
            case SPEC_HUNTER_BEASTMASTER: sClass = "BHunt "; break;
            case SPEC_HUNTER_MARKSMAN: sClass = "MHunt "; break;
            case SPEC_HUNTER_SURVIVAL: sClass = "SHunt "; break;
            }
            break;
        case CLASS_ROGUE:
            switch (Spec)
            {
            case SPEC_ROGUE_ASSASSINATION: sClass = "ARogue "; break;
            case SPEC_ROGUE_COMBAT: sClass = "CRogue "; break;
            case SPEC_ROGUE_SUBTLETY: sClass = "SRogue "; break;
            }
            break;
        case CLASS_PRIEST:
            switch (Spec)
            {
            case SPEC_PRIEST_DISCIPLINE: sClass = "DPriest "; break;
            case SPEC_PRIEST_HOLY: sClass = "HPriest "; break;
            case SPEC_PRIEST_SHADOW: sClass = "SPriest "; break;
            }
            break;
        case CLASS_DEATH_KNIGHT:
            switch (Spec)
            {
            case SPEC_DK_BLOOD: sClass = "BDk "; break;
            case SPEC_DK_FROST: sClass = "FDk "; break;
            case SPEC_DK_UNHOLY: sClass = "UDk "; break;
            }
            break;
        case CLASS_SHAMAN:
            switch (Spec)
            {
            case SPEC_SHAMAN_ELEMENTAL: sClass = "ELShaman "; break;
            case SPEC_SHAMAN_ENHANCEMENT: sClass = "ENShaman "; break;
            case SPEC_SHAMAN_RESTORATION: sClass = "RShaman "; break;
            }
            break;
        case CLASS_MAGE:
            switch (Spec)
            {
            case SPEC_MAGE_ARCANE: sClass = "AMage "; break;
            case SPEC_MAGE_FIRE: sClass = "FIMage "; break;
            case SPEC_MAGE_FROST: sClass = "FRMage "; break;
            }
            break;
        case CLASS_WARLOCK:
            switch (Spec)
            {
            case SPEC_WARLOCK_AFFLICTION: sClass = "ALock "; break;
            case SPEC_WARLOCK_DEMONOLOGY: sClass = "DemoLock "; break;
            case SPEC_WARLOCK_DESTRUCTION: sClass = "DestLock "; break;
            }
            break;
        case CLASS_DRUID:
            switch (Spec)
            {
            case SPEC_DRUID_BALANCE: sClass = "BDruid "; break;
            case SPEC_DRUID_CAT: sClass = "FDruid "; break;
            case SPEC_DRUID_BEAR: sClass = "FDruid "; break;
            case SPEC_DRUID_RESTORATION: sClass = "RDruid "; break;
            }
            break;
        case CLASS_MONK:
            switch (Spec)
            {
            case SPEC_MONK_BREWMASTER: sClass = "BmMonk "; break;
            case SPEC_MONK_WINDWALKER: sClass = "WwMonk "; break;
            case SPEC_MONK_MISTWEAVER: sClass = "MwMonk "; break;
            }
            break;
        }
        return sClass;
    }
};

void AddSC_arena_spectator_script()
{
    new arena_spectator_commands();
    new npc_arena_spectator();
}