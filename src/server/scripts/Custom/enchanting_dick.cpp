enum Enchants
{
	//WEAPONS
	ENCHANT_WEP_BLOODY_DANCING_STEEL = 5125, //fixed id
	ENCHANT_WEP_SPIRIT_OF_CONQUEST = 5124, //fixed id
	ENCHANT_WEP_WINDSONG = 4441, //fixed id
	ENCHANT_WEP_JADE_SPIRIT = 4442, //fixed id
	ENCHANT_WEP_ELEMENTAL_FORCE = 4443, //fixed id
	ENCHANT_WEP_DANCING_STEEL = 4444,
	ENCHANT_WEP_COLOSSUS = 4445,
	ENCHANT_WEP_RIVERS_SONG = 4446,

	RF_1 = 3369, //Rune of Cinderglaicer (non speficic)
	RF_2 = 3366, //Rune of Lichbane (non specific)
	RF_3 = 3370, //Rune of Razorice (non speficic)
	RF_4 = 3595, //Rune of Spellbreaking (one handed)
	RF_5 = 3367, //Rune of Spellshattering (two handed)
	RF_6 = 3594, //Rune of Swordbreaking (one handed)
	RF_7 = 3365, //Rune of Swordshattering (two handed)
	RF_8 = 3368, //Rune of the Fallen Crusader (non specific)
	RF_9 = 3883, //Rune of the Nerubian Carapace (one handed)
	RF_10 = 3847,//Rune of the Stoneskin Gargoyle (two handed)

	ENCHANT_OFF_MAJOR_INTELLECT = 4434,
	ENCHANT_OFF_GREATER_PARRY = 4993,

	ENCHANT_SECRET_CRANE_WING = 4915,
	ENCHANT_SECRET_OX_HORN = 4912,
	ENCHANT_SECRET_TIGER_CLAW = 4914,
	ENCHANT_SECRET_TIGER_FANG = 4913,
	
	ENCHANT_TIGER_FANG = 4803,
	ENCHANT_TIGER_CLAW = 4804, 
	ENCHANT_CRANE_WING = 4806,
	ENCHANT_OX_HORN = 4805,

	ENCHANT_CLOAK_1 = 4424,
	ENCHANT_CLOAK_2 = 4423,
	ENCHANT_CLOAK_3 = 4422,
	ENCHANT_CLOAK_4 = 4421,

	ENCHANT_CHEST_1 = 4420,
	ENCHANT_CHEST_2 = 4419,
	ENCHANT_CHEST_3 = 4418,
	ENCHANT_CHEST_4 = 4417,

	ENCHANT_WRIST_1 = 4416,
	ENCHANT_WRIST_2 = 4415,
	ENCHANT_WRIST_3 = 4414,
	ENCHANT_WRIST_4 = 4412,
	ENCHANT_WRIST_5 = 4411,

	ENCHANT_GLOVES_1 = 4433,
	ENCHANT_GLOVES_2 = 4432,
	ENCHANT_GLOVES_3 = 4431,
	ENCHANT_GLOVES_4 = 4430,

	ENCHANT_BOOTS_1 = 4429,
	ENCHANT_BOOTS_2 = 4428,
	ENCHANT_BOOTS_3 = 4427,
	ENCHANT_BOOTS_4 = 4426,
	
	BELT_SOCKET = 5002,
	BRACER_SOCKET_BS = 3717,
	GLOVE_SOCKET_BS = 3723,
};

#include "ScriptPCH.h"

void EnchantWithItem(Player* player, Item* item_id, Item* target)
{
	if (!target)
	{
		player->GetSession()->SendNotification("|cff00FF7FYou must first equip the item you are trying to enchant in order to enchant it!");
		return;
	}

	if (!item_id)
	{
		player->GetSession()->SendNotification("|cff00FF7FSomething went wrong in the code. It hasn't been logged for developers and will be not looked into, because somebody removed the logging code.");
		//sLog->("enchant_vendor::Enchant: Enchant NPC 'enchantid' is NULL, something went wrong here!");
		return;
	}

	SpellCastTargets* targets = new SpellCastTargets();
	targets->SetItemTarget(target);
	player->CastItemUseSpell(item_id, *targets, 1, 0);

	player->GetSession()->SendNotification("|cff800080%s |cffFF0000succesfully enchanted!", item_id->GetTemplate()->Name1.c_str());
}

void Enchant(Player* player, Item* item, uint32 enchantid)
{
	if (!item)
	{
		player->GetSession()->SendNotification("|cff00FF7FYou must first equip the item you are trying to enchant in order to enchant it!");
		return;
	}

	if (!enchantid)
	{
		player->GetSession()->SendNotification("|cff00FF7FSomething went wrong in the code. It hasn't been logged for developers and will be not looked into, because somebody removed the logging code.");
		//sLog->("enchant_vendor::Enchant: Enchant NPC 'enchantid' is NULL, something went wrong here!");
		return;
	}

	player->ApplyEnchantment(item, PERM_ENCHANTMENT_SLOT, false);
	item->SetEnchantment(PERM_ENCHANTMENT_SLOT, enchantid, 0, 0);
	player->ApplyEnchantment(item, PERM_ENCHANTMENT_SLOT, true);
	player->GetSession()->SendNotification("|cff800080%s |cffFF0000succesfully enchanted!", item->GetTemplate()->Name1.c_str());
}

void RemoveEnchant(Player* player, Item* item)
{
	if (!item)
	{
		player->GetSession()->SendNotification("|cff00FF7FYou don't have the item equipped?");
		return;
	}

	item->ClearEnchantment(PERM_ENCHANTMENT_SLOT);
	player->GetSession()->SendNotification("|cff800080%s's |cffFF0000enchant has successfully been removed!", item->GetTemplate()->Name1.c_str());
}
void WeaponCheck_1H(Player* player, Item* item)
{
	if (!item)
	{
		player->GetSession()->SendNotification("|cff00FF7FYou must first equip the item you are trying to enchant in order to enchant it!");
		return;
	}
	if (item->GetTemplate()->InventoryType != INVTYPE_WEAPONMAINHAND) //one-handed weaponcheck
	{
		player->GetSession()->SendNotification("|cff00FF7FNice try, trying to put a one handed enchant on a two hander pal!");
		return;
	}

}

class npc_enchantment : public CreatureScript
{
public:
	npc_enchantment() : CreatureScript("npc_enchantment") { }

	bool OnGossipHello(Player* player, Creature* creature)
	{
		player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_axe_2h_cataclysm_c_01.png:28:28:-16|t[-Enchant Weapon-]", GOSSIP_SENDER_MAIN, 15);
		player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_helmet_142.png:28:28:-16|t[-Enchant Armor-]", GOSSIP_SENDER_MAIN, 16);

		player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/ability_iyyokuk_sword_red.png:28:28:-16|t[-I wish to remove my enchant-]", GOSSIP_SENDER_MAIN, 14);
		player->PlayerTalkClass->SendGossipMenu(100001, creature->GetGUID());
		return true;
	}

	bool OnGossipSelect(Player* player, Creature* creature, uint32 /*sender*/, uint32 action)
	{
		player->PlayerTalkClass->ClearMenus();
		Item * item;
		Item * item2;

		switch (action)
		{

		case 15:
		{
				   player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_axe_2h_cataclysm_c_01.png:28:28:-16|t[-Enchant Weapon-]", GOSSIP_SENDER_MAIN, 1);
				   player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_knife_1h_bwdraid_d_01.png:28:28:-16|t[-Enchant Off-Hand Weapon-]", GOSSIP_SENDER_MAIN, 3); //fixed menu name
				   player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_shield_55.png:28:28:-16|t[-Enchant Off-Hand]", GOSSIP_SENDER_MAIN, 2);
				   player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_weapon_crossbow_27.png:28:28:-16|t[-Enchant Ranged Weapon]", GOSSIP_SENDER_MAIN, 25);

				   player->ADD_GOSSIP_ITEM(1, "<-Back", GOSSIP_SENDER_MAIN, 108);
				   player->PlayerTalkClass->SendGossipMenu(100002, creature->GetGUID());
		}
			break;

		case 16:
		{
				   //player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_axe_2h_cataclysm_c_01.png:28:28:-16|t[-Enchant Helmet-]", GOSSIP_SENDER_MAIN, 17);
				   player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_shoulder_129.png:28:28:-16|t[-Enchant Shoulders-]", GOSSIP_SENDER_MAIN, 18);
				   player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_cape_pandaria_dragoncaster_d_02.png:28:28:-16|t[-Enchant Cloak-]", GOSSIP_SENDER_MAIN, 19);
				   player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_chest_cloth_raidwarlock_l_01.png:28:28:-16|t[-Enchant Chest-]", GOSSIP_SENDER_MAIN, 20);
				   player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_pant_leather_pvpmonk_g_01.png:28:28:-16|t[-Enchant Legs]", GOSSIP_SENDER_MAIN, 21);
				   player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_belt_62.png:28:28:-16|t[-Enchant Belt]", GOSSIP_SENDER_MAIN, 26);  //NOTE, maybe add engineering enchants later? totally forgot they existed
				   player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_bracer_43.png:28:28:-16|t[-Enchant Wrist-]", GOSSIP_SENDER_MAIN, 22);
				   player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_gloves_cloth_panda_b_01_green.png:28:28:-16|t[-Enchant Gloves-]", GOSSIP_SENDER_MAIN, 23);
				   player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_boots_cloth_29.png:28:28:-16|t[-Enchant Boots-]", GOSSIP_SENDER_MAIN, 24);

				   player->ADD_GOSSIP_ITEM(1, "<-Back", GOSSIP_SENDER_MAIN, 108);
				   player->PlayerTalkClass->SendGossipMenu(100002, creature->GetGUID());
		}
			break;


		case 108: //<-Back menu
		{
					  player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_axe_2h_cataclysm_c_01.png:28:28:-16|t[-Enchant Weapon-]", GOSSIP_SENDER_MAIN, 15);
					  player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_helmet_142.png:28:28:-16|t[-Enchant Armor-]", GOSSIP_SENDER_MAIN, 16);

					  player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/ability_iyyokuk_sword_red.png:28:28:-16|t[-I wish to remove my enchant-]", GOSSIP_SENDER_MAIN, 14);
					  player->PlayerTalkClass->SendGossipMenu(100001, creature->GetGUID());
					  return true;
		}
			break;

		case 1: // Enchant Weapon                   
			
			item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND);

			if (!item)
			{
				creature->MonsterWhisper("|cff00FF7FYou must equip a weapon first!", player->GetGUID(), true);
				player->PlayerTalkClass->SendCloseGossip();
				return false;
			}
			if (item->GetTemplate()->InventoryType == INVTYPE_2HWEAPON || item->GetTemplate()->InventoryType == INVTYPE_WEAPON)
			{
				player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_inscription_runescrolloffortitude_red.png:28:28:-16|tLiving Steel Weapon Chain ", GOSSIP_SENDER_MAIN, 9510);
				//player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_inscription_runescrolloffortitude_red.png:28:28:-16|tSpirit of Conquest", GOSSIP_SENDER_MAIN, 101);
				player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_inscription_runescrolloffortitude_red.png:28:28:-16|tWindsong", GOSSIP_SENDER_MAIN, 102);
				player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_inscription_runescrolloffortitude_red.png:28:28:-16|tJade Spirit", GOSSIP_SENDER_MAIN, 103);
				player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_inscription_runescrolloffortitude_red.png:28:28:-16|tElemental Force", GOSSIP_SENDER_MAIN, 104);
				player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_inscription_runescrolloffortitude_red.png:28:28:-16|tDancing Steel", GOSSIP_SENDER_MAIN, 105);
				player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_inscription_runescrolloffortitude_red.png:28:28:-16|tColossus", GOSSIP_SENDER_MAIN, 106);
				player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_inscription_runescrolloffortitude_red.png:28:28:-16|tRiver's Song", GOSSIP_SENDER_MAIN, 107);

				if (player->getClass() == CLASS_DEATH_KNIGHT)
				{
					item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND);
					if (item->GetTemplate()->InventoryType == INVTYPE_WEAPON)
					{
						player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_sword_122.png:28:28:-16|t1H Runeforging", GOSSIP_SENDER_MAIN, 7000); //needs a one-hand/two-hand weaponcheck.
					}
					else
					{
						player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_axe_113.png:28:28:-16|t2H Runeforging", GOSSIP_SENDER_MAIN, 7001); //needs a one-hand/two-hand weaponcheck.
					}
				}
			}
			else
			{
				creature->MonsterWhisper("|cff00FF7FYou don't have a weapon equipped!", player->GetGUID(), true);
				player->PlayerTalkClass->SendCloseGossip();
			}

			player->ADD_GOSSIP_ITEM(1, "<-Back", GOSSIP_SENDER_MAIN, 108);
			player->PlayerTalkClass->SendGossipMenu(100002, creature->GetGUID());
			return true;
			break;

		case 2: // Enchant Off-Hand
		{
					item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_OFFHAND);

					if (!item)
					{
						creature->MonsterWhisper("|cff00FF7FYou must equip an off-hand weapon first!", player->GetGUID(), true);
						player->PlayerTalkClass->SendCloseGossip();
						return false;
					}

					if (item->GetTemplate()->InventoryType == INVTYPE_SHIELD || item->GetTemplate()->InventoryType == INVTYPE_HOLDABLE) //there are no shield proprietary enchants, so this is a test for some off-hands
					{
						player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_inscription_runescrolloffortitude_red.png:28:28:-16|tMajor Intellect", GOSSIP_SENDER_MAIN, 109);
						
						if (item->GetTemplate()->InventoryType == INVTYPE_SHIELD)
						{
							player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_inscription_runescrolloffortitude_red.png:28:28:-16|tGreater Parry", GOSSIP_SENDER_MAIN, 110);
						}
						player->ADD_GOSSIP_ITEM(1, "<-Back", GOSSIP_SENDER_MAIN, 108);
						player->PlayerTalkClass->SendGossipMenu(100004, creature->GetGUID());
						return true;
					}
					else
					{
						creature->MonsterWhisper("|cff00FF7FYou don't have an off-hand equipped!", player->GetGUID(), true);
						player->PlayerTalkClass->SendCloseGossip();
					}
		}
			break;

		case 3: //Enchant Off-Hand weapons
		{
					item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_OFFHAND);

					if (!item)
					{
						creature->MonsterWhisper("|cff00FF7FThis enchant needs a one-hand weapon or two-handed equipped in the off-hand!", player->GetGUID(), true);
						player->PlayerTalkClass->SendCloseGossip();
						return false;
					}

					if (item->GetTemplate()->InventoryType == INVTYPE_WEAPON || item->GetTemplate()->InventoryType == INVTYPE_2HWEAPON) //Handles Fury Warriors better?
					{
						//off-hand requires a slot check before the enchant can be completed, to ensure no retard opens the menu then puts on a tome or shield.
					 
						player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_inscription_runescrolloffortitude_red.png:28:28:-16|tLiving Steel Weapon Chain ", GOSSIP_SENDER_MAIN, 9509);
						//player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_inscription_runescrolloffortitude_red.png:28:28:-16|tSpirit of Conquest", GOSSIP_SENDER_MAIN, 201);
						player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_inscription_runescrolloffortitude_red.png:28:28:-16|tWindsong", GOSSIP_SENDER_MAIN, 202);
						player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_inscription_runescrolloffortitude_red.png:28:28:-16|tJade Spirit", GOSSIP_SENDER_MAIN, 203);
						player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_inscription_runescrolloffortitude_red.png:28:28:-16|tElemental Force", GOSSIP_SENDER_MAIN, 204);
						player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_inscription_runescrolloffortitude_red.png:28:28:-16|tDancing Steel", GOSSIP_SENDER_MAIN, 205);
						player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_inscription_runescrolloffortitude_red.png:28:28:-16|tColossus", GOSSIP_SENDER_MAIN, 206);
						player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_inscription_runescrolloffortitude_red.png:28:28:-16|tRiver's Song", GOSSIP_SENDER_MAIN, 207);
						if (player->getClass() == CLASS_DEATH_KNIGHT)
						{
							player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_sword_122.png:28:28:-16|tOff-Hand Runeforging", GOSSIP_SENDER_MAIN, 7003);
						}
						player->ADD_GOSSIP_ITEM(1, "<-Back", GOSSIP_SENDER_MAIN, 108);
						player->PlayerTalkClass->SendGossipMenu(100002, creature->GetGUID());
						return true;
					}
					else
					{
						creature->MonsterWhisper("|cff00FF7FYour Off-Hand is not a weapon!", player->GetGUID(), true);
						player->PlayerTalkClass->SendCloseGossip();
					}
		}
			break;

		case 14: //Remove enchant menu
			player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_axe_2h_cataclysm_c_01.png:28:28:-16|tRemove enchant on Main-hand", GOSSIP_SENDER_MAIN, 50);
			player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_knife_1h_bwdraid_d_01.png:28:28:-16|tRemove enchant on Off-hand", GOSSIP_SENDER_MAIN, 51);
			player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_axe_2h_cataclysm_c_01.png:28:28:-16|tRemove enchant on Shoulder", GOSSIP_SENDER_MAIN, 52);
			player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_cape_pandaria_dragoncaster_d_02.png:28:28:-16|tRemove enchant on Cloak", GOSSIP_SENDER_MAIN, 53);
			player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_chest_cloth_raidwarlock_l_01.png:28:28:-16|tRemove enchant on Chest", GOSSIP_SENDER_MAIN, 54);
			player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_pant_leather_pvpmonk_g_01.png:28:28:-16|tRemove enchant on Legs", GOSSIP_SENDER_MAIN, 58);
			player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_belt_62.png:28:28:-16|tRemove enchant on Belt", GOSSIP_SENDER_MAIN, 59);
			player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_bracer_43.png:28:28:-16|tRemove enchant on Wrists", GOSSIP_SENDER_MAIN, 55);
			player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_gloves_cloth_panda_b_01_green.png:28:28:-16|tRemove enchant on Gloves", GOSSIP_SENDER_MAIN, 56);
			player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_boots_cloth_29.png:28:28:-16|tRemove enchant on Boots", GOSSIP_SENDER_MAIN, 57);
			player->ADD_GOSSIP_ITEM(1, "<-Back", GOSSIP_SENDER_MAIN, 108);
			player->PlayerTalkClass->SendGossipMenu(100014, creature->GetGUID());
			return true;
			break;

		case 18: // Enchant Shoulder                   
			
			item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_SHOULDERS);

			if (!item)
			{
				creature->MonsterWhisper("|cff00FF7FYou must equip shoulders first!", player->GetGUID(), true);
				player->PlayerTalkClass->SendCloseGossip();
				return false;
			}
			if (player->HasSkill(SKILL_INSCRIPTION))
			{
				player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_inscription_runescrolloffortitude_red.png:28:28:-16|tSecret Crane Wing Inscription", GOSSIP_SENDER_MAIN, 208);
				player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_inscription_runescrolloffortitude_red.png:28:28:-16|tSecret Ox Horn Inscription", GOSSIP_SENDER_MAIN, 209);
				player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_inscription_runescrolloffortitude_red.png:28:28:-16|tSecret Tiger Claw Inscription", GOSSIP_SENDER_MAIN, 210);
				player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_inscription_runescrolloffortitude_red.png:28:28:-16|tSecret Tiger Fang Inscription", GOSSIP_SENDER_MAIN, 211);						
			}
			player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_inscription_runescrolloffortitude_red.png:28:28:-16|tTiger Fang Inscription", GOSSIP_SENDER_MAIN, 3051);
			player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_inscription_runescrolloffortitude_red.png:28:28:-16|tTiger Claw Wing Inscription", GOSSIP_SENDER_MAIN, 3052);
			player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_inscription_runescrolloffortitude_red.png:28:28:-16|tCrane Wing Inscription", GOSSIP_SENDER_MAIN, 3053);
			player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_inscription_runescrolloffortitude_red.png:28:28:-16|tOx Horn Inscription", GOSSIP_SENDER_MAIN, 3054);
			player->ADD_GOSSIP_ITEM(1, "<-Back", GOSSIP_SENDER_MAIN, 108);
			player->PlayerTalkClass->SendGossipMenu(100002, creature->GetGUID());
			return true;
			break;

		case 19: // Enchant Cloak                   
			
			item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_BACK);

			if (!item)
			{
				creature->MonsterWhisper("|cff00FF7FYou must equip a cloak first!", player->GetGUID(), true);
				player->PlayerTalkClass->SendCloseGossip();
				return false;
			}
			player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_inscription_runescrolloffortitude_red.png:28:28:-16|tSuperior Critical Strike", GOSSIP_SENDER_MAIN, 212);
			player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_inscription_runescrolloffortitude_red.png:28:28:-16|tSuperior Intellect", GOSSIP_SENDER_MAIN, 213);
			player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_inscription_runescrolloffortitude_red.png:28:28:-16|tGreater Protection", GOSSIP_SENDER_MAIN, 214);
			player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_inscription_runescrolloffortitude_red.png:28:28:-16|tAccuracy", GOSSIP_SENDER_MAIN, 215);
			player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_inscription_runescrolloffortitude_red.png:28:28:-16|tDarkglow Embroidery", GOSSIP_SENDER_MAIN, 233);
			player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_inscription_runescrolloffortitude_red.png:28:28:-16|tLightweave Embroidery", GOSSIP_SENDER_MAIN, 234);
			player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_inscription_runescrolloffortitude_red.png:28:28:-16|tSwordguard Embroidery", GOSSIP_SENDER_MAIN, 236);

			player->ADD_GOSSIP_ITEM(1, "<-Back", GOSSIP_SENDER_MAIN, 108);
			player->PlayerTalkClass->SendGossipMenu(100002, creature->GetGUID());
			return true;
			break;

		case 20: // Enchant Chest                   
			
			item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_CHEST);

			if (!item)
			{
				creature->MonsterWhisper("|cff00FF7FYou must equip a chest first!", player->GetGUID(), true);
				player->PlayerTalkClass->SendCloseGossip();
				return false;
			}
			player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_inscription_runescrolloffortitude_red.png:28:28:-16|tSuperior Stamina", GOSSIP_SENDER_MAIN, 216);
			player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_inscription_runescrolloffortitude_red.png:28:28:-16|tGlorious Stats", GOSSIP_SENDER_MAIN, 217);
			player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_inscription_runescrolloffortitude_red.png:28:28:-16|tMighty Versatility", GOSSIP_SENDER_MAIN, 218);
			player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_inscription_runescrolloffortitude_red.png:28:28:-16|tSuper Resilience", GOSSIP_SENDER_MAIN, 219);

			player->ADD_GOSSIP_ITEM(1, "<-Back", GOSSIP_SENDER_MAIN, 108);
			player->PlayerTalkClass->SendGossipMenu(100002, creature->GetGUID());
			return true;
			break;

		case 21: //Enchant Legs
			item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_LEGS);

			if (!item)
			{
				creature->MonsterWhisper("|cff00FF7FYou must equip legs first!", player->GetGUID(), true);
				player->PlayerTalkClass->SendCloseGossip();
				return false;
			}
			player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_inscription_runescrolloffortitude_red.png:28:28:-16|tAngerhide Leg Armor", GOSSIP_SENDER_MAIN, 9500);
			player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_inscription_runescrolloffortitude_red.png:28:28:-16|tIronscale Leg Armor", GOSSIP_SENDER_MAIN, 9501);
			player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_inscription_runescrolloffortitude_red.png:28:28:-16|tShadowleather Leg Armor", GOSSIP_SENDER_MAIN, 9502);
			player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_inscription_runescrolloffortitude_red.png:28:28:-16|tGreater Cerulean Spellthread", GOSSIP_SENDER_MAIN, 9503);
			player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_inscription_runescrolloffortitude_red.png:28:28:-16|tGreater Pearlescent Spellthread", GOSSIP_SENDER_MAIN, 9504);

			player->ADD_GOSSIP_ITEM(1, "<-Back", GOSSIP_SENDER_MAIN, 108);
			player->PlayerTalkClass->SendGossipMenu(100002, creature->GetGUID());
			return true;
			break;

		case 22: // Enchant Wrist                   
			
			item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_WRISTS);

			if (!item)
			{
				creature->MonsterWhisper("|cff00FF7FYou must equip wrists first!", player->GetGUID(), true);
				player->PlayerTalkClass->SendCloseGossip();
				return false;
			}
			/*
			if (player->HasSkill(SKILL_BLACKSMITHING) )
			{
				//player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_inscription_runescrolloffortitude_red.png:28:28:-16|tSocket Bracer", GOSSIP_SENDER_MAIN, 3061);
			}
			*/
			player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_inscription_runescrolloffortitude_red.png:28:28:-16|tGreater Agility", GOSSIP_SENDER_MAIN, 220);
			player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_inscription_runescrolloffortitude_red.png:28:28:-16|tExceptional Strength", GOSSIP_SENDER_MAIN, 221);
			player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_inscription_runescrolloffortitude_red.png:28:28:-16|tSuper Intellect", GOSSIP_SENDER_MAIN, 222);
			player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_inscription_runescrolloffortitude_red.png:28:28:-16|tMajor Dodge", GOSSIP_SENDER_MAIN, 223);
			player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_inscription_runescrolloffortitude_red.png:28:28:-16|tMastery", GOSSIP_SENDER_MAIN, 224);

			player->ADD_GOSSIP_ITEM(1, "<-Back", GOSSIP_SENDER_MAIN, 108);
			player->PlayerTalkClass->SendGossipMenu(100002, creature->GetGUID());
			return true;
			break;

		case 23: // Enchant Gloves                   
			
			item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_HANDS);

			if (!item)
			{
				creature->MonsterWhisper("|cff00FF7FYou must equip gloves first!", player->GetGUID(), true);
				player->PlayerTalkClass->SendCloseGossip();
				return false;
			}
			/*
			if (player->HasSkill(SKILL_BLACKSMITHING) )
			{
				//player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_inscription_runescrolloffortitude_red.png:28:28:-16|tSocket Gloves", GOSSIP_SENDER_MAIN, 3062);
			}
			*/
			player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_inscription_runescrolloffortitude_red.png:28:28:-16|tSuperior Mastery", GOSSIP_SENDER_MAIN, 225);
			player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_inscription_runescrolloffortitude_red.png:28:28:-16|tSuper Strength", GOSSIP_SENDER_MAIN, 226);
			player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_inscription_runescrolloffortitude_red.png:28:28:-16|tSuperior Expertise", GOSSIP_SENDER_MAIN, 227);
			player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_inscription_runescrolloffortitude_red.png:28:28:-16|tGreater Haste", GOSSIP_SENDER_MAIN, 228);
			//player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_inscription_runescrolloffortitude_red.png:28:28:-16|tAdd Socket", GOSSIP_SENDER_MAIN, 237);

			player->ADD_GOSSIP_ITEM(1, "<-Back", GOSSIP_SENDER_MAIN, 108);
			player->PlayerTalkClass->SendGossipMenu(100002, creature->GetGUID());
			return true;
			break;

		case 24: // Enchant Boots                   
			
			item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_FEET);

			if (!item)
			{
				creature->MonsterWhisper("|cff00FF7FYou must equip boots first!", player->GetGUID(), true);
				player->PlayerTalkClass->SendCloseGossip();
				return false;
			}
			player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_inscription_runescrolloffortitude_red.png:28:28:-16|tPandaren's Step", GOSSIP_SENDER_MAIN, 229);
			player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_inscription_runescrolloffortitude_red.png:28:28:-16|tBlurred Speed", GOSSIP_SENDER_MAIN, 230);
			player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_inscription_runescrolloffortitude_red.png:28:28:-16|tGreater Precision", GOSSIP_SENDER_MAIN, 231);
			player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_inscription_runescrolloffortitude_red.png:28:28:-16|tGreater Haste", GOSSIP_SENDER_MAIN, 232);

			player->ADD_GOSSIP_ITEM(1, "<-Back", GOSSIP_SENDER_MAIN, 108);
			player->PlayerTalkClass->SendGossipMenu(100002, creature->GetGUID());
			return true;
			break;

		case 26: // Enchant belt                   
			
			item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_WAIST);

			if (!item)
			{
				creature->MonsterWhisper("|cff00FF7FYou must equip a belt first!", player->GetGUID(), true);
				player->PlayerTalkClass->SendCloseGossip();
				return false;
			}
			player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_inscription_runescrolloffortitude_red.png:28:28:-16|tLiving Steel Belt Buckle", GOSSIP_SENDER_MAIN, 235);

			player->ADD_GOSSIP_ITEM(1, "<-Back", GOSSIP_SENDER_MAIN, 108);
			player->PlayerTalkClass->SendGossipMenu(100002, creature->GetGUID());
			return true;
			break;

		case 25: // Enchant Ranged                  
			
			item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND);

			if (!item)
			{
				creature->MonsterWhisper("|cff00FF7FYou must equip a ranged weapon first!", player->GetGUID(), true);
				player->PlayerTalkClass->SendCloseGossip();
				return false;
			}

			if (item->GetTemplate()->InventoryType == INVTYPE_RANGED) {
				player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_inscription_runescrolloffortitude_red.png:28:28:-16|tLord Blastington's Scope of Doom", GOSSIP_SENDER_MAIN, 300);
				player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_inscription_runescrolloffortitude_red.png:28:28:-16|tMirror Scope", GOSSIP_SENDER_MAIN, 301);

				player->ADD_GOSSIP_ITEM(1, "<-Back", GOSSIP_SENDER_MAIN, 108);
				player->PlayerTalkClass->SendGossipMenu(100002, creature->GetGUID());
			}
			else
			{
				player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs a ranged weapon.");
				player->PlayerTalkClass->SendCloseGossip();
			}
			return true;
			break;

			//------------------------
			//ENCHANTING PROCESS CASES
			//------------------------


			//RUNEFORGING CASES

			//Weapon Type cases

		case 7000:
			//one handed
			player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/spell_deathknight_frozenruneweapon.png:28:28:-16|tRune of Cinderglacier", GOSSIP_SENDER_MAIN, 9001);
			player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/spell_deathknight_frozenruneweapon.png:28:28:-16|tRune of Lichbane", GOSSIP_SENDER_MAIN, 9002);
			player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/spell_deathknight_frozenruneweapon.png:28:28:-16|tRune of Razorice", GOSSIP_SENDER_MAIN, 9003);
			player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/spell_deathknight_frozenruneweapon.png:28:28:-16|tRune of Spellbreaking", GOSSIP_SENDER_MAIN, 9004);
			//player->ADD_GOSSIP_ITEM(1, "Rune of Spellshattering", GOSSIP_SENDER_MAIN, 9005);
			player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/spell_deathknight_frozenruneweapon.png:28:28:-16|tRune of Swordbreaking", GOSSIP_SENDER_MAIN, 9006);
			//player->ADD_GOSSIP_ITEM(1, "Rune of Swordshattering", GOSSIP_SENDER_MAIN, 9007);
			player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/spell_deathknight_frozenruneweapon.png:28:28:-16|tRune of the Fallen Crusader", GOSSIP_SENDER_MAIN, 9008);
			player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/spell_deathknight_frozenruneweapon.png:28:28:-16|tRune of the Nerubian Carapace", GOSSIP_SENDER_MAIN, 9009);
			//player->ADD_GOSSIP_ITEM(1, "Rune of the Stoneskin Gargoyle", GOSSIP_SENDER_MAIN, 9010);
			player->ADD_GOSSIP_ITEM(1, "<-Back", GOSSIP_SENDER_MAIN, 108);
			player->PlayerTalkClass->SendGossipMenu(100002, creature->GetGUID());
			break;
		case 7001:
			//two handed
			player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/spell_deathknight_frozenruneweapon.png:28:28:-16|tRune of Cinderglacier", GOSSIP_SENDER_MAIN, 9001);  //RF_1
			player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/spell_deathknight_frozenruneweapon.png:28:28:-16|tRune of Lichbane", GOSSIP_SENDER_MAIN, 9002);		//RF_2
			player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/spell_deathknight_frozenruneweapon.png:28:28:-16|tRune of Razorice", GOSSIP_SENDER_MAIN, 9003);		//RF_3
			//player->ADD_GOSSIP_ITEM(1, "Rune of Spellbreaking", GOSSIP_SENDER_MAIN, 9004);//RF_4
			player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/spell_deathknight_frozenruneweapon.png:28:28:-16|tRune of Spellshattering", GOSSIP_SENDER_MAIN, 9005);//RF_5
			//player->ADD_GOSSIP_ITEM(1, "Rune of Swordbreaking", GOSSIP_SENDER_MAIN, 9006);//RF_6
			player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/spell_deathknight_frozenruneweapon.png:28:28:-16|tRune of Swordshattering", GOSSIP_SENDER_MAIN, 9007);//RF_7
			player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/spell_deathknight_frozenruneweapon.png:28:28:-16|tRune of the Fallen Crusader", GOSSIP_SENDER_MAIN, 9008);//RF_8
			//player->ADD_GOSSIP_ITEM(1, "Rune of the Nerubian Carapace", GOSSIP_SENDER_MAIN, 9009);//RF_9
			player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/spell_deathknight_frozenruneweapon.png:28:28:-16|tRune of the Stoneskin Gargoyle", GOSSIP_SENDER_MAIN, 9010);//RF_10
			player->ADD_GOSSIP_ITEM(1, "<-Back", GOSSIP_SENDER_MAIN, 108);
			player->PlayerTalkClass->SendGossipMenu(100002, creature->GetGUID());
			break;
		case 7003:
			//one handed off hand
			player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/spell_deathknight_frozenruneweapon.png:28:28:-16|tRune of Cinderglacier", GOSSIP_SENDER_MAIN, 8001);
			player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/spell_deathknight_frozenruneweapon.png:28:28:-16|tRune of Lichbane", GOSSIP_SENDER_MAIN, 8002);
			player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/spell_deathknight_frozenruneweapon.png:28:28:-16|tRune of Razorice", GOSSIP_SENDER_MAIN, 8003);
			player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/spell_deathknight_frozenruneweapon.png:28:28:-16|tRune of Spellbreaking", GOSSIP_SENDER_MAIN, 8004);
			//player->ADD_GOSSIP_ITEM(1, "Rune of Spellshattering", GOSSIP_SENDER_MAIN, 9005);
			player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/spell_deathknight_frozenruneweapon.png:28:28:-16|tRune of Swordbreaking", GOSSIP_SENDER_MAIN, 8006);
			//player->ADD_GOSSIP_ITEM(1, "Rune of Swordshattering", GOSSIP_SENDER_MAIN, 9007);
			player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/spell_deathknight_frozenruneweapon.png:28:28:-16|tRune of the Fallen Crusader", GOSSIP_SENDER_MAIN, 8008);
			player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/spell_deathknight_frozenruneweapon.png:28:28:-16|tRune of the Nerubian Carapace", GOSSIP_SENDER_MAIN, 8009);
			//player->ADD_GOSSIP_ITEM(1, "Rune of the Stoneskin Gargoyle", GOSSIP_SENDER_MAIN, 9010);
			player->ADD_GOSSIP_ITEM(1, "<-Back", GOSSIP_SENDER_MAIN, 108);
			player->PlayerTalkClass->SendGossipMenu(100002, creature->GetGUID());
			break;




		case 9001:
			//weaponcheck required due because DK mainhand can be 1h or 2h.
			item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND);

			if (!item)
			{
				player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs a Two-hand or One-handed weapon equipped in the main-hand.");
				player->PlayerTalkClass->SendCloseGossip();
				return false;
			}

			if (item->GetTemplate()->InventoryType == INVTYPE_2HWEAPON || item->GetTemplate()->InventoryType == INVTYPE_WEAPON)
			{
				Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND), RF_1);
				player->PlayerTalkClass->SendCloseGossip();
			}
			else
			{
				player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs a two-handed or one-handed weapon.");
				player->PlayerTalkClass->SendCloseGossip();
			}
			break;
		case 9002:
			//weaponcheck required due because DK mainhand can be 1h or 2h.
			item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND);

			if (!item)
			{
				player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs a Two-hand or One-handed weapon equipped in the main-hand.");
				player->PlayerTalkClass->SendCloseGossip();
				return false;
			}

			if (item->GetTemplate()->InventoryType == INVTYPE_2HWEAPON || item->GetTemplate()->InventoryType == INVTYPE_WEAPON)
			{
				Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND), RF_2);
				player->PlayerTalkClass->SendCloseGossip();
			}
			else
			{
				player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs a two-handed or one-handed weapon.");
				player->PlayerTalkClass->SendCloseGossip();
			}
			break;
		case 9003:
			//weaponcheck required due because DK mainhand can be 1h or 2h.
			item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND);

			if (!item)
			{
				player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs a Two-hand or One-handed weapon equipped in the main-hand.");
				player->PlayerTalkClass->SendCloseGossip();
				return false;
			}

			if (item->GetTemplate()->InventoryType == INVTYPE_2HWEAPON || item->GetTemplate()->InventoryType == INVTYPE_WEAPON)
			{
				Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND), RF_3);
				player->PlayerTalkClass->SendCloseGossip();
			}
			else
			{
				player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs a two-handed or one-handed weapon.");
				player->PlayerTalkClass->SendCloseGossip();
			}
			break;
		case 9004:
			//weaponcheck required due because DK mainhand can be 1h or 2h.
			item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND);

			if (!item)
			{
				player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs a one-hand weapon equipped in the main-hand.");
				player->PlayerTalkClass->SendCloseGossip();
				return false;
			}

			if (item->GetTemplate()->InventoryType == INVTYPE_WEAPON)
			{
				Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND), RF_4); //Spellbreaking
				player->PlayerTalkClass->SendCloseGossip();
			}
			else
			{
				player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs a two-handed weapon.");
				player->PlayerTalkClass->SendCloseGossip();
			}
			break;
		case 9005:
			//weaponcheck required due because DK mainhand can be 1h or 2h.
			item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND);

			if (!item)
			{
				player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs a Two-hand weapon equipped in the main-hand.");
				player->PlayerTalkClass->SendCloseGossip();
				return false;
			}

			if (item->GetTemplate()->InventoryType == INVTYPE_2HWEAPON)
			{
				Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND), RF_5); //Spellshattering
				player->PlayerTalkClass->SendCloseGossip();
			}
			else
			{
				player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs a two-handed weapon.");
				player->PlayerTalkClass->SendCloseGossip();
			}
			break;
		case 9006:
			//weaponcheck required due because DK mainhand can be 1h or 2h.
			item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND);

			if (!item)
			{
				player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs a one-hand weapon equipped in the main-hand.");
				player->PlayerTalkClass->SendCloseGossip();
				return false;
			}

			if (item->GetTemplate()->InventoryType == INVTYPE_WEAPON)
			{
				Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND), RF_6); //Swordbreaking
				player->PlayerTalkClass->SendCloseGossip();
			}
			else
			{
				player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs a two-handed weapon.");
				player->PlayerTalkClass->SendCloseGossip();
			}
			break;
		case 9007:
			//weaponcheck required due because DK mainhand can be 1h or 2h.
			item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND);

			if (!item)
			{
				player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs a Two-hand weapon equipped in the main-hand.");
				player->PlayerTalkClass->SendCloseGossip();
				return false;
			}

			if (item->GetTemplate()->InventoryType == INVTYPE_2HWEAPON)
			{
				Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND), RF_7); //Swordshattering
				player->PlayerTalkClass->SendCloseGossip();
			}
			else
			{
				player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs a two-handed weapon.");
				player->PlayerTalkClass->SendCloseGossip();
			}
			break;
		case 9008:
			//no weaponslot check required because this is a non-speficic enchant.
			Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND), RF_8); //Fallen Crusader
			player->PlayerTalkClass->SendCloseGossip();
			break;
		case 9009:
			//weaponcheck required due because DK mainhand can be 1h or 2h.
			item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND);

			if (!item)
			{
				player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs a one-hand weapon equipped in the main-hand.");
				player->PlayerTalkClass->SendCloseGossip();
				return false;
			}

			if (item->GetTemplate()->InventoryType == INVTYPE_WEAPON)
			{
				Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND), RF_9); //Nerubian Carapace
				player->PlayerTalkClass->SendCloseGossip();
			}
			else
			{
				player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs a two-handed weapon.");
				player->PlayerTalkClass->SendCloseGossip();
			}
			break;
		case 9010:
			//weaponcheck required due because DK mainhand can be 1h or 2h.
			item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND);

			if (!item)
			{
				player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs a Two-hand weapon equipped in the main-hand.");
				player->PlayerTalkClass->SendCloseGossip();
				return false;
			}

			if (item->GetTemplate()->InventoryType == INVTYPE_2HWEAPON)
			{
				Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND), RF_10); //Stoneskin Gargoyle
				player->PlayerTalkClass->SendCloseGossip();
			}
			else
			{
				player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs a two-handed weapon.");
				player->PlayerTalkClass->SendCloseGossip();
			}
			break;



			//OFF HAND ONE HANDED WEAPON RUNEFORGES CASES.


		case 8001:
			//weaponcheck required due because DK mainhand can be 1h or 2h.
			item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_OFFHAND);

			if (!item)
			{
				player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs a one-hand weapon equipped in the main-hand.");
				player->PlayerTalkClass->SendCloseGossip();
				return false;
			}

			if (item->GetTemplate()->InventoryType == INVTYPE_WEAPON)
			{
				Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_OFFHAND), RF_1);
				player->PlayerTalkClass->SendCloseGossip();
			}
			else
			{
				player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs a one-handed weapon.");
				player->PlayerTalkClass->SendCloseGossip();
			}
			break;
		case 8002:
			//weaponcheck required due because DK mainhand can be 1h or 2h.
			item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_OFFHAND);

			if (!item)
			{
				player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs a one-hand weapon equipped in the off-hand.");
				player->PlayerTalkClass->SendCloseGossip();
				return false;
			}

			if (item->GetTemplate()->InventoryType == INVTYPE_WEAPON)
			{
				Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_OFFHAND), RF_2);
				player->PlayerTalkClass->SendCloseGossip();
			}
			else
			{
				player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs a one-handed weapon.");
				player->PlayerTalkClass->SendCloseGossip();
			}
			break;
		case 8003:
			//weaponcheck required due because DK mainhand can be 1h or 2h.
			item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_OFFHAND);

			if (!item)
			{
				player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs a one-hand weapon equipped in the off-hand.");
				player->PlayerTalkClass->SendCloseGossip();
				return false;
			}

			if (item->GetTemplate()->InventoryType == INVTYPE_WEAPON)
			{
				Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_OFFHAND), RF_3);
				player->PlayerTalkClass->SendCloseGossip();
			}
			else
			{
				player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs a one-handed weapon.");
				player->PlayerTalkClass->SendCloseGossip();
			}
			break;
		case 8004:
			//weaponcheck required due because DK mainhand can be 1h or 2h.
			item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_OFFHAND);

			if (!item)
			{
				player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs a one-hand weapon equipped in the off-hand.");
				player->PlayerTalkClass->SendCloseGossip();
				return false;
			}

			if (item->GetTemplate()->InventoryType == INVTYPE_WEAPON)
			{
				Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_OFFHAND), RF_4);
				player->PlayerTalkClass->SendCloseGossip();
			}
			else
			{
				player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs a one-handed weapon.");
				player->PlayerTalkClass->SendCloseGossip();
			}
			break;
		case 8006:
			//weaponcheck required due because DK mainhand can be 1h or 2h.
			item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_OFFHAND);

			if (!item)
			{
				player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs a one-hand weapon equipped in the off-hand.");
				player->PlayerTalkClass->SendCloseGossip();
				return false;
			}

			if (item->GetTemplate()->InventoryType == INVTYPE_WEAPON)
			{
				Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_OFFHAND), RF_6);
				player->PlayerTalkClass->SendCloseGossip();
			}
			else
			{
				player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs a one-handed weapon.");
				player->PlayerTalkClass->SendCloseGossip();
			}
			break;
		case 8008:
			//weaponcheck required due because DK mainhand can be 1h or 2h.
			item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_OFFHAND);

			if (!item)
			{
				player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs a one-hand weapon equipped in the off-hand.");
				player->PlayerTalkClass->SendCloseGossip();
				return false;
			}

			if (item->GetTemplate()->InventoryType == INVTYPE_WEAPON)
			{
				Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_OFFHAND), RF_8);
				player->PlayerTalkClass->SendCloseGossip();
			}
			else
			{
				player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs a two-handed weapon.");
				player->PlayerTalkClass->SendCloseGossip();
			}
			break;
		case 8009:
			//weaponcheck required due because DK mainhand can be 1h or 2h.
			item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_OFFHAND);

			if (!item)
			{
				player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs a one-hand weapon equipped in the off-hand.");
				player->PlayerTalkClass->SendCloseGossip();
				return false;
			}

			if (item->GetTemplate()->InventoryType == INVTYPE_WEAPON)
			{
				Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_OFFHAND), RF_9);
				player->PlayerTalkClass->SendCloseGossip();
			}
			else
			{
				player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs a one-handed weapon.");
				player->PlayerTalkClass->SendCloseGossip();
			}
			break;





		case 100:
			//no weaponslot check required because only weapons can exist in the mainhand
			Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND), ENCHANT_WEP_BLOODY_DANCING_STEEL);
			player->PlayerTalkClass->SendCloseGossip();
			break;

		case 101:
			//no weaponslot check required because only weapons can exist in the mainhand
			Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND), ENCHANT_WEP_SPIRIT_OF_CONQUEST);
			player->PlayerTalkClass->SendCloseGossip();
			break;

		case 102:
			//no weaponslot check required because only weapons can exist in the mainhand
			Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND), ENCHANT_WEP_WINDSONG);
			player->PlayerTalkClass->SendCloseGossip();
			break;

		case 103:
			//no weaponslot check required because only weapons can exist in the mainhand
			Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND), ENCHANT_WEP_JADE_SPIRIT);
			player->PlayerTalkClass->SendCloseGossip();
			break;

		case 104:
			//no weaponslot check required because only weapons can exist in the mainhand
			Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND), ENCHANT_WEP_ELEMENTAL_FORCE);
			player->PlayerTalkClass->SendCloseGossip();
			break;

		case 105:
			//no weaponslot check required because only weapons can exist in the mainhand
			Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND), ENCHANT_WEP_DANCING_STEEL);
			player->PlayerTalkClass->SendCloseGossip();
			break;

		case 106:
			//no weaponslot check required because only weapons can exist in the mainhand
			Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND), ENCHANT_WEP_COLOSSUS);
			player->PlayerTalkClass->SendCloseGossip();
			break;

		case 107:
			//no weaponslot check required because only weapons can exist in the mainhand
			Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND), ENCHANT_WEP_RIVERS_SONG);
			player->PlayerTalkClass->SendCloseGossip();
			break;

		case 109: 
		{
					  //weaponcheck required due to item being an off-hand which can occupy multiple entities.
					  item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_OFFHAND);

					  if (!item)
					  {
						  player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs an off-hand equipped.");
						  player->PlayerTalkClass->SendCloseGossip();
						  return false;
					  }

					  if (item->GetTemplate()->InventoryType == INVTYPE_HOLDABLE || item->GetTemplate()->InventoryType == INVTYPE_SHIELD) //removing weapon tag as a test.
					  {
						  //because I still don't know what non-weapon/non-shield off-hands classify as.
						  Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_OFFHAND), ENCHANT_OFF_MAJOR_INTELLECT);
						  player->PlayerTalkClass->SendCloseGossip();
					  }
					  else
					  {
						  player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs an off-hand equipped.");
						  player->PlayerTalkClass->SendCloseGossip();
					  }
		}
			break;

		case 110: {
					  //weaponcheck required due to item being an off-hand which can occupy multiple entities.
					  item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_OFFHAND);

					  if (!item)
					  {
						  player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs a shield equipped in the off-hand.");
						  player->PlayerTalkClass->SendCloseGossip();
						  return false;
					  }

					  if (item->GetTemplate()->InventoryType == INVTYPE_SHIELD) //removing weapon tag as a test.
					  {
						  {
							  Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_OFFHAND), ENCHANT_WEP_JADE_SPIRIT);
							  player->PlayerTalkClass->SendCloseGossip();
						  }
					  }
					  else
					  {
						  player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs a one-hand weapon equipped in the off-hand.");
						  player->PlayerTalkClass->SendCloseGossip();
					  }
					  }
					  break;

		case 204:
		{
					//weaponcheck required due to item being an off-hand which can occupy multiple entities.
					item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_OFFHAND);

					if (!item)
					{
						player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs a one-hand weapon equipped in the off-hand.");
						player->PlayerTalkClass->SendCloseGossip();
						return false;
					}

					if (item->GetTemplate()->InventoryType == INVTYPE_WEAPON || item->GetTemplate()->InventoryType == INVTYPE_2HWEAPON)
					{
						Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_OFFHAND), ENCHANT_WEP_ELEMENTAL_FORCE);
						player->PlayerTalkClass->SendCloseGossip();
					}
					else
					{
						player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs a one-hand weapon equipped in the off-hand.");
						player->PlayerTalkClass->SendCloseGossip();
					}
		}
			break;

		case 205:
		{
					//weaponcheck required due to item being an off-hand which can occupy multiple entities.
					item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_OFFHAND);

					if (!item)
					{
						player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs a one-hand weapon equipped in the off-hand.");
						player->PlayerTalkClass->SendCloseGossip();
						return false;
					}

					if (item->GetTemplate()->InventoryType == INVTYPE_WEAPON || item->GetTemplate()->InventoryType == INVTYPE_2HWEAPON)
					{
						Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_OFFHAND), ENCHANT_WEP_DANCING_STEEL);
						player->PlayerTalkClass->SendCloseGossip();
					}
					else
					{
						player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs a one-hand weapon equipped in the off-hand.");
						player->PlayerTalkClass->SendCloseGossip();
					}
		}
			break;

		case 206:
		{
					//weaponcheck required due to item being an off-hand which can occupy multiple entities.
					item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_OFFHAND);

					if (!item)
					{
						player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs a one-hand weapon equipped in the off-hand.");
						player->PlayerTalkClass->SendCloseGossip();
						return false;
					}

					if (item->GetTemplate()->InventoryType == INVTYPE_WEAPON || item->GetTemplate()->InventoryType == INVTYPE_2HWEAPON)
					{
						Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_OFFHAND), ENCHANT_WEP_COLOSSUS);
						player->PlayerTalkClass->SendCloseGossip();
					}
					else
					{
						player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs a one-hand weapon equipped in the off-hand.");
						player->PlayerTalkClass->SendCloseGossip();
					}
		}
			break;

		case 207:
		{
					//weaponcheck required due to item being an off-hand which can occupy multiple entities.
					item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_OFFHAND);

					if (!item)
					{
						player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs a one-hand weapon equipped in the off-hand.");
						player->PlayerTalkClass->SendCloseGossip();
						return false;
					}

					if (item->GetTemplate()->InventoryType == INVTYPE_WEAPON || item->GetTemplate()->InventoryType == INVTYPE_2HWEAPON)
					{
						Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_OFFHAND), ENCHANT_WEP_RIVERS_SONG);
						player->PlayerTalkClass->SendCloseGossip();
					}
					else
					{
						player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs a one-hand weapon equipped in the off-hand.");
						player->PlayerTalkClass->SendCloseGossip();
					}
		}
			break;

		case 208:
		{
					item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_SHOULDERS);

					if (item->GetTemplate()->InventoryType == INVTYPE_SHOULDERS&&player->HasSkill(SKILL_INSCRIPTION)) //additional condition for inscription enchants)
					{
						Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_SHOULDERS), ENCHANT_SECRET_CRANE_WING);
						player->PlayerTalkClass->SendCloseGossip();
					}
					else
					{
						player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs a shoulder equipped.");
						player->PlayerTalkClass->SendCloseGossip();
					}
		}
			break;

		case 209:
		{
					item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_SHOULDERS);

					if (item->GetTemplate()->InventoryType == INVTYPE_SHOULDERS&&player->HasSkill(SKILL_INSCRIPTION)) //additional condition for inscription enchants)
					{
						Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_SHOULDERS), ENCHANT_SECRET_OX_HORN);
						player->PlayerTalkClass->SendCloseGossip();
					}
					else
					{
						player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs a shoulder equipped.");
						player->PlayerTalkClass->SendCloseGossip();
					}
		}
			break;

		case 210:
		{
					item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_SHOULDERS);

					if (item->GetTemplate()->InventoryType == INVTYPE_SHOULDERS&&player->HasSkill(SKILL_INSCRIPTION)) //additional condition for inscription enchants)
					{
						Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_SHOULDERS), ENCHANT_SECRET_TIGER_CLAW);
						player->PlayerTalkClass->SendCloseGossip();
					}
					else
					{
						player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs a shoulder equipped.");
						player->PlayerTalkClass->SendCloseGossip();
					}
		}
			break;

		case 211:
		{
					item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_SHOULDERS);

					if (item->GetTemplate()->InventoryType == INVTYPE_SHOULDERS&&player->HasSkill(SKILL_INSCRIPTION)) //additional condition for inscription enchants
					{
						Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_SHOULDERS), ENCHANT_SECRET_TIGER_FANG);
						player->PlayerTalkClass->SendCloseGossip();
					}
					else
					{
						player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs a shoulder equipped.");
						player->PlayerTalkClass->SendCloseGossip();
					}
		}
			break;
		
		case 3051:
		{
					item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_SHOULDERS);

					if (item->GetTemplate()->InventoryType == INVTYPE_SHOULDERS)
					{
						Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_SHOULDERS), ENCHANT_TIGER_FANG);
						player->PlayerTalkClass->SendCloseGossip();
					}
					else
					{
						player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs a shoulder equipped.");
						player->PlayerTalkClass->SendCloseGossip();
					}
		}
			break;
		
		case 3052:
		{
					item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_SHOULDERS);

					if (item->GetTemplate()->InventoryType == INVTYPE_SHOULDERS)
					{
						Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_SHOULDERS), ENCHANT_TIGER_CLAW);
						player->PlayerTalkClass->SendCloseGossip();
					}
					else
					{
						player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs a shoulder equipped.");
						player->PlayerTalkClass->SendCloseGossip();
					}
		}
			break;
			
		case 3053:
		{
					item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_SHOULDERS);

					if (item->GetTemplate()->InventoryType == INVTYPE_SHOULDERS)
					{
						Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_SHOULDERS), ENCHANT_CRANE_WING);
						player->PlayerTalkClass->SendCloseGossip();
					}
					else
					{
						player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs a shoulder equipped.");
						player->PlayerTalkClass->SendCloseGossip();
					}
		}
			break;
			
		case 3054:
		{
					item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_SHOULDERS);

					if (item->GetTemplate()->InventoryType == INVTYPE_SHOULDERS)
					{
						Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_SHOULDERS), ENCHANT_OX_HORN);
						player->PlayerTalkClass->SendCloseGossip();
					}
					else
					{
						player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs a shoulder equipped.");
						player->PlayerTalkClass->SendCloseGossip();
					}
		}
			break;
		
		case 3062:
		{
					item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_HANDS);

					if (item->GetTemplate()->InventoryType == INVTYPE_HANDS &&player->HasSkill(SKILL_BLACKSMITHING)) //additional condition for inscription enchants
					{
						Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_HANDS), GLOVE_SOCKET_BS);
						player->PlayerTalkClass->SendCloseGossip();
					}
					else
					{
						player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs a bracer equipped.");
						player->PlayerTalkClass->SendCloseGossip();
					}
		}
			break;
			
		case 3061:
		{
					item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_WRISTS);

					if (item->GetTemplate()->InventoryType == INVTYPE_WRISTS &&player->HasSkill(SKILL_BLACKSMITHING)) //additional condition for inscription enchants
					{
						Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_WRISTS ), BRACER_SOCKET_BS);
						player->PlayerTalkClass->SendCloseGossip();
					}
					else
					{
						player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs a bracer equipped.");
						player->PlayerTalkClass->SendCloseGossip();
					}
		}
			break;
		

		case 212:
		{
					item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_BACK);

					if (item->GetTemplate()->InventoryType == INVTYPE_CLOAK)
					{
						Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_BACK), ENCHANT_CLOAK_1);
						player->PlayerTalkClass->SendCloseGossip();
					}
					else
					{
						player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs a cloak equipped.");
						player->PlayerTalkClass->SendCloseGossip();
					}
		}
			break;

		case 213:
		{
					item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_BACK);

					if (item->GetTemplate()->InventoryType == INVTYPE_CLOAK)
					{
						Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_BACK), ENCHANT_CLOAK_2);
						player->PlayerTalkClass->SendCloseGossip();
					}
					else
					{
						player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs a cloak equipped.");
						player->PlayerTalkClass->SendCloseGossip();
					}
		}
			break;

		case 214:
		{
					item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_BACK);

					if (item->GetTemplate()->InventoryType == INVTYPE_CLOAK)
					{
						Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_BACK), ENCHANT_CLOAK_3);
						player->PlayerTalkClass->SendCloseGossip();
					}
					else
					{
						player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs a cloak equipped.");
						player->PlayerTalkClass->SendCloseGossip();
					}
		}
			break;

		case 215:
		{
					item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_BACK);

					if (item->GetTemplate()->InventoryType == INVTYPE_CLOAK)
					{
						Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_BACK), ENCHANT_CLOAK_4);
						player->PlayerTalkClass->SendCloseGossip();
					}
					else
					{
						player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs a cloak equipped.");
						player->PlayerTalkClass->SendCloseGossip();
					}
		}
			break;

		case 233:
		{
					item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_BACK);

					if (item->GetTemplate()->InventoryType == INVTYPE_CLOAK)
					{
						Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_BACK), 4893);
						player->PlayerTalkClass->SendCloseGossip();
					}
					else
					{
						player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs a cloak equipped.");
						player->PlayerTalkClass->SendCloseGossip();
					}
		}
			break;

		case 234:
		{
					item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_BACK);

					if (item->GetTemplate()->InventoryType == INVTYPE_CLOAK)
					{
						Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_BACK), 5110);
						player->PlayerTalkClass->SendCloseGossip();
					}
					else
					{
						player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs a cloak equipped.");
						player->PlayerTalkClass->SendCloseGossip();
					}
		}
			break;

		case 236:
		{
					item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_BACK);

					if (item->GetTemplate()->InventoryType == INVTYPE_CLOAK)
					{
						Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_BACK), 4894);
						player->PlayerTalkClass->SendCloseGossip();
					}
					else
					{
						player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs a cloak equipped.");
						player->PlayerTalkClass->SendCloseGossip();
					}
		}
			break;

		case 216:
		{
					item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_CHEST);

					if (item->GetTemplate()->InventoryType == INVTYPE_CHEST || item->GetTemplate()->InventoryType == INVTYPE_ROBE)
					{
						Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_CHEST), ENCHANT_CHEST_1);
						player->PlayerTalkClass->SendCloseGossip();
					}
					else
					{
						player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs a chest equipped.");
						player->PlayerTalkClass->SendCloseGossip();
					}
		}
			break;

		case 217:
		{
					item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_CHEST);

					if (item->GetTemplate()->InventoryType == INVTYPE_CHEST || item->GetTemplate()->InventoryType == INVTYPE_ROBE)
					{
						Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_CHEST), ENCHANT_CHEST_2);
						player->PlayerTalkClass->SendCloseGossip();
					}
					else
					{
						player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs a chest equipped.");
						player->PlayerTalkClass->SendCloseGossip();
					}
		}
			break;

		case 218:
		{
					item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_CHEST);

					if (item->GetTemplate()->InventoryType == INVTYPE_CHEST || item->GetTemplate()->InventoryType == INVTYPE_ROBE)
					{
						Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_CHEST), ENCHANT_CHEST_3);
						player->PlayerTalkClass->SendCloseGossip();
					}
					else
					{
						player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs a chest equipped.");
						player->PlayerTalkClass->SendCloseGossip();
					}
		}
			break;

		case 219:
		{
					item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_CHEST);

					if (item->GetTemplate()->InventoryType == INVTYPE_CHEST || item->GetTemplate()->InventoryType == INVTYPE_ROBE)
					{
						Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_CHEST), ENCHANT_CHEST_4);
						player->PlayerTalkClass->SendCloseGossip();
					}
					else
					{
						player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs a chest equipped.");
						player->PlayerTalkClass->SendCloseGossip();
					}
		}
			break;

		case 220:
		{
					item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_WRISTS);

					if (item->GetTemplate()->InventoryType == INVTYPE_WRISTS)
					{
						Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_WRISTS), ENCHANT_WRIST_1);
						player->PlayerTalkClass->SendCloseGossip();
					}
					else
					{
						player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs a wrist equipped.");
						player->PlayerTalkClass->SendCloseGossip();
					}
		}
			break;

		case 221:
		{
					item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_WRISTS);

					if (item->GetTemplate()->InventoryType == INVTYPE_WRISTS)
					{
						Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_WRISTS), ENCHANT_WRIST_2);
						player->PlayerTalkClass->SendCloseGossip();
					}
					else
					{
						player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs a wrist equipped.");
						player->PlayerTalkClass->SendCloseGossip();
					}
		}
			break;

		case 222:
		{
					item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_WRISTS);

					if (item->GetTemplate()->InventoryType == INVTYPE_WRISTS)
					{
						Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_WRISTS), ENCHANT_WRIST_3);
						player->PlayerTalkClass->SendCloseGossip();
					}
					else
					{
						player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs a wrist equipped.");
						player->PlayerTalkClass->SendCloseGossip();
					}
		}
			break;

		case 223:
		{
					item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_WRISTS);

					if (item->GetTemplate()->InventoryType == INVTYPE_WRISTS)
					{
						Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_WRISTS), ENCHANT_WRIST_4);
						player->PlayerTalkClass->SendCloseGossip();
					}
					else
					{
						player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs a wrist equipped.");
						player->PlayerTalkClass->SendCloseGossip();
					}
		}
			break;

		case 224:
		{
					item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_WRISTS);

					if (item->GetTemplate()->InventoryType == INVTYPE_WRISTS)
					{
						Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_WRISTS), ENCHANT_WRIST_5);
						player->PlayerTalkClass->SendCloseGossip();
					}
					else
					{
						player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs a wrist equipped.");
						player->PlayerTalkClass->SendCloseGossip();
					}
		}
			break;

		case 225:
		{
					item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_HANDS);

					if (item->GetTemplate()->InventoryType == INVTYPE_HANDS)
					{
						Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_HANDS), ENCHANT_GLOVES_1);
						player->PlayerTalkClass->SendCloseGossip();
					}
					else
					{
						player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs a glove equipped.");
						player->PlayerTalkClass->SendCloseGossip();
					}
		}
			break;

		case 226:
		{
					item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_HANDS);

					if (item->GetTemplate()->InventoryType == INVTYPE_HANDS)
					{
						Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_HANDS), ENCHANT_GLOVES_2);
						player->PlayerTalkClass->SendCloseGossip();
					}
					else
					{
						player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs a glove equipped.");
						player->PlayerTalkClass->SendCloseGossip();
					}
		}
			break;

		case 227:
		{
					item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_HANDS);

					if (item->GetTemplate()->InventoryType == INVTYPE_HANDS)
					{
						Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_HANDS), ENCHANT_GLOVES_3);
						player->PlayerTalkClass->SendCloseGossip();
					}
					else
					{
						player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs a glove equipped.");
						player->PlayerTalkClass->SendCloseGossip();
					}
		}
			break;

		case 228:
		{
					item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_HANDS);

					if (item->GetTemplate()->InventoryType == INVTYPE_HANDS)
					{
						Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_HANDS), ENCHANT_GLOVES_4);
						player->PlayerTalkClass->SendCloseGossip();
					}
					else
					{
						player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs a glove equipped.");
						player->PlayerTalkClass->SendCloseGossip();
					}
		}
			break;

		case 229:
		{
					item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_FEET);

					if (item->GetTemplate()->InventoryType == INVTYPE_FEET)
					{
						Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_FEET), ENCHANT_BOOTS_1);
						player->PlayerTalkClass->SendCloseGossip();
					}
					else
					{
						player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs boots equipped.");
						player->PlayerTalkClass->SendCloseGossip();
					}
		}
			break;

		case 230:
		{
					item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_FEET);

					if (item->GetTemplate()->InventoryType == INVTYPE_FEET)
					{
						Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_FEET), ENCHANT_BOOTS_2);
						player->PlayerTalkClass->SendCloseGossip();
					}
					else
					{
						player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs boots equipped.");
						player->PlayerTalkClass->SendCloseGossip();
					}
		}
			break;

		case 231:
		{
					item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_FEET);

					if (item->GetTemplate()->InventoryType == INVTYPE_FEET)
					{
						Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_FEET), ENCHANT_BOOTS_3);
						player->PlayerTalkClass->SendCloseGossip();
					}
					else
					{
						player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs boots equipped.");
						player->PlayerTalkClass->SendCloseGossip();
					}
		}
			break;

		case 232:
		{
					item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_FEET);

					if (item->GetTemplate()->InventoryType == INVTYPE_FEET)
					{
						Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_FEET), ENCHANT_BOOTS_4);
						player->PlayerTalkClass->SendCloseGossip();
					}
					else
					{
						player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs boots equipped.");
						player->PlayerTalkClass->SendCloseGossip();
					}
		}
			break;

		case 235:
		{
					item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_WAIST);

					if (item->GetTemplate()->InventoryType == INVTYPE_WAIST)
					{
						//Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_WAIST), BELT_SOCKET);
						Item* item = new Item; 
						SpellCastTargets* target = new SpellCastTargets();

						item = player->GetItemByEntry(90046);
						target->SetItemTarget(player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_WAIST));

						if (player->GetBagsFreeSlots() == 0 && !player->HasItemCount(90046)) {
							creature->MonsterWhisper("|cff00FF7FThis required one free bag slot or Living Steel Belt Buckle in your inventory.", player->GetGUID(), true);
						}
						else if (player->GetBagsFreeSlots() > 0 && !player->HasItemCount(90046)) {
							player->AddItem(90046, 1);
							creature->MonsterWhisper("|cff00FF7FAs a temporary fix, the enchant has been added to your inventory.", player->GetGUID(), true);
						}			

						//player->CastItemUseSpell(item, *target, 1, 0);
						//EnchantWithItem(player, player->GetItemByEntry(90046), player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_WAIST));
						player->PlayerTalkClass->SendCloseGossip();
					}
					else
					{
						player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs a belt equipped.");
						player->PlayerTalkClass->SendCloseGossip();
					}

					if (player->GetBagsFreeSlots() == 0 && !player->HasItemCount(90046)) {
						player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant requires one free bag slot.");
					}
					else if (player->GetBagsFreeSlots() > 0 && !player->HasItemCount(90046)) {
						player->AddItem(90046, 1);
						player->GetSession()->SendAreaTriggerMessage("|cff00FF7FAs a temporary fix, the enchant has been added to your inventory.");
					}
					else if (player->HasItemCount(90046)) {
						player->GetSession()->SendAreaTriggerMessage("|cff00FF7FYou already have Living Steel Belt Buckle in your inventory.");
					}

		}
			break;

		case 237:
		{
					item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_HANDS);

					if (item->GetTemplate()->InventoryType == INVTYPE_HANDS)
					{
						Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_HANDS), 3319);
						player->PlayerTalkClass->SendCloseGossip();
					}
					else
					{
						player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs a glove equipped.");
						player->PlayerTalkClass->SendCloseGossip();
					}
		}
			break;

		case 300:
		{
					item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND);

					if (item->GetTemplate()->InventoryType == INVTYPE_RANGED || item->GetTemplate()->InventoryType == INVTYPE_RANGEDRIGHT)
					{
						Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND), 4699);
						player->PlayerTalkClass->SendCloseGossip();
					}
					else
					{
						player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs a ranged weapon equipped.");
						player->PlayerTalkClass->SendCloseGossip();
					}
		}
			break;

		case 301:
		{
					item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND);

					if (item->GetTemplate()->InventoryType == INVTYPE_RANGED)
					{
						Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND), 4700);
						player->PlayerTalkClass->SendCloseGossip();
					}
					else
					{
						player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs a ranged weapon equipped.");
						player->PlayerTalkClass->SendCloseGossip();
					}
		}
			break;

		case 9500:
		{
					 item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_LEGS);

					 if (item->GetTemplate()->InventoryType == INVTYPE_LEGS)
					 {
						 Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_LEGS), 4823);
						 player->PlayerTalkClass->SendCloseGossip();
					 }
					 else
					 {
						 player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs legs equipped.");
						 player->PlayerTalkClass->SendCloseGossip();
					 }
		}
			break;

		case 9501:
		{
					 item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_LEGS);

					 if (item->GetTemplate()->InventoryType == INVTYPE_LEGS)
					 {
						 Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_LEGS), 4824);
						 player->PlayerTalkClass->SendCloseGossip();
					 }
					 else
					 {
						 player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs legs equipped.");
						 player->PlayerTalkClass->SendCloseGossip();
					 }
		}
			break;

		case 9502:
		{
					 item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_LEGS);

					 if (item->GetTemplate()->InventoryType == INVTYPE_LEGS)
					 {
						 Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_LEGS), 4822);
						 player->PlayerTalkClass->SendCloseGossip();
					 }
					 else
					 {
						 player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs legs equipped.");
						 player->PlayerTalkClass->SendCloseGossip();
					 }
		}
			break;

		case 9503:
		{
					 item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_LEGS);

					 if (item->GetTemplate()->InventoryType == INVTYPE_LEGS)
					 {
						 Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_LEGS), 4825);
						 player->PlayerTalkClass->SendCloseGossip();
					 }
					 else
					 {
						 player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs legs equipped.");
						 player->PlayerTalkClass->SendCloseGossip();
					 }
		}
			break;

		case 9504:
		{
					 item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_LEGS);

					 if (item->GetTemplate()->InventoryType == INVTYPE_LEGS)
					 {
						 Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_LEGS), 4826);
						 player->PlayerTalkClass->SendCloseGossip();
					 }
					 else
					 {
						 player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs legs equipped.");
						 player->PlayerTalkClass->SendCloseGossip();
					 }
		}
			break;

		case 9509:
		{
					 //weaponcheck required due to item being an off-hand which can occupy multiple entities.
					 item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_OFFHAND);

					 if (!item)
					 {
						 player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs a one-hand weapon equipped in the off-hand.");
						 player->PlayerTalkClass->SendCloseGossip();
						 return false;
					 }

					 if (item->GetTemplate()->InventoryType == INVTYPE_WEAPON || item->GetTemplate()->InventoryType == INVTYPE_2HWEAPON)
					 {
						 Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_OFFHAND), 4918);
						 player->PlayerTalkClass->SendCloseGossip();
					 }
					 else
					 {
						 player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs a one-hand weapon equipped in the off-hand.");
						 player->PlayerTalkClass->SendCloseGossip();
					 }
		}
			break;

		case 9510:
		{
					 //weaponcheck required due to item being an off-hand which can occupy multiple entities.
					 item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND);

					 if (!item)
					 {
						 player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs a one-hand weapon equipped in the off-hand.");
						 player->PlayerTalkClass->SendCloseGossip();
						 return false;
					 }

					 if (item->GetTemplate()->InventoryType == INVTYPE_WEAPON || item->GetTemplate()->InventoryType == INVTYPE_2HWEAPON)
					 {
						 Enchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND), 4918);
						 player->PlayerTalkClass->SendCloseGossip();
					 }
					 else
					 {
						 player->GetSession()->SendAreaTriggerMessage("|cff00FF7FThis enchant needs a weapon equipped in the main-hand.");
						 player->PlayerTalkClass->SendCloseGossip();
					 }
		}
			break;




			//------------------
			//ENCHANT REMOVAL
			//------------------

		case 50: //Remove enchant for mainhand
			item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND);

			if (!item)
			{
				creature->MonsterWhisper("|cff00FF7FYou must equip a weapon first!", player->GetGUID(), true);
				player->PlayerTalkClass->SendCloseGossip();
				return false;
			}
			RemoveEnchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND));
			player->PlayerTalkClass->SendCloseGossip();
			break;

		case 51: //Remove enchant for offhand
			item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_OFFHAND);

			if (!item)
			{
				creature->MonsterWhisper("|cff00FF7FYou must equip an off-hand weapon first!", player->GetGUID(), true);
				player->PlayerTalkClass->SendCloseGossip();
				return false;
			}
			RemoveEnchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_OFFHAND));
			player->PlayerTalkClass->SendCloseGossip();
			break;

		case 52: //Remove enchant for shoulders
			item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_SHOULDERS);

			if (!item)
			{
				creature->MonsterWhisper("|cff00FF7FYou must equip shoulders first!", player->GetGUID(), true);
				player->PlayerTalkClass->SendCloseGossip();
				return false;
			}
			RemoveEnchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_SHOULDERS));
			player->PlayerTalkClass->SendCloseGossip();
			break;

		case 53: //Remove enchant for CLOAK
			item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_BACK);

			if (!item)
			{
				creature->MonsterWhisper("|cff00FF7FYou must equip a cloak first!", player->GetGUID(), true);
				player->PlayerTalkClass->SendCloseGossip();
				return false;
			}
			RemoveEnchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_BACK));
			player->PlayerTalkClass->SendCloseGossip();
			break;

		case 54: //Remove enchant for CHEST
			item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_CHEST);

			if (!item)
			{
				creature->MonsterWhisper("|cff00FF7FYou must equip a chest first!", player->GetGUID(), true);
				player->PlayerTalkClass->SendCloseGossip();
				return false;
			}
			RemoveEnchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_CHEST));
			player->PlayerTalkClass->SendCloseGossip();
			break;

		case 55: //Remove enchant for WRIST
			item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_WRISTS);

			if (!item)
			{
				creature->MonsterWhisper("|cff00FF7FYou must equip wrists first!", player->GetGUID(), true);
				player->PlayerTalkClass->SendCloseGossip();
				return false;
			}
			RemoveEnchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_WRISTS));
			player->PlayerTalkClass->SendCloseGossip();
			break;

		case 56: //Remove enchant for GLOVES
			item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_HANDS);

			if (!item)
			{
				creature->MonsterWhisper("|cff00FF7FYou must equip gloves first!", player->GetGUID(), true);
				player->PlayerTalkClass->SendCloseGossip();
				return false;
			}
			RemoveEnchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_HANDS));
			player->PlayerTalkClass->SendCloseGossip();
			break;

		case 57: //Remove enchant for BOOTS
			item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_FEET);

			if (!item)
			{
				creature->MonsterWhisper("|cff00FF7FYou must equip boots first!", player->GetGUID(), true);
				player->PlayerTalkClass->SendCloseGossip();
				return false;
			}
			RemoveEnchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_FEET));
			player->PlayerTalkClass->SendCloseGossip();
			break;

		case 58: //Remove enchant for LEGS
			item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_LEGS);

			if (!item)
			{
				creature->MonsterWhisper("|cff00FF7FYou must equip legs first!", player->GetGUID(), true);
				player->PlayerTalkClass->SendCloseGossip();
				return false;
			}
			RemoveEnchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_LEGS));
			player->PlayerTalkClass->SendCloseGossip();
			break;

		case 59: //Remove enchant for belt
			item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_WAIST);

			if (!item)
			{
				creature->MonsterWhisper("|cff00FF7FYou must equip a belt first!", player->GetGUID(), true);
				player->PlayerTalkClass->SendCloseGossip();
				return false;
			}
			RemoveEnchant(player, player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_WAIST));
			player->PlayerTalkClass->SendCloseGossip();
			break;
		}
			return true;
		}
		
	};
	void AddSC_npc_enchantment()
	{
		new npc_enchantment();
	}