/* Custom Shadowhack PetGuy */

#include "ScriptPCH.h"
#include "ScriptedEscortAI.h"
#include "ScriptedFollowerAI.h"
#include "ScriptMgr.h"
#include "SpellScript.h"
#include "SpellAuraEffects.h"
#include "ScriptedCreature.h"
#include "ScriptedGossip.h"
#include "ScriptedEscortAI.h"
#include "ObjectMgr.h"
#include "ScriptMgr.h"
#include "World.h"
#include "PetAI.h"
#include "PassiveAI.h"
#include "CombatAI.h"
#include "GameEventMgr.h"
#include "GridNotifiers.h"
#include "GridNotifiersImpl.h"
#include "Cell.h"
#include "CellImpl.h"
#include "SpellAuras.h"
#include "Vehicle.h"
#include "Player.h"
#include "MoveSplineInit.h"
#include "VMapFactory.h"
#include "Chat.h"

#define GOSSIP_CHOOSE_FACTION		"I would like to choose my faction"
#define GOSSIP_ALLI			"I would like to join the Alliance"
#define GOSSIP_HORDE		"I would like to join the Horde"
#define GOSSIP_TEXT_EXP         1570
#define GOSSIP_TEXT_EXP_2         14743

class factionselect : public CreatureScript
{
public:
	factionselect() : CreatureScript("factionselect") { }

/**	bool OnGossipHello(Player* player, Creature* creature)
	{
		if (player->getRace() == RACE_PANDAREN_NEUTRAL)
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, GOSSIP_ALLI, GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 1);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, GOSSIP_HORDE, GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 2);

		player->PlayerTalkClass->SendGossipMenu(GOSSIP_TEXT_EXP, creature->GetGUID());
		return true;
	}

	bool OnGossipSelect(Player* player, Creature* , uint32 , uint32 action)
	{
		if (action == GOSSIP_ACTION_INFO_DEF + 1)
			player->setRace(RACE_PANDAREN_ALLI);
		else if (action == GOSSIP_ACTION_INFO_DEF + 2)
			player->setRace(RACE_PANDAREN_HORDE);

		player->PlayerTalkClass->SendCloseGossip();
		return true;
	}
}; **/

	bool OnGossipHello(Player* player, Creature* creature)
	{
		if (player->getRace() == RACE_PANDAREN_NEUTRAL) {
			player->PlayerTalkClass->SendGossipMenu(GOSSIP_TEXT_EXP, creature->GetGUID());
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, GOSSIP_CHOOSE_FACTION, GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 1);
		}
		else {
            creature->MonsterWhisper("|cff00FF7FYou are not a pandaren and/or already have a faction!", player->GetGUID(), true);
            //ChatHandler(player->GetSession()).PSendSysMessage("|cffffff00[|cffFFC125Serenity-WoW|cffffff00]: |cff00FF7FYou are not a pandaren and/or already have a faction!");
		}
		return true;
	}

	bool OnGossipSelect(Player* player, Creature* /*creature*/, uint32 /*sender*/, uint32 action)
	{
		if (action == GOSSIP_ACTION_INFO_DEF + 1)
			player->ShowNeutralPlayerFactionSelectUI();

		player->PlayerTalkClass->SendCloseGossip();
		return true;
	}
};

void AddSC_factionselect()
{
	new factionselect;
}