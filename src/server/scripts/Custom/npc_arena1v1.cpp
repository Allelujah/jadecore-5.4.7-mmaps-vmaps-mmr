/*
*
* Copyright (C) 2013 Emu-Devstore <http://emu-devstore.com/>
* Written by Teiby <http://www.teiby.de/>
*
*/

#include "ScriptMgr.h"
#include "Common.h"
#include "DisableMgr.h"
#include "BattlegroundMgr.h"
#include "Battleground.h"
#include "Language.h"


class npc_1v1arena : public CreatureScript
{
public:
	npc_1v1arena() : CreatureScript("npc_1v1arena")
	{
	}

	bool JoinQueueArena(Player* player, Creature* me, bool isRated)
	{
		if (!player || !me)
			return false;

		uint64 guid = player->GetGUID();
		uint8 arenatype = ARENA_TYPE_5v5;
		uint32 arenaRating = 0;
		uint32 matchmakerRating = 0;

		// ignore if we already in BG or BG queue
		if (player->IsInBattleground())
			return false;

		//check existance
		Battleground* bg = sBattlegroundMgr->GetBattlegroundTemplate(BATTLEGROUND_AA);
		if (!bg)
		{
			//TC_LOG_ERROR("network", "Battleground: template bg (all arenas) not found");
			return false;
		}

		if (DisableMgr::IsDisabledFor(DISABLE_TYPE_BATTLEGROUND, BATTLEGROUND_AA, NULL))
		{
			ChatHandler(player->GetSession()).PSendSysMessage(LANG_ARENA_DISABLED);
			return false;
		}

		BattlegroundTypeId bgTypeId = bg->GetTypeID();
		BattlegroundQueueTypeId bgQueueTypeId = BattlegroundMgr::BGQueueTypeId(bgTypeId, arenatype);

		PvPDifficultyEntry const* bracketEntry = GetBattlegroundBracketByLevel(bg->GetMapId(), player->getLevel());
		if (!bracketEntry)
			return false;

		// check if already in queue
		if (player->GetBattlegroundQueueIndex(bgQueueTypeId) < PLAYER_MAX_BATTLEGROUND_QUEUES)
			//player is already in this queue
			return false;
		// check if has free queue slots
		if (!player->HasFreeBattlegroundQueueId())
			return false;

		if (arenaRating <= 0)
			arenaRating = 1;
		if (matchmakerRating <= 0)
			matchmakerRating = 1;


		BattlegroundQueue &bgQueue = sBattlegroundMgr->GetBattlegroundQueue(bgQueueTypeId);

        GroupQueueInfo* ginfo = bgQueue.AddGroup(player, NULL, bgTypeId, bracketEntry, arenatype, true, false, arenaRating, matchmakerRating);
        uint32 avgTime = bgQueue.GetAverageQueueWaitTime(ginfo, bracketEntry->GetBracketId());
        uint32 queueSlot = player->AddBattlegroundQueueId(bgQueueTypeId);

        // add joined time data
        player->AddBattlegroundQueueJoinTime(bgTypeId, ginfo->JoinTime);

        WorldPacket data; // send status packet (in queue)
        sBattlegroundMgr->BuildBattlegroundStatusPacket(&data, bg, player, queueSlot, STATUS_WAIT_QUEUE, avgTime, ginfo->JoinTime, arenatype);
        player->GetSession()->SendPacket(&data);

		sBattlegroundMgr->ScheduleQueueUpdate(matchmakerRating, arenatype, bgQueueTypeId, bgTypeId, bracketEntry->GetBracketId());

		return true;
	}

	bool OnGossipHello(Player* player, Creature* me)
	{
		if (!player || !me)
			return true;

		if (player->InBattlegroundQueueForBattlegroundQueueType(BATTLEGROUND_QUEUE_5v5))
            me->MonsterWhisper("|cff00FF7FYou are already in queue for a 1v1 arena!", player->GetGUID(), true);
            //ChatHandler(player->GetSession()).PSendSysMessage("|cffffff00[|cffFFC125Serenity-WoW|cffffff00]: |cff00FF7FYou are already in queue for a 5v5 arena!");
			//player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_CHAT, "Leave queue 1v1 Arena", GOSSIP_SENDER_MAIN, 3, "Are you sure?", 0, false);
        else if (player->InBattlegroundQueueForBattlegroundQueueType(BATTLEGROUND_QUEUE_3v3)) {
            me->MonsterWhisper("|cff00FF7FYou are already in queue for a 3v3 arena, but you may leave and queue for 1v1!", player->GetGUID(), true);
            //ChatHandler(pPlayer->GetSession()).PSendSysMessage("|cffffff00[|cffFFC125Serenity-WoW|cffffff00]: |cff00FF7FYou are already in queue for a 3v3 arena, but you may queue for 2v2!");
            //pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "|TInterface/ICONS/achievement_arena_2v2_7.png:28:28:-16|tJoin 2v2 Skirmish Queue", GOSSIP_SENDER_MAIN, ARENA_TYPE_2v2);
        }
        else if (player->InBattlegroundQueueForBattlegroundQueueType(BATTLEGROUND_QUEUE_2v2)) {
            me->MonsterWhisper("|cff00FF7FYou are already in queue for a 2v2 arena, but you may leave and queue for 1v1!", player->GetGUID(), true);
            //ChatHandler(pPlayer->GetSession()).PSendSysMessage("|cffffff00[|cffFFC125Serenity-WoW|cffffff00]: |cff00FF7FYou are already in queue for a 2v2 arena, but you may queue for 3v3!");
            //pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "|TInterface/ICONS/achievement_arena_3v3_7.png:28:28:-16|tJoin 3v3 Skirmish Queue", GOSSIP_SENDER_MAIN, ARENA_TYPE_3v3);
        }
		else
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "|TInterface/ICONS/achievement_arena_5v5_7.png:28:28:-16|tJoin The 1v1 Arena Queue", GOSSIP_SENDER_MAIN, 20);


		switch (player->ToPlayer()->GetRoleForGroup(player->ToPlayer()->GetSpecializationId(player->ToPlayer()->GetActiveSpecialization())))
		{
		case ROLES_DPS:
			player->SEND_GOSSIP_MENU(68, me->GetGUID());
			break;
		case ROLES_HEALER:
		case ROLES_TANK:
			ChatHandler(player->GetSession()).PSendSysMessage("You can't join 1v1 Queue with Tank or Healer spec!");
			player->CLOSE_GOSSIP_MENU();
			break;
		default:
			ChatHandler(player->GetSession()).PSendSysMessage("You need to choose a specilization!");
		}
		return true;
	}

    enum Cases
    {
        JOIN    = 20,
        LEAVE   = 3
    };


	bool OnGossipSelect(Player* player, Creature* me, uint32 /*uiSender*/, uint32 uiAction)
	{
		if (!player || !me)
			return true;

		player->PlayerTalkClass->ClearMenus();

		switch (uiAction)
		{

        case JOIN: // Join Queue Arena (unrated)
		{
			if (JoinQueueArena(player, me, false) == false)
				ChatHandler(player->GetSession()).SendSysMessage("Something went wrong while join queue.");

			player->CLOSE_GOSSIP_MENU();
			return true;
		}
		break;

        case LEAVE: // Leave Queue
		{
            ChatHandler(player->GetSession()).SendSysMessage("Leave Queue Manually.");
            player->CLOSE_GOSSIP_MENU();
            return false; // disable npc leave, there is not possible anymore.

            ObjectGuid guid = player->GetGUID();
            uint8 arenatype = ARENA_TYPE_5v5;

            //check existance
            Battleground* bg = sBattlegroundMgr->GetBattlegroundTemplate(BATTLEGROUND_AA);
            if (!bg)
            {
                //TC_LOG_ERROR("network", "Battleground: template bg (all arenas) not found");
                return false;
            }

            if (DisableMgr::IsDisabledFor(DISABLE_TYPE_BATTLEGROUND, BATTLEGROUND_AA, NULL))
            {
                ChatHandler(player->GetSession()).PSendSysMessage(LANG_ARENA_DISABLED);
                return false;
            }

            BattlegroundTypeId bgTypeId = bg->GetTypeID();
            BattlegroundQueueTypeId bgQueueTypeId = BattlegroundMgr::BGQueueTypeId(bgTypeId, arenatype);
            uint32 queueSlot = player->GetBattlegroundQueueIndex(bgQueueTypeId);
            uint32 joinTime = player->GetBattlegroundQueueJoinTime(BATTLEGROUND_AA);

			WorldPacket* data;
            data->WriteBit(0x1);
            *data << uint32(queueSlot);
            *data << uint32(0);;
            *data << uint32(joinTime);;
            
            data->WriteBit(guid[6]);
            data->WriteBit(guid[7]);
            data->WriteBit(guid[1]);
            data->WriteBit(guid[2]);
            data->WriteBit(guid[0]);
            data->WriteBit(guid[5]);
            data->WriteBit(guid[4]);
            data->WriteBit(guid[3]);

            data->WriteByteSeq(guid[7]);
            data->WriteByteSeq(guid[2]);
            data->WriteByteSeq(guid[1]);
            data->WriteByteSeq(guid[5]);
            data->WriteByteSeq(guid[4]);
            data->WriteByteSeq(guid[0]);
            data->WriteByteSeq(guid[3]);
            data->WriteByteSeq(guid[6]);

            player->GetSession()->HandleLeaveBattlefieldOpcode(*data);
			player->CLOSE_GOSSIP_MENU();
			return true;
		}
		break;
		}

		OnGossipHello(player, me);
		return true;
	}
};


void AddSC_npc_1v1arena()
{
	new npc_1v1arena();
}