#include "ScriptPCH.h"

class npc_pvp_rewards : public CreatureScript
{
public:
	npc_pvp_rewards() : CreatureScript("npc_pvp_rewards") { }

	bool OnGossipHello(Player* player, Creature* creature)
	{
		player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_axe_2h_cataclysm_c_01.png:28:28:-16|t[-Honor Rewards-]", GOSSIP_SENDER_MAIN, 2);
		player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_helmet_142.png:28:28:-16|t[-Conquest Rewards-]", GOSSIP_SENDER_MAIN, 3);
		player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/ability_iyyokuk_sword_red.png:28:28:-16|t[-Rating Rewards-]", GOSSIP_SENDER_MAIN, 4);
		player->PlayerTalkClass->SendGossipMenu(100001, creature->GetGUID());
		return true;
	}

	bool OnGossipSelect(Player* player, Creature* creature, uint32 /*sender*/, uint32 action)
	{
		player->PlayerTalkClass->ClearMenus();

		switch (action)
		{

		case 4:
		{
				  if (player->GetArenaPersonalRating(ARENA_TYPE_2v2) >= 2200)// 2200 rating
				  {
					  player->PlayerTalkClass->ClearMenus();
					  //SendVendor(player, creature->GetGUID(), 999911); //vendor/creature id
					  player->PlayerTalkClass->SendGossipMenu(100001, creature->GetGUID());
					  
				  }
				  else
				  {
					  ChatHandler(player->GetSession()).PSendSysMessage("you don't have the required 2200 2V2 rating to open this vendor option!");
					  return false;
				  }
		}
			break;

		case 1: //<-Back menu
		{
					player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_axe_2h_cataclysm_c_01.png:28:28:-16|t[-Honor Rewards-]", GOSSIP_SENDER_MAIN, 2);
					player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_helmet_142.png:28:28:-16|t[-Conquest Rewards-]", GOSSIP_SENDER_MAIN, 3);
					player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/ability_iyyokuk_sword_red.png:28:28:-16|t[-Rating Rewards-]", GOSSIP_SENDER_MAIN, 4);
					player->PlayerTalkClass->SendGossipMenu(100001, creature->GetGUID());
					return true;
		}
			break;

		case 2:
		{
				  player->PlayerTalkClass->ClearMenus();
				  player->PlayerTalkClass->SendGossipMenu(79999, creature->GetGUID());
		}
			break;
		}
			return true;
		}
		
	};
	void AddSC_npc_pvp_rewards()
	{
		new npc_pvp_rewards();
	}