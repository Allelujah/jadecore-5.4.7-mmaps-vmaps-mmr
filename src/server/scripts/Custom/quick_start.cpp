#include "ScriptPCH.h"

/*
ScriptName	 : Template NPC
Date (DoC)   : 18/5/2015
Server		 : AdverseWoW
Status		 : Untested, verified compiling.
Credits 	 : Ayik0 & Lightning Blade

Done:
+ Fully gear starter system.

Features:
+ Implemented Security Check
+ Implemented Glyph Template
+ Implemented Talent Template
+ Implemented Gearing Template
+ Implemented Gemming Template
+ Implemented Enchanting Template
ToDO:
- Implement I-Icons to CLASS specs (Cleaner look, not needed)
- Add the socketting, gemming stuff, talents etc too lazy atm
*/
class npc_template : public CreatureScript
{
public:
	npc_template() : CreatureScript("npc_template") { }

	void Enchant(Player* player, Item* item, uint32 enchantid)
	{
		if (!item)
		{
			player->GetSession()->SendNotification("You must first equip the item you are trying to enchant in order to enchant it!");
			return;
		}
		player->ApplyEnchantment(item, PERM_ENCHANTMENT_SLOT, false);
		item->SetEnchantment(PERM_ENCHANTMENT_SLOT, enchantid, 0, 0);
		player->ApplyEnchantment(item, PERM_ENCHANTMENT_SLOT, true);
	}

	void SocketSlot(Player* player, Item* item, uint32 gemid, uint32 socketslot)
	{
		/*
		Basic Information overview on the usage of SocketSlot Method

		I implemented the uin32 socketslot argument, which basically determents
		which socket slot you're using ( 1 = First gem slot on the item, 2 = Second gem slot on the item, 3 = third gem slot on the item )
		So it could be used like this

		SocketSlot(player, player->GetItemByPos(EQUIPMENT_SLOT_HEAD), 0, 0);
		--
		Lets go over it,
		the first arugment which is player basically selects our player.
		--
		second argument selects our item based on position ( currently equipped item ).
		--
		Third arugment is the GemID, basically the gem you would like to gem into the item.
		--
		fourth argument is as explained above the socket slot ( 1 = first, 2 = second, 3 = third )
		--
		*/
		if (!item)
		{
			player->GetSession()->SendNotification("You must first equip the item you are trying to enchant in order to enchant it!");
			return;
		}
		if (socketslot == 2)
		{
			player->ApplyEnchantment(item, SOCK_ENCHANTMENT_SLOT_2, false);
			item->SetEnchantment(SOCK_ENCHANTMENT_SLOT_2, gemid, 0, 0);
			player->ApplyEnchantment(item, SOCK_ENCHANTMENT_SLOT_2, true);
		}
		else if (socketslot == 3)
		{
			player->ApplyEnchantment(item, SOCK_ENCHANTMENT_SLOT_3, false);
			item->SetEnchantment(SOCK_ENCHANTMENT_SLOT_3, gemid, 0, 0);
			player->ApplyEnchantment(item, SOCK_ENCHANTMENT_SLOT_3, true);
		}
		else
		{
			player->ApplyEnchantment(item, SOCK_ENCHANTMENT_SLOT, false);
			item->SetEnchantment(SOCK_ENCHANTMENT_SLOT, gemid, 0, 0);
			player->ApplyEnchantment(item, SOCK_ENCHANTMENT_SLOT, true);
		}
	}

bool OnGossipHello(Player* player, Creature* creature)
{
	if (!player->isInCombat())
	{
		switch (player->getClass())
		{
		case CLASS_WARRIOR:
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, "|cff800517 Arms", GOSSIP_SENDER_MAIN, 1);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, "|cff800517 Fury", GOSSIP_SENDER_MAIN, 2);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, "|cff800517 Protection", GOSSIP_SENDER_MAIN, 3);
			break;
		case CLASS_PALADIN:
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, "|cff800517 Holy", GOSSIP_SENDER_MAIN, 4);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, "|cff800517 Protection", GOSSIP_SENDER_MAIN, 5);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, "|cff800517 Retribution", GOSSIP_SENDER_MAIN, 6);
			break;
		case CLASS_HUNTER:
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, "|cff800517 Beastmastery", GOSSIP_SENDER_MAIN, 7);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, "|cff800517 Marksmanship", GOSSIP_SENDER_MAIN, 8);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, "|cff800517 Survival", GOSSIP_SENDER_MAIN, 9);
			break;
		case CLASS_ROGUE:
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, "|cff800517 Assassination", GOSSIP_SENDER_MAIN, 10);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, "|cff800517 Combat", GOSSIP_SENDER_MAIN, 11);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, "|cff800517 Sublety", GOSSIP_SENDER_MAIN, 12);
			break;
		case CLASS_PRIEST:
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, "|cff800517 Discipline", GOSSIP_SENDER_MAIN, 13);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, "|cff800517 Holy", GOSSIP_SENDER_MAIN, 14);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, "|cff800517 Shadow", GOSSIP_SENDER_MAIN, 15);
			break;
		case CLASS_DEATH_KNIGHT:
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, "|cff800517 Blood", GOSSIP_SENDER_MAIN, 16);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, "|cff800517 Frost", GOSSIP_SENDER_MAIN, 17);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, "|cff800517 Unholy", GOSSIP_SENDER_MAIN, 18);
			break;
		case CLASS_SHAMAN:
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, "|cff800517 Elemental", GOSSIP_SENDER_MAIN, 19);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, "|cff800517 Enhancement", GOSSIP_SENDER_MAIN, 20);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, "|cff800517 Restoration", GOSSIP_SENDER_MAIN, 21);
			break;
		case CLASS_MAGE:
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, "|cff800517 Arcane", GOSSIP_SENDER_MAIN, 22);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, "|cff800517 Fire", GOSSIP_SENDER_MAIN, 23);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, "|cff800517 Frost", GOSSIP_SENDER_MAIN, 24);
			break;
		case CLASS_WARLOCK:
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, "|cff800517 Affliction", GOSSIP_SENDER_MAIN, 25);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, "|cff800517 Demonology", GOSSIP_SENDER_MAIN, 26);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, "|cff800517 Destruction", GOSSIP_SENDER_MAIN, 27);
			break;
		case CLASS_DRUID:
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, "|cff800517 Balance", GOSSIP_SENDER_MAIN, 28);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, "|cff800517 Feral", GOSSIP_SENDER_MAIN, 29);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, "|cff800517 Restoration", GOSSIP_SENDER_MAIN, 30);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, "|cff800517 Guardian", GOSSIP_SENDER_MAIN, 31);
			break;
		case CLASS_MONK:
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, "|cff800517 Mistweaver", GOSSIP_SENDER_MAIN, 32);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, "|cff800517 Windwalker", GOSSIP_SENDER_MAIN, 33);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, "|cff800517 Brewmaster", GOSSIP_SENDER_MAIN, 34);
			break;
		}
		player->SEND_GOSSIP_MENU(68, creature->GetGUID());
	}
	return true;
}

bool OnGossipSelect(Player* player, Creature* creature, uint32 /*sender*/, uint32 actions)
{
#pragma region
	player->DestroyItem(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_HEAD, true);
	player->DestroyItem(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_NECK, true);
	player->DestroyItem(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_SHOULDERS, true);
	player->DestroyItem(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_CHEST, true);
	player->DestroyItem(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_BODY, true);
	player->DestroyItem(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_WAIST, true);
	player->DestroyItem(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_LEGS, true);
	player->DestroyItem(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_WRISTS, true);
	player->DestroyItem(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_HANDS, true);
	player->DestroyItem(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_FINGER1, true);
	player->DestroyItem(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_FINGER2, true);
	player->DestroyItem(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_TRINKET1, true);
	player->DestroyItem(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_TRINKET2, true);
	player->DestroyItem(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_BACK, true);
	player->DestroyItem(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND, true);
	player->DestroyItem(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_OFFHAND, true);
	player->DestroyItem(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_RANGED, true);
	player->DestroyItem(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_TABARD, true);
	player->DestroyItem(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_FEET, true);
#pragma endregion Destroying Current Equipped items

	switch (actions)
	{
	case 1:
		player->EquipNewItem(EQUIPMENT_SLOT_HEAD, 99959, true);
		player->EquipNewItem(EQUIPMENT_SLOT_NECK, 99946, true);
		player->EquipNewItem(EQUIPMENT_SLOT_SHOULDERS, 99961, true);
		player->EquipNewItem(EQUIPMENT_SLOT_CHEST, 99957, true);
		player->EquipNewItem(EQUIPMENT_SLOT_WAIST, 99887, true);
		player->EquipNewItem(EQUIPMENT_SLOT_LEGS, 99960, true);
		player->EquipNewItem(EQUIPMENT_SLOT_WRISTS, 99890, true);
		player->EquipNewItem(EQUIPMENT_SLOT_HANDS, 99958, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FINGER1, 99950, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FINGER2, 99949, true);
		player->EquipNewItem(EQUIPMENT_SLOT_TRINKET1, 99948, true);
		player->EquipNewItem(EQUIPMENT_SLOT_TRINKET2, 99943, true);
		player->EquipNewItem(EQUIPMENT_SLOT_BACK, 99944, true);
		player->EquipNewItem(EQUIPMENT_SLOT_MAINHAND, 99768, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FEET, 99888, true);
		break;
	case 2:
		player->EquipNewItem(EQUIPMENT_SLOT_HEAD, 99959, true);
		player->EquipNewItem(EQUIPMENT_SLOT_NECK, 99946, true);
		player->EquipNewItem(EQUIPMENT_SLOT_SHOULDERS, 99961, true);
		player->EquipNewItem(EQUIPMENT_SLOT_CHEST, 99957, true);
		player->EquipNewItem(EQUIPMENT_SLOT_WAIST, 99887, true);
		player->EquipNewItem(EQUIPMENT_SLOT_LEGS, 99960, true);
		player->EquipNewItem(EQUIPMENT_SLOT_WRISTS, 99891, true);
		player->EquipNewItem(EQUIPMENT_SLOT_HANDS, 99958, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FINGER1, 99950, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FINGER2, 99949, true);
		player->EquipNewItem(EQUIPMENT_SLOT_TRINKET1, 99948, true);
		player->EquipNewItem(EQUIPMENT_SLOT_TRINKET2, 99943, true);
		player->EquipNewItem(EQUIPMENT_SLOT_BACK, 99945, true);
		player->EquipNewItem(EQUIPMENT_SLOT_MAINHAND, 99768, true);
		player->EquipNewItem(EQUIPMENT_SLOT_OFFHAND, 99768, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FEET, 99888, true);
		break;
	case 3:
		player->EquipNewItem(EQUIPMENT_SLOT_HEAD, 99959, true);
		player->EquipNewItem(EQUIPMENT_SLOT_NECK, 99946, true);
		player->EquipNewItem(EQUIPMENT_SLOT_SHOULDERS, 99961, true);
		player->EquipNewItem(EQUIPMENT_SLOT_CHEST, 99957, true);
		player->EquipNewItem(EQUIPMENT_SLOT_WAIST, 99887, true);
		player->EquipNewItem(EQUIPMENT_SLOT_LEGS, 99960, true);
		player->EquipNewItem(EQUIPMENT_SLOT_WRISTS, 99891, true);
		player->EquipNewItem(EQUIPMENT_SLOT_HANDS, 99958, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FINGER1, 99950, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FINGER2, 99949, true);
		player->EquipNewItem(EQUIPMENT_SLOT_TRINKET1, 99948, true);
		player->EquipNewItem(EQUIPMENT_SLOT_TRINKET2, 99943, true);
		player->EquipNewItem(EQUIPMENT_SLOT_BACK, 99945, true);
		player->EquipNewItem(EQUIPMENT_SLOT_MAINHAND, 99940, true);
		player->EquipNewItem(EQUIPMENT_SLOT_OFFHAND, 99951, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FEET, 99888, true);
		break;
	case 4:
		player->EquipNewItem(EQUIPMENT_SLOT_HEAD, 99883, true);
		player->EquipNewItem(EQUIPMENT_SLOT_NECK, 99799, true);
		player->EquipNewItem(EQUIPMENT_SLOT_SHOULDERS, 99885, true);
		player->EquipNewItem(EQUIPMENT_SLOT_CHEST, 99881, true);
		player->EquipNewItem(EQUIPMENT_SLOT_WAIST, 99876, true);
		player->EquipNewItem(EQUIPMENT_SLOT_LEGS, 99884, true);
		player->EquipNewItem(EQUIPMENT_SLOT_WRISTS, 99880, true);
		player->EquipNewItem(EQUIPMENT_SLOT_HANDS, 99882, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FINGER1, 99800, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FINGER2, 99802, true);
		player->EquipNewItem(EQUIPMENT_SLOT_TRINKET1, 99840, true);
		player->EquipNewItem(EQUIPMENT_SLOT_TRINKET2, 99938, true);
		player->EquipNewItem(EQUIPMENT_SLOT_BACK, 99792, true);
		player->EquipNewItem(EQUIPMENT_SLOT_MAINHAND, 99835, true);
		player->EquipNewItem(EQUIPMENT_SLOT_OFFHAND, 99836, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FEET, 99878, true);
		break;
	case 5:
		player->EquipNewItem(EQUIPMENT_SLOT_HEAD, 99872, true);
		player->EquipNewItem(EQUIPMENT_SLOT_NECK, 99947, true);
		player->EquipNewItem(EQUIPMENT_SLOT_SHOULDERS, 99874, true);
		player->EquipNewItem(EQUIPMENT_SLOT_CHEST, 99870, true);
		player->EquipNewItem(EQUIPMENT_SLOT_WAIST, 99887, true);
		player->EquipNewItem(EQUIPMENT_SLOT_LEGS, 99873, true);
		player->EquipNewItem(EQUIPMENT_SLOT_WRISTS, 99891, true);
		player->EquipNewItem(EQUIPMENT_SLOT_HANDS, 99871, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FINGER1, 99950, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FINGER2, 99949, true);
		player->EquipNewItem(EQUIPMENT_SLOT_TRINKET1, 99948, true);
		player->EquipNewItem(EQUIPMENT_SLOT_TRINKET2, 99943, true);
		player->EquipNewItem(EQUIPMENT_SLOT_BACK, 99945, true);
		player->EquipNewItem(EQUIPMENT_SLOT_MAINHAND, 99942, true);
		player->EquipNewItem(EQUIPMENT_SLOT_OFFHAND, 99951, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FEET, 99888, true);
		break;
	case 6:
		player->EquipNewItem(EQUIPMENT_SLOT_HEAD, 99872, true);
		player->EquipNewItem(EQUIPMENT_SLOT_NECK, 99947, true);
		player->EquipNewItem(EQUIPMENT_SLOT_SHOULDERS, 99874, true);
		player->EquipNewItem(EQUIPMENT_SLOT_CHEST, 99870, true);
		player->EquipNewItem(EQUIPMENT_SLOT_WAIST, 99887, true);
		player->EquipNewItem(EQUIPMENT_SLOT_LEGS, 99873, true);
		player->EquipNewItem(EQUIPMENT_SLOT_WRISTS, 99891, true);
		player->EquipNewItem(EQUIPMENT_SLOT_HANDS, 99871, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FINGER1, 99950, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FINGER2, 99949, true);
		player->EquipNewItem(EQUIPMENT_SLOT_TRINKET1, 99948, true);
		player->EquipNewItem(EQUIPMENT_SLOT_TRINKET2, 99943, true);
		player->EquipNewItem(EQUIPMENT_SLOT_BACK, 99945, true);
		player->EquipNewItem(EQUIPMENT_SLOT_MAINHAND, 99768, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FEET, 99888, true);
		break;
	case 7:
		player->EquipNewItem(EQUIPMENT_SLOT_HEAD, 99849, true);
		player->EquipNewItem(EQUIPMENT_SLOT_NECK, 99776, true);
		player->EquipNewItem(EQUIPMENT_SLOT_SHOULDERS, 99851, true);
		player->EquipNewItem(EQUIPMENT_SLOT_CHEST, 99847, true);
		player->EquipNewItem(EQUIPMENT_SLOT_WAIST, 99841, true);
		player->EquipNewItem(EQUIPMENT_SLOT_LEGS, 99850, true);
		player->EquipNewItem(EQUIPMENT_SLOT_WRISTS, 99845, true);
		player->EquipNewItem(EQUIPMENT_SLOT_HANDS, 99848, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FINGER1, 99778, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FINGER2, 99779, true);
		player->EquipNewItem(EQUIPMENT_SLOT_TRINKET1, 99777, true);
		player->EquipNewItem(EQUIPMENT_SLOT_TRINKET2, 99772, true);
		player->EquipNewItem(EQUIPMENT_SLOT_BACK, 99774, true);
		player->EquipNewItem(EQUIPMENT_SLOT_MAINHAND, 99805, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FEET, 99843, true);
		break;
	case 8:
		player->EquipNewItem(EQUIPMENT_SLOT_HEAD, 99849, true);
		player->EquipNewItem(EQUIPMENT_SLOT_NECK, 99775, true);
		player->EquipNewItem(EQUIPMENT_SLOT_SHOULDERS, 99851, true);
		player->EquipNewItem(EQUIPMENT_SLOT_CHEST, 99847, true);
		player->EquipNewItem(EQUIPMENT_SLOT_WAIST, 99841, true);
		player->EquipNewItem(EQUIPMENT_SLOT_LEGS, 99850, true);
		player->EquipNewItem(EQUIPMENT_SLOT_WRISTS, 99846, true);
		player->EquipNewItem(EQUIPMENT_SLOT_HANDS, 99848, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FINGER1, 99778, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FINGER2, 99779, true);
		player->EquipNewItem(EQUIPMENT_SLOT_TRINKET1, 99777, true);
		player->EquipNewItem(EQUIPMENT_SLOT_TRINKET2, 99772, true);
		player->EquipNewItem(EQUIPMENT_SLOT_BACK, 99773, true);
		player->EquipNewItem(EQUIPMENT_SLOT_MAINHAND, 99805, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FEET, 99843, true);
		break;
	case 9:
		player->EquipNewItem(EQUIPMENT_SLOT_HEAD, 99849, true);
		player->EquipNewItem(EQUIPMENT_SLOT_NECK, 99776, true);
		player->EquipNewItem(EQUIPMENT_SLOT_SHOULDERS, 99851, true);
		player->EquipNewItem(EQUIPMENT_SLOT_CHEST, 99847, true);
		player->EquipNewItem(EQUIPMENT_SLOT_WAIST, 99842, true);
		player->EquipNewItem(EQUIPMENT_SLOT_LEGS, 99850, true);
		player->EquipNewItem(EQUIPMENT_SLOT_WRISTS, 99845, true);
		player->EquipNewItem(EQUIPMENT_SLOT_HANDS, 99848, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FINGER1, 99778, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FINGER2, 99779, true);
		player->EquipNewItem(EQUIPMENT_SLOT_TRINKET1, 99777, true);
		player->EquipNewItem(EQUIPMENT_SLOT_TRINKET2, 99772, true);
		player->EquipNewItem(EQUIPMENT_SLOT_BACK, 99773, true);
		player->EquipNewItem(EQUIPMENT_SLOT_MAINHAND, 99805, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FEET, 99844, true);
		break;
	case 10:
		player->EquipNewItem(EQUIPMENT_SLOT_HEAD, 99908, true);
		player->EquipNewItem(EQUIPMENT_SLOT_NECK, 99776, true);
		player->EquipNewItem(EQUIPMENT_SLOT_SHOULDERS, 99910, true);
		player->EquipNewItem(EQUIPMENT_SLOT_CHEST, 99906, true);
		player->EquipNewItem(EQUIPMENT_SLOT_WAIST, 99857, true);
		player->EquipNewItem(EQUIPMENT_SLOT_LEGS, 99909, true);
		player->EquipNewItem(EQUIPMENT_SLOT_WRISTS, 99905, true);
		player->EquipNewItem(EQUIPMENT_SLOT_HANDS, 99907, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FINGER1, 99778, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FINGER2, 99779, true);
		player->EquipNewItem(EQUIPMENT_SLOT_TRINKET1, 99772, true);
		player->EquipNewItem(EQUIPMENT_SLOT_TRINKET2, 99777, true);
		player->EquipNewItem(EQUIPMENT_SLOT_BACK, 99791, true);
		player->EquipNewItem(EQUIPMENT_SLOT_MAINHAND, 99932, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FEET, 99904, true);
		player->EquipNewItem(EQUIPMENT_SLOT_OFFHAND, 99932, true);
		break;
	case 11:
		player->EquipNewItem(EQUIPMENT_SLOT_HEAD, 99908, true);
		player->EquipNewItem(EQUIPMENT_SLOT_NECK, 99776, true);
		player->EquipNewItem(EQUIPMENT_SLOT_SHOULDERS, 99910, true);
		player->EquipNewItem(EQUIPMENT_SLOT_CHEST, 99906, true);
		player->EquipNewItem(EQUIPMENT_SLOT_WAIST, 99857, true);
		player->EquipNewItem(EQUIPMENT_SLOT_LEGS, 99909, true);
		player->EquipNewItem(EQUIPMENT_SLOT_WRISTS, 99905, true);
		player->EquipNewItem(EQUIPMENT_SLOT_HANDS, 99907, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FINGER1, 99778, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FINGER2, 99779, true);
		player->EquipNewItem(EQUIPMENT_SLOT_TRINKET1, 99772, true);
		player->EquipNewItem(EQUIPMENT_SLOT_TRINKET2, 99777, true);
		player->EquipNewItem(EQUIPMENT_SLOT_BACK, 99791, true);
		player->EquipNewItem(EQUIPMENT_SLOT_MAINHAND, 99769, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FEET, 99904, true);
		player->EquipNewItem(EQUIPMENT_SLOT_OFFHAND, 99769, true);
		break;
	case 12:
		player->EquipNewItem(EQUIPMENT_SLOT_HEAD, 99908, true);
		player->EquipNewItem(EQUIPMENT_SLOT_NECK, 99775, true);
		player->EquipNewItem(EQUIPMENT_SLOT_SHOULDERS, 99910, true);
		player->EquipNewItem(EQUIPMENT_SLOT_CHEST, 99906, true);
		player->EquipNewItem(EQUIPMENT_SLOT_WAIST, 99857, true);
		player->EquipNewItem(EQUIPMENT_SLOT_LEGS, 99909, true);
		player->EquipNewItem(EQUIPMENT_SLOT_WRISTS, 99859, true);
		player->EquipNewItem(EQUIPMENT_SLOT_HANDS, 99907, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FINGER1, 99778, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FINGER2, 99779, true);
		player->EquipNewItem(EQUIPMENT_SLOT_TRINKET1, 99772, true);
		player->EquipNewItem(EQUIPMENT_SLOT_TRINKET2, 99777, true);
		player->EquipNewItem(EQUIPMENT_SLOT_BACK, 99773, true);
		player->EquipNewItem(EQUIPMENT_SLOT_MAINHAND, 99932, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FEET, 999858, true);
		player->EquipNewItem(EQUIPMENT_SLOT_OFFHAND, 99932, true);
		break;
	case 13:
		player->EquipNewItem(EQUIPMENT_SLOT_HEAD, 99894, true);
		player->EquipNewItem(EQUIPMENT_SLOT_NECK, 9999799, true);
		player->EquipNewItem(EQUIPMENT_SLOT_SHOULDERS, 99897, true);
		player->EquipNewItem(EQUIPMENT_SLOT_CHEST, 99896, true);
		player->EquipNewItem(EQUIPMENT_SLOT_WAIST, 99783, true);
		player->EquipNewItem(EQUIPMENT_SLOT_LEGS, 99895, true);
		player->EquipNewItem(EQUIPMENT_SLOT_WRISTS, 99789, true);
		player->EquipNewItem(EQUIPMENT_SLOT_HANDS, 99893, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FINGER1, 99800, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FINGER2, 99802, true);
		player->EquipNewItem(EQUIPMENT_SLOT_TRINKET1, 99938, true);
		player->EquipNewItem(EQUIPMENT_SLOT_TRINKET2, 99840, true);
		player->EquipNewItem(EQUIPMENT_SLOT_BACK, 99792, true);
		player->EquipNewItem(EQUIPMENT_SLOT_MAINHAND, 99795, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FEET, 99786, true);
		player->EquipNewItem(EQUIPMENT_SLOT_OFFHAND, 99803, true);
		break;
	case 14:
		player->EquipNewItem(EQUIPMENT_SLOT_HEAD, 99894, true);
		player->EquipNewItem(EQUIPMENT_SLOT_NECK, 9999799, true);
		player->EquipNewItem(EQUIPMENT_SLOT_SHOULDERS, 99897, true);
		player->EquipNewItem(EQUIPMENT_SLOT_CHEST, 99896, true);
		player->EquipNewItem(EQUIPMENT_SLOT_WAIST, 99783, true);
		player->EquipNewItem(EQUIPMENT_SLOT_LEGS, 99895, true);
		player->EquipNewItem(EQUIPMENT_SLOT_WRISTS, 99789, true);
		player->EquipNewItem(EQUIPMENT_SLOT_HANDS, 99893, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FINGER1, 99800, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FINGER2, 99802, true);
		player->EquipNewItem(EQUIPMENT_SLOT_TRINKET1, 99938, true);
		player->EquipNewItem(EQUIPMENT_SLOT_TRINKET2, 99840, true);
		player->EquipNewItem(EQUIPMENT_SLOT_BACK, 99792, true);
		player->EquipNewItem(EQUIPMENT_SLOT_MAINHAND, 99795, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FEET, 99786, true);
		player->EquipNewItem(EQUIPMENT_SLOT_OFFHAND, 99803, true);
		break;
	case 15:
		player->EquipNewItem(EQUIPMENT_SLOT_HEAD, 99899, true);
		player->EquipNewItem(EQUIPMENT_SLOT_NECK, 99789, true);
		player->EquipNewItem(EQUIPMENT_SLOT_SHOULDERS, 99902, true);
		player->EquipNewItem(EQUIPMENT_SLOT_CHEST, 99901, true);
		player->EquipNewItem(EQUIPMENT_SLOT_WAIST, 99782, true);
		player->EquipNewItem(EQUIPMENT_SLOT_LEGS, 99900, true);
		player->EquipNewItem(EQUIPMENT_SLOT_WRISTS, 99788, true);
		player->EquipNewItem(EQUIPMENT_SLOT_HANDS, 99893, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FINGER1, 99800, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FINGER2, 99801, true);
		player->EquipNewItem(EQUIPMENT_SLOT_TRINKET1, 99938, true);
		player->EquipNewItem(EQUIPMENT_SLOT_TRINKET2, 99937, true);
		player->EquipNewItem(EQUIPMENT_SLOT_BACK, 99791, true);
		player->EquipNewItem(EQUIPMENT_SLOT_MAINHAND, 99795, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FEET, 99785, true);
		player->EquipNewItem(EQUIPMENT_SLOT_OFFHAND, 99793, true);
		break;
	case 16:
		player->EquipNewItem(EQUIPMENT_SLOT_HEAD, 99808, true);
		player->EquipNewItem(EQUIPMENT_SLOT_NECK, 99946, true);
		player->EquipNewItem(EQUIPMENT_SLOT_SHOULDERS, 99810, true);
		player->EquipNewItem(EQUIPMENT_SLOT_CHEST, 99806, true);
		player->EquipNewItem(EQUIPMENT_SLOT_WAIST, 99887, true);
		player->EquipNewItem(EQUIPMENT_SLOT_LEGS, 99809, true);
		player->EquipNewItem(EQUIPMENT_SLOT_WRISTS, 99891, true);
		player->EquipNewItem(EQUIPMENT_SLOT_HANDS, 99807, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FINGER1, 99949, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FINGER2, 99950, true);
		player->EquipNewItem(EQUIPMENT_SLOT_TRINKET1, 99948, true);
		player->EquipNewItem(EQUIPMENT_SLOT_TRINKET2, 99943, true);
		player->EquipNewItem(EQUIPMENT_SLOT_BACK, 99945, true);
		player->EquipNewItem(EQUIPMENT_SLOT_MAINHAND, 99768, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FEET, 99888, true);
		break;
	case 17:
		player->EquipNewItem(EQUIPMENT_SLOT_HEAD, 99808, true);
		player->EquipNewItem(EQUIPMENT_SLOT_NECK, 99946, true);
		player->EquipNewItem(EQUIPMENT_SLOT_SHOULDERS, 99810, true);
		player->EquipNewItem(EQUIPMENT_SLOT_CHEST, 99806, true);
		player->EquipNewItem(EQUIPMENT_SLOT_WAIST, 99887, true);
		player->EquipNewItem(EQUIPMENT_SLOT_LEGS, 99809, true);
		player->EquipNewItem(EQUIPMENT_SLOT_WRISTS, 99891, true);
		player->EquipNewItem(EQUIPMENT_SLOT_HANDS, 99807, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FINGER1, 99949, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FINGER2, 99950, true);
		player->EquipNewItem(EQUIPMENT_SLOT_TRINKET1, 99948, true);
		player->EquipNewItem(EQUIPMENT_SLOT_TRINKET2, 99943, true);
		player->EquipNewItem(EQUIPMENT_SLOT_BACK, 99945, true);
		player->EquipNewItem(EQUIPMENT_SLOT_MAINHAND, 99768, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FEET, 99888, true);
		break;
	case 18:
		player->EquipNewItem(EQUIPMENT_SLOT_HEAD, 99808, true);
		player->EquipNewItem(EQUIPMENT_SLOT_NECK, 99947, true);
		player->EquipNewItem(EQUIPMENT_SLOT_SHOULDERS, 99810, true);
		player->EquipNewItem(EQUIPMENT_SLOT_CHEST, 99806, true);
		player->EquipNewItem(EQUIPMENT_SLOT_WAIST, 99886, true);
		player->EquipNewItem(EQUIPMENT_SLOT_LEGS, 99809, true);
		player->EquipNewItem(EQUIPMENT_SLOT_WRISTS, 99891, true);
		player->EquipNewItem(EQUIPMENT_SLOT_HANDS, 99807, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FINGER1, 99949, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FINGER2, 99950, true);
		player->EquipNewItem(EQUIPMENT_SLOT_TRINKET1, 99948, true);
		player->EquipNewItem(EQUIPMENT_SLOT_TRINKET2, 99943, true);
		player->EquipNewItem(EQUIPMENT_SLOT_BACK, 99944, true);
		player->EquipNewItem(EQUIPMENT_SLOT_MAINHAND, 99768, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FEET, 99889, true);
		break;
	case 19:
		player->EquipNewItem(EQUIPMENT_SLOT_HEAD, 99929, true);
		player->EquipNewItem(EQUIPMENT_SLOT_NECK, 99798, true);
		player->EquipNewItem(EQUIPMENT_SLOT_SHOULDERS, 99931, true);
		player->EquipNewItem(EQUIPMENT_SLOT_CHEST, 99927, true);
		player->EquipNewItem(EQUIPMENT_SLOT_WAIST, 99926, true);
		player->EquipNewItem(EQUIPMENT_SLOT_LEGS, 99930, true);
		player->EquipNewItem(EQUIPMENT_SLOT_WRISTS, 99914, true);
		player->EquipNewItem(EQUIPMENT_SLOT_HANDS, 99928, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FINGER1, 99800, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FINGER2, 99801, true);
		player->EquipNewItem(EQUIPMENT_SLOT_TRINKET1, 99937, true);
		player->EquipNewItem(EQUIPMENT_SLOT_TRINKET2, 94386, true);
		player->EquipNewItem(EQUIPMENT_SLOT_BACK, 99791, true);
		player->EquipNewItem(EQUIPMENT_SLOT_MAINHAND, 99795, true);
		player->EquipNewItem(EQUIPMENT_SLOT_OFFHAND, 99794, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FEET, 99912, true);
		break;
	case 20:
		player->EquipNewItem(EQUIPMENT_SLOT_HEAD, 99923, true);
		player->EquipNewItem(EQUIPMENT_SLOT_NECK, 99776, true);
		player->EquipNewItem(EQUIPMENT_SLOT_SHOULDERS, 99925, true);
		player->EquipNewItem(EQUIPMENT_SLOT_CHEST, 99921, true);
		player->EquipNewItem(EQUIPMENT_SLOT_WAIST, 99841, true);
		player->EquipNewItem(EQUIPMENT_SLOT_LEGS, 99924, true);
		player->EquipNewItem(EQUIPMENT_SLOT_WRISTS, 99845, true);
		player->EquipNewItem(EQUIPMENT_SLOT_HANDS, 99922, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FINGER1, 99779, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FINGER2, 99778, true);
		player->EquipNewItem(EQUIPMENT_SLOT_TRINKET1, 94386, true);
		player->EquipNewItem(EQUIPMENT_SLOT_TRINKET2, 99777, true);
		player->EquipNewItem(EQUIPMENT_SLOT_BACK, 99774, true);
		player->EquipNewItem(EQUIPMENT_SLOT_MAINHAND, 99769, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FEET, 99843, true);
		player->EquipNewItem(EQUIPMENT_SLOT_OFFHAND, 99769, true);
		break;
	case 21:
		player->EquipNewItem(EQUIPMENT_SLOT_HEAD, 99918, true);
		player->EquipNewItem(EQUIPMENT_SLOT_NECK, 99799, true);
		player->EquipNewItem(EQUIPMENT_SLOT_SHOULDERS, 99920, true);
		player->EquipNewItem(EQUIPMENT_SLOT_CHEST, 99916, true);
		player->EquipNewItem(EQUIPMENT_SLOT_WAIST, 99911, true);
		player->EquipNewItem(EQUIPMENT_SLOT_LEGS, 99919, true);
		player->EquipNewItem(EQUIPMENT_SLOT_WRISTS, 99915, true);
		player->EquipNewItem(EQUIPMENT_SLOT_HANDS, 99917, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FINGER1, 99800, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FINGER2, 99802, true);
		player->EquipNewItem(EQUIPMENT_SLOT_TRINKET1, 99840, true);
		player->EquipNewItem(EQUIPMENT_SLOT_TRINKET2, 94386, true);
		player->EquipNewItem(EQUIPMENT_SLOT_BACK, 99792, true);
		player->EquipNewItem(EQUIPMENT_SLOT_MAINHAND, 99795, true);
		player->EquipNewItem(EQUIPMENT_SLOT_OFFHAND, 99836, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FEET, 99913, true);
		break;
	case 22:
		player->EquipNewItem(EQUIPMENT_SLOT_HEAD, 99853, true);
		player->EquipNewItem(EQUIPMENT_SLOT_NECK, 99797, true);
		player->EquipNewItem(EQUIPMENT_SLOT_SHOULDERS, 99856, true);
		player->EquipNewItem(EQUIPMENT_SLOT_CHEST, 91238, true);
		player->EquipNewItem(EQUIPMENT_SLOT_WAIST, 99781, true);
		player->EquipNewItem(EQUIPMENT_SLOT_LEGS, 99854, true);
		player->EquipNewItem(EQUIPMENT_SLOT_WRISTS, 99788, true);
		player->EquipNewItem(EQUIPMENT_SLOT_HANDS, 99852, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FINGER1, 99800, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FINGER2, 99801, true);
		player->EquipNewItem(EQUIPMENT_SLOT_TRINKET1, 99937, true);
		player->EquipNewItem(EQUIPMENT_SLOT_TRINKET2, 99938, true);
		player->EquipNewItem(EQUIPMENT_SLOT_BACK, 99791, true);
		player->EquipNewItem(EQUIPMENT_SLOT_MAINHAND, 99936, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FEET, 99784, true);
		player->EquipNewItem(EQUIPMENT_SLOT_OFFHAND, 99793, true);
		break;
	case 23:
		player->EquipNewItem(EQUIPMENT_SLOT_HEAD, 99853, true);
		player->EquipNewItem(EQUIPMENT_SLOT_NECK, 99797, true);
		player->EquipNewItem(EQUIPMENT_SLOT_SHOULDERS, 99856, true);
		player->EquipNewItem(EQUIPMENT_SLOT_CHEST, 91238, true);
		player->EquipNewItem(EQUIPMENT_SLOT_WAIST, 99781, true);
		player->EquipNewItem(EQUIPMENT_SLOT_LEGS, 99854, true);
		player->EquipNewItem(EQUIPMENT_SLOT_WRISTS, 99787, true);
		player->EquipNewItem(EQUIPMENT_SLOT_HANDS, 99852, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FINGER1, 99800, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FINGER2, 99801, true);
		player->EquipNewItem(EQUIPMENT_SLOT_TRINKET1, 99937, true);
		player->EquipNewItem(EQUIPMENT_SLOT_TRINKET2, 99938, true);
		player->EquipNewItem(EQUIPMENT_SLOT_BACK, 99790, true);
		player->EquipNewItem(EQUIPMENT_SLOT_MAINHAND, 99936, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FEET, 99784, true);
		player->EquipNewItem(EQUIPMENT_SLOT_OFFHAND, 99793, true);
		break;
	case 24:
		player->EquipNewItem(EQUIPMENT_SLOT_HEAD, 99853, true);
		player->EquipNewItem(EQUIPMENT_SLOT_NECK, 99797, true);
		player->EquipNewItem(EQUIPMENT_SLOT_SHOULDERS, 99856, true);
		player->EquipNewItem(EQUIPMENT_SLOT_CHEST, 91238, true);
		player->EquipNewItem(EQUIPMENT_SLOT_WAIST, 99781, true);
		player->EquipNewItem(EQUIPMENT_SLOT_LEGS, 99854, true);
		player->EquipNewItem(EQUIPMENT_SLOT_WRISTS, 99788, true);
		player->EquipNewItem(EQUIPMENT_SLOT_HANDS, 99852, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FINGER1, 99800, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FINGER2, 99801, true);
		player->EquipNewItem(EQUIPMENT_SLOT_TRINKET1, 99937, true);
		player->EquipNewItem(EQUIPMENT_SLOT_TRINKET2, 99938, true);
		player->EquipNewItem(EQUIPMENT_SLOT_BACK, 99791, true);
		player->EquipNewItem(EQUIPMENT_SLOT_MAINHAND, 99936, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FEET, 99784, true);
		player->EquipNewItem(EQUIPMENT_SLOT_OFFHAND, 99793, true);
		break;
	case 25:
		player->EquipNewItem(EQUIPMENT_SLOT_HEAD, 99953, true);
		player->EquipNewItem(EQUIPMENT_SLOT_NECK, 99797, true);
		player->EquipNewItem(EQUIPMENT_SLOT_SHOULDERS, 99956, true);
		player->EquipNewItem(EQUIPMENT_SLOT_CHEST, 99955, true);
		player->EquipNewItem(EQUIPMENT_SLOT_WAIST, 99781, true);
		player->EquipNewItem(EQUIPMENT_SLOT_LEGS, 99954, true);
		player->EquipNewItem(EQUIPMENT_SLOT_WRISTS, 99788, true);
		player->EquipNewItem(EQUIPMENT_SLOT_HANDS, 99952, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FINGER1, 99800, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FINGER2, 99801, true);
		player->EquipNewItem(EQUIPMENT_SLOT_TRINKET1, 99937, true);
		player->EquipNewItem(EQUIPMENT_SLOT_TRINKET2, 99938, true);
		player->EquipNewItem(EQUIPMENT_SLOT_BACK, 99791, true);
		player->EquipNewItem(EQUIPMENT_SLOT_MAINHAND, 99936, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FEET, 99784, true);
		player->EquipNewItem(EQUIPMENT_SLOT_OFFHAND, 99793, true);
		break;
	case 26:
		player->EquipNewItem(EQUIPMENT_SLOT_HEAD, 99953, true);
		player->EquipNewItem(EQUIPMENT_SLOT_NECK, 99797, true);
		player->EquipNewItem(EQUIPMENT_SLOT_SHOULDERS, 99956, true);
		player->EquipNewItem(EQUIPMENT_SLOT_CHEST, 99955, true);
		player->EquipNewItem(EQUIPMENT_SLOT_WAIST, 99781, true);
		player->EquipNewItem(EQUIPMENT_SLOT_LEGS, 99954, true);
		player->EquipNewItem(EQUIPMENT_SLOT_WRISTS, 99788, true);
		player->EquipNewItem(EQUIPMENT_SLOT_HANDS, 99952, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FINGER1, 99800, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FINGER2, 99801, true);
		player->EquipNewItem(EQUIPMENT_SLOT_TRINKET1, 99937, true);
		player->EquipNewItem(EQUIPMENT_SLOT_TRINKET2, 99938, true);
		player->EquipNewItem(EQUIPMENT_SLOT_BACK, 99791, true);
		player->EquipNewItem(EQUIPMENT_SLOT_MAINHAND, 99936, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FEET, 99784, true);
		player->EquipNewItem(EQUIPMENT_SLOT_OFFHAND, 99793, true);
		break;
	case 27:
		player->EquipNewItem(EQUIPMENT_SLOT_HEAD, 99953, true);
		player->EquipNewItem(EQUIPMENT_SLOT_NECK, 99797, true);
		player->EquipNewItem(EQUIPMENT_SLOT_SHOULDERS, 99956, true);
		player->EquipNewItem(EQUIPMENT_SLOT_CHEST, 99955, true);
		player->EquipNewItem(EQUIPMENT_SLOT_WAIST, 99781, true);
		player->EquipNewItem(EQUIPMENT_SLOT_LEGS, 99954, true);
		player->EquipNewItem(EQUIPMENT_SLOT_WRISTS, 99787, true);
		player->EquipNewItem(EQUIPMENT_SLOT_HANDS, 99952, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FINGER1, 99800, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FINGER2, 99801, true);
		player->EquipNewItem(EQUIPMENT_SLOT_TRINKET1, 99937, true);
		player->EquipNewItem(EQUIPMENT_SLOT_TRINKET2, 99938, true);
		player->EquipNewItem(EQUIPMENT_SLOT_BACK, 99790, true);
		player->EquipNewItem(EQUIPMENT_SLOT_MAINHAND, 99936, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FEET, 99785, true);
		player->EquipNewItem(EQUIPMENT_SLOT_OFFHAND, 99793, true);
		break;
	case 28:
		player->EquipNewItem(EQUIPMENT_SLOT_HEAD, 99829, true);
		player->EquipNewItem(EQUIPMENT_SLOT_NECK, 99797, true);
		player->EquipNewItem(EQUIPMENT_SLOT_SHOULDERS, 99832, true);
		player->EquipNewItem(EQUIPMENT_SLOT_CHEST, 99831, true);
		player->EquipNewItem(EQUIPMENT_SLOT_WAIST, 99825, true);
		player->EquipNewItem(EQUIPMENT_SLOT_LEGS, 99830, true);
		player->EquipNewItem(EQUIPMENT_SLOT_WRISTS, 99827, true);
		player->EquipNewItem(EQUIPMENT_SLOT_HANDS, 99828, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FINGER1, 99800, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FINGER2, 99801, true);
		player->EquipNewItem(EQUIPMENT_SLOT_TRINKET1, 99937, true);
		player->EquipNewItem(EQUIPMENT_SLOT_TRINKET2, 94386, true);
		player->EquipNewItem(EQUIPMENT_SLOT_BACK, 99791, true);
		player->EquipNewItem(EQUIPMENT_SLOT_MAINHAND, 99795, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FEET, 99826, true);
		player->EquipNewItem(EQUIPMENT_SLOT_OFFHAND, 99793, true);
		break;
	case 29:
		player->EquipNewItem(EQUIPMENT_SLOT_HEAD, 99812, true);
		player->EquipNewItem(EQUIPMENT_SLOT_NECK, 99776, true);
		player->EquipNewItem(EQUIPMENT_SLOT_SHOULDERS, 99815, true);
		player->EquipNewItem(EQUIPMENT_SLOT_CHEST, 99814, true);
		player->EquipNewItem(EQUIPMENT_SLOT_WAIST, 99857, true);
		player->EquipNewItem(EQUIPMENT_SLOT_LEGS, 99813, true);
		player->EquipNewItem(EQUIPMENT_SLOT_WRISTS, 99905, true);
		player->EquipNewItem(EQUIPMENT_SLOT_HANDS, 99811, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FINGER1, 99778, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FINGER2, 99779, true);
		player->EquipNewItem(EQUIPMENT_SLOT_TRINKET1, 99772, true);
		player->EquipNewItem(EQUIPMENT_SLOT_TRINKET2, 94386, true);
		player->EquipNewItem(EQUIPMENT_SLOT_BACK, 99774, true);
		player->EquipNewItem(EQUIPMENT_SLOT_MAINHAND, 99824, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FEET, 99904, true);
		break;
	case 30:
		player->EquipNewItem(EQUIPMENT_SLOT_HEAD, 99820, true);
		player->EquipNewItem(EQUIPMENT_SLOT_NECK, 99799, true);
		player->EquipNewItem(EQUIPMENT_SLOT_SHOULDERS, 99823, true);
		player->EquipNewItem(EQUIPMENT_SLOT_CHEST, 99822, true);
		player->EquipNewItem(EQUIPMENT_SLOT_WAIST, 99816, true);
		player->EquipNewItem(EQUIPMENT_SLOT_LEGS, 99821, true);
		player->EquipNewItem(EQUIPMENT_SLOT_WRISTS, 99818, true);
		player->EquipNewItem(EQUIPMENT_SLOT_HANDS, 99819, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FINGER1, 99800, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FINGER2, 99802, true);
		player->EquipNewItem(EQUIPMENT_SLOT_TRINKET1, 100058, true);
		player->EquipNewItem(EQUIPMENT_SLOT_TRINKET2, 99840, true);
		player->EquipNewItem(EQUIPMENT_SLOT_BACK, 99792, true);
		player->EquipNewItem(EQUIPMENT_SLOT_MAINHAND, 99795, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FEET, 99817, true);
		player->EquipNewItem(EQUIPMENT_SLOT_OFFHAND, 99803, true);
		break;
	case 31:
		player->EquipNewItem(EQUIPMENT_SLOT_HEAD, 99812, true);
		player->EquipNewItem(EQUIPMENT_SLOT_NECK, 99776, true);
		player->EquipNewItem(EQUIPMENT_SLOT_SHOULDERS, 99815, true);
		player->EquipNewItem(EQUIPMENT_SLOT_CHEST, 99814, true);
		player->EquipNewItem(EQUIPMENT_SLOT_WAIST, 99857, true);
		player->EquipNewItem(EQUIPMENT_SLOT_LEGS, 99813, true);
		player->EquipNewItem(EQUIPMENT_SLOT_WRISTS, 99905, true);
		player->EquipNewItem(EQUIPMENT_SLOT_HANDS, 99811, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FINGER1, 99778, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FINGER2, 99779, true);
		player->EquipNewItem(EQUIPMENT_SLOT_TRINKET1, 100031, true);
		player->EquipNewItem(EQUIPMENT_SLOT_TRINKET2, 99839, true);
		player->EquipNewItem(EQUIPMENT_SLOT_BACK, 99774, true);
		player->EquipNewItem(EQUIPMENT_SLOT_MAINHAND, 99824, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FEET, 99904, true);
		break;
	case 32:
		player->EquipNewItem(EQUIPMENT_SLOT_HEAD, 99866, true);
		player->EquipNewItem(EQUIPMENT_SLOT_NECK, 99799, true);
		player->EquipNewItem(EQUIPMENT_SLOT_SHOULDERS, 99868, true);
		player->EquipNewItem(EQUIPMENT_SLOT_CHEST, 99869, true);
		player->EquipNewItem(EQUIPMENT_SLOT_WAIST, 99816, true);
		player->EquipNewItem(EQUIPMENT_SLOT_LEGS, 99867, true);
		player->EquipNewItem(EQUIPMENT_SLOT_WRISTS, 99818, true);
		player->EquipNewItem(EQUIPMENT_SLOT_HANDS, 99865, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FINGER1, 99778, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FINGER2, 99779, true);
		player->EquipNewItem(EQUIPMENT_SLOT_TRINKET1, 99938, true);
		player->EquipNewItem(EQUIPMENT_SLOT_TRINKET2, 99840, true);
		player->EquipNewItem(EQUIPMENT_SLOT_BACK, 99792, true);
		player->EquipNewItem(EQUIPMENT_SLOT_MAINHAND, 99835, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FEET, 99817, true);
		player->EquipNewItem(EQUIPMENT_SLOT_OFFHAND, 99803, true);
		break;
	case 33:
		player->EquipNewItem(EQUIPMENT_SLOT_HEAD, 99861, true);
		player->EquipNewItem(EQUIPMENT_SLOT_NECK, 99776, true);
		player->EquipNewItem(EQUIPMENT_SLOT_SHOULDERS, 99863, true);
		player->EquipNewItem(EQUIPMENT_SLOT_CHEST, 99864, true);
		player->EquipNewItem(EQUIPMENT_SLOT_WAIST, 99857, true);
		player->EquipNewItem(EQUIPMENT_SLOT_LEGS, 99862, true);
		player->EquipNewItem(EQUIPMENT_SLOT_WRISTS, 99905, true);
		player->EquipNewItem(EQUIPMENT_SLOT_HANDS, 99860, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FINGER1, 99778, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FINGER2, 99779, true);
		player->EquipNewItem(EQUIPMENT_SLOT_TRINKET1, 99772, true);
		player->EquipNewItem(EQUIPMENT_SLOT_TRINKET2, 99777, true);
		player->EquipNewItem(EQUIPMENT_SLOT_BACK, 99774, true);
		player->EquipNewItem(EQUIPMENT_SLOT_MAINHAND, 99769, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FEET, 99904, true);
		player->EquipNewItem(EQUIPMENT_SLOT_OFFHAND, 99769, true);
		break;
	case 34:
		player->EquipNewItem(EQUIPMENT_SLOT_HEAD, 99861, true);
		player->EquipNewItem(EQUIPMENT_SLOT_NECK, 99776, true);
		player->EquipNewItem(EQUIPMENT_SLOT_SHOULDERS, 99863, true);
		player->EquipNewItem(EQUIPMENT_SLOT_CHEST, 99864, true);
		player->EquipNewItem(EQUIPMENT_SLOT_WAIST, 99857, true);
		player->EquipNewItem(EQUIPMENT_SLOT_LEGS, 99862, true);
		player->EquipNewItem(EQUIPMENT_SLOT_WRISTS, 99905, true);
		player->EquipNewItem(EQUIPMENT_SLOT_HANDS, 99860, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FINGER1, 99778, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FINGER2, 99779, true);
		player->EquipNewItem(EQUIPMENT_SLOT_TRINKET1, 99772, true);
		player->EquipNewItem(EQUIPMENT_SLOT_TRINKET2, 99777, true);
		player->EquipNewItem(EQUIPMENT_SLOT_BACK, 99774, true);
		player->EquipNewItem(EQUIPMENT_SLOT_MAINHAND, 99769, true);
		player->EquipNewItem(EQUIPMENT_SLOT_FEET, 99904, true);
		player->EquipNewItem(EQUIPMENT_SLOT_OFFHAND, 99769, true);
		break;
	}
	player->CLOSE_GOSSIP_MENU();
	return true;
}
};


void AddSC_npc_template()
{
	new npc_template();
}