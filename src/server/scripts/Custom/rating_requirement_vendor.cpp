#include "ScriptPCH.h"
#include "Chat.h"
#include "WorldSession.h"
#include "AccountMgr.h"
#include "Config.h"

typedef std::set<uint32> DisabledPlrs;

class Rating_Vendor : public CreatureScript
{
private:
	DisabledPlrs m_plrList;
	DisabledPlrs m_charList;
public:
	Rating_Vendor() : CreatureScript("Rating_Vendor")
	{

	}

	struct Rating_VendorAI : public ScriptedAI
	{
		Rating_VendorAI(Creature *c) : ScriptedAI(c)
		{
		}
	};

	CreatureAI* GetAI(Creature* _creature) const
	{
		return new Rating_VendorAI(_creature);
	}

	bool OnGossipHello(Player* player, Creature* creature)
	{
		player->PlayerTalkClass->ClearMenus();
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Would you like to purchase some items?", GOSSIP_SENDER_MAIN, 1);
		player->PlayerTalkClass->SendGossipMenu(creature->GetEntry(), creature->GetGUID());

		return true;
	}


	void SendVendor(Player* player, uint64 creatureGuid, uint32 vendorid)
	{
		if (!vendorid)
			return;

		player->GetSession()->SendListInventory(creatureGuid);
	}


	bool OnGossipSelect(Player* player, Creature* creature, uint32 /*sender*/, uint32 action)
	{

		switch (action)
		{
		case 1:
			if (player->GetSession()->GetSecurity() >= 1)
			{
				player->PlayerTalkClass->ClearMenus();
				player->GetSession()->SendListInventory(creature->GetGUID());
			}
			else
				ChatHandler(player->GetSession()).PSendSysMessage("|cffff6060[Information]:|r If you would like access, please visit project-destiny.net!|r!");
			break;
		}

		return true;
	}

};

void AddSC_Rating_Vendor()
{
	new Rating_Vendor();
}