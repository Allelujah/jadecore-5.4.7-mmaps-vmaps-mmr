// Courtesy of Dogmar.

#include "ScriptPCH.h"
#include "ScriptedCreature.h"
#include "ScriptedGossip.h"
#include "Player.h"
 
class tele : public CreatureScript
{
public: tele() : CreatureScript("tele") {}

		bool OnGossipHello(Player* player, Creature* creature)
		{
			if (player->isInCombat())
			{
				player->GetSession()->SendNotification("|cffFFFF00MTG - Teleporter \n |cffFFFFFFYou are in combat!");
				player->PlayerTalkClass->SendCloseGossip();
				return false;
			}
			
			player->ADD_GOSSIP_ITEM(1, "|TInterface\\icons\\Inv_shirt_01:30:30:-15:0|tMall: Shirt Mall", GOSSIP_SENDER_MAIN, 1000);
			player->ADD_GOSSIP_ITEM(1, "|TInterface\\icons\\Achievement_cloudnine:30:30:-15:0|tMall: Transmog", GOSSIP_SENDER_MAIN, 1001);
			player->ADD_GOSSIP_ITEM(1, "|TInterface\\icons\\Ability_hunter_markedfordeath:30:30:-15:0|tMall: 1v1 Arena Queue", GOSSIP_SENDER_MAIN, 1002);
			player->ADD_GOSSIP_ITEM(1, "|TInterface\\icons\\Achievement_arena_3v3_7:30:30:-15:0|tPVP Rewards", GOSSIP_SENDER_MAIN, 1003);
			player->ADD_GOSSIP_ITEM(1, "|TInterface\\icons\\Achievement_bg_topdps:30:30:-15:0|tMall: Dummys", GOSSIP_SENDER_MAIN, 1004);
			player->ADD_GOSSIP_ITEM(1, "|TInterface\\icons\\Item_spellcloththread:30:30:-15:0|tMall: Professions", GOSSIP_SENDER_MAIN, 1005);
			player->ADD_GOSSIP_ITEM(1, "|TInterface\\icons\\Ability_mount_jungletiger:30:30:-15:0|tMall: Mounts", GOSSIP_SENDER_MAIN, 1006);
			player->ADD_GOSSIP_ITEM(1, "|TInterface\\icons\\Achievement_arena_3v3_7:30:30:-15:0|tGurubashi Arena", GOSSIP_SENDER_MAIN, 1007);

			player->SEND_GOSSIP_MENU(777777, creature->GetGUID());
			return true;
		}

		bool OnGossipSelect(Player* player, Creature* creature, uint32 /*uiSender*/, uint32 action)
		{
			player->CLOSE_GOSSIP_MENU();
			switch (action)
			{
				case 1000:
					player->TeleportTo(1, 1962.427f, -4776.459f, 38.769f, 4.618f); // MAP, X, Y, Z, O
					break;
				case 1001:
					player->TeleportTo(870, 835.545f, 232.445f, 504.429f, 5.283f);
					break;
				case 1002:
					player->TeleportTo(870, 802.900f, 277.294f, 503.630f, 5.305f);
					break;
				case 1003:
					player->TeleportTo(870, 842.117f, -175.157f, 413.631f, 2.654f);
					break;
				case 1004:
					player->TeleportTo(870, 892.060f, 232.467f, 503.120f, 5.262f);
					break;
				case 1005:
					player->TeleportTo(870, 822.516f, 328.242f, 503.119f, 2.575f);
					break;
				case 1006:
					player->TeleportTo(870, 923.461f, 333.401f, 506.094f, 0.985f);
					break;
				case 1007:
					player->TeleportTo(0, -13228.622070f, 227.733826f, 32.845123f, 1.106999f);
			}
			return true;
		}
};

void AddSC_tele()
{
        new tele();
}