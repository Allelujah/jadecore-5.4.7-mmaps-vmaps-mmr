#include "ScriptPCH.h"
 
struct Rochet2
{
  uint8 icon;
	std::string name;
	uint32 HK, titleID;
};
 
//	{icon, "title name", honored_kills, titleID}
Rochet2 Titles [] =
{
	{9,		"|TInterface/PvPRankBadges/PvPRank01:30:30:-15:0|tPrivate",					50,			1	},
	{9,		"|TInterface/PvPRankBadges/PvPRank02:30:30:-15:0|tCorporal",					100,		2	},
	{9,		"|TInterface/PvPRankBadges/PvPRank03:30:30:-15:0|tSergeant",					150,		3	},
	{9,		"|TInterface/PvPRankBadges/PvPRank04:30:30:-15:0|tMaster Sergeant",			200,		4	},
	{9,		"|TInterface/PvPRankBadges/PvPRank05:30:30:-15:0|tSergeant Major",			250,		5	},
	{9,		"|TInterface/PvPRankBadges/PvPRank06:30:30:-15:0|tKnight",					300,		6	},
	{9,		"|TInterface/PvPRankBadges/PvPRank07:30:30:-15:0|tKnight-Lieutenant",		350,		7	},
	{9,		"|TInterface/PvPRankBadges/PvPRank08:30:30:-15:0|tKnight-Captain",			400,		8	},
	{9,		"|TInterface/PvPRankBadges/PvPRank09:30:30:-15:0|tKnight-Champion",			450,		9	},
	{9,		"|TInterface/PvPRankBadges/PvPRank10:30:30:-15:0|tLieutenant Commander",		500,		10	},
	{9,		"|TInterface/PvPRankBadges/PvPRank11:30:30:-15:0|tCommander",				550,		11	},
	{9,		"|TInterface/PvPRankBadges/PvPRank12:30:30:-15:0|tMarshal",					600,		12	},
	{9,		"|TInterface/PvPRankBadges/PvPRank13:30:30:-15:0|tField Marshal",			650,		13	},
	{9,		"|TInterface/PvPRankBadges/PvPRank14:30:30:-15:0|tGrand Marshal",			700,		14	},
	{9,		"|TInterface/PvPRankBadges/PvPRank14:30:30:-15:0|tOf The Alliance",   		750,	    126 },
	{9, 	"|TInterface/PvPRankBadges/PvPRank14:30:30:-15:0|tThe Bloodthirsty",			800,		255 },
 
	{9,		"|TInterface/PvPRankBadges/PvPRank01:30:30:-15:0|tScout",					50,			1	},
	{9,		"|TInterface/PvPRankBadges/PvPRank02:30:30:-15:0|tGrunt",						100,		2	},
	{9,		"|TInterface/PvPRankBadges/PvPRank03:30:30:-15:0|tSergeant",					150,		3	},
	{9,		"|TInterface/PvPRankBadges/PvPRank04:30:30:-15:0|tSenior Sergeant",			200,		4	},
	{9,		"|TInterface/PvPRankBadges/PvPRank05:30:30:-15:0|tFirst Sergeant",			250,		5	},
	{9,		"|TInterface/PvPRankBadges/PvPRank06:30:30:-15:0|tStone Guard",				300,		6	},
	{9,		"|TInterface/PvPRankBadges/PvPRank07:30:30:-15:0|tBlood Guard",				350,		7	},
	{9,		"|TInterface/PvPRankBadges/PvPRank08:30:30:-15:0|tLegionnaire",				400,		8	},
	{9,		"|TInterface/PvPRankBadges/PvPRank09:30:30:-15:0|tCenturion",				450,		9	},
	{9,		"|TInterface/PvPRankBadges/PvPRank10:30:30:-15:0|tChampion",					500,		10	},
	{9,		"|TInterface/PvPRankBadges/PvPRank11:30:30:-15:0|tLieutenant General",		550,		11	},
	{9,		"|TInterface/PvPRankBadges/PvPRank12:30:30:-15:0|tGeneral",					600,		12	},
	{9,		"|TInterface/PvPRankBadges/PvPRank13:30:30:-15:0|tWarlord",					650,		13	},
	{9,		"|TInterface/PvPRankBadges/PvPRank14:30:30:-15:0|tHigh Warlord",			700,		14	},
	{9, 	"|TInterface/PvPRankBadges/PvPRank14:30:30:-15:0|tOf The Horde",			750,	    126 },
	{9,     "|TInterface/PvPRankBadges/PvPRank14:30:30:-15:0|tThe Bloodthirsty",			800,		255 },
};
 
enum eEnums
{
	Amount	=	sizeof Titles/sizeof(*Titles),
 
	// npc_text ID
	Greetings_A	=	1,
	Greetings_H	=	2,
};
 
#define ERROR_HASTITLE	"|cffff0000You already have this title|r" // Error message that shows up when a player already has the title
#define ERROR_CASH "|cffff0000You don't have enough honorable kills|r"
 
 
class Title_gossip_codebox : public CreatureScript
{
	public:
	Title_gossip_codebox()
	: CreatureScript("Title_gossip_codebox")
	{
	}
 
	bool OnGossipHello(Player* pPlayer, Creature* pCreature)
	{
		uint32 txt	= Greetings_A;
		uint32 i	= 0;
		uint32 m	= Amount/2;
		if(pPlayer->GetTeam() == HORDE)
		{
			txt = Greetings_H;
			i = Amount/2;
			m = Amount;
		}
		for (i; i<m; i++)
		{
			std::ostringstream ss;
			ss << Titles[i].name << " - " << Titles[i].HK << " Honorable Kills";
			std::string showcoolshit = ss.str();
			ss.clear();
			ss << "Are you back?\nYou will be granted the title: " << Titles[i].name;
			std::string showcoolshit2 = ss.str();
			// ADD_GOSSIP_ITEM_EXTENDED Parameters: (icon, label, GOSSIP_SENDER_MAIN (Sender), Title ID ? (action), popup, coppercost, code (false))
			pPlayer->ADD_GOSSIP_ITEM_EXTENDED(Titles[i].icon, showcoolshit.c_str(), GOSSIP_SENDER_MAIN, i, showcoolshit2, 0, false);
		}
		pPlayer->PlayerTalkClass->SendGossipMenu(txt, pCreature->GetGUID());
		return true;
	}
 
	bool OnGossipSelect(Player* pPlayer, Creature* pCreature, uint32 /*uiSender*/, uint32 i)
	{
		pPlayer->PlayerTalkClass->ClearMenus(); // clear the menu
 
		if (CharTitlesEntry const* Title = sCharTitlesStore.LookupEntry(Titles[i].titleID)) // Get title
		{
			if(pPlayer->HasTitle(Title)) // If has title
				pPlayer->GetSession()->SendAreaTriggerMessage(ERROR_HASTITLE);
			else if(Titles[i].HK > pPlayer->GetUInt32Value(PLAYER_FIELD_LIFETIME_HONORABLE_KILLS)) // If doesnt have enough honored kills
				pPlayer->GetSession()->SendAreaTriggerMessage(ERROR_CASH);
			else
			{
				pPlayer->SetTitle(Title);
				pPlayer->SaveToDB();
			}
			OnGossipHello(pPlayer, pCreature);
		}
		return true;
	}
};
 
void AddSC_Title_gossip_codebox()
{
    new Title_gossip_codebox();
}