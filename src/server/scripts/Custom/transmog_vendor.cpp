/*INSERT INTO `creature_template` (`entry`, `difficulty_entry_1`, `difficulty_entry_2`, `difficulty_entry_3`, `difficulty_entry_4`, `difficulty_entry_5`, `difficulty_entry_6`, `difficulty_entry_7`, `difficulty_entry_8`, `difficulty_entry_9`, `difficulty_entry_10`, `difficulty_entry_11`, `difficulty_entry_12`, `difficulty_entry_13`, `difficulty_entry_14`, `difficulty_entry_15`, `KillCredit1`, `KillCredit2`, `modelid1`, `modelid2`, `modelid3`, `modelid4`, `name`, `subname`, `IconName`, `gossip_menu_id`, `minlevel`, `maxlevel`, `exp`, `exp_unk`, `faction_A`, `faction_H`, `npcflag`, `npcflag2`, `speed_walk`, `speed_run`, `speed_fly`, `scale`, `rank`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `rangeattacktime`, `unit_class`, `unit_flags`, `unit_flags2`, `dynamicflags`, `family`, `trainer_type`, `trainer_spell`, `trainer_class`, `trainer_race`, `minrangedmg`, `maxrangedmg`, `rangedattackpower`, `type`, `type_flags`, `type_flags2`, `lootid`, `pickpocketloot`, `skinloot`, `resistance1`, `resistance2`, `resistance3`, `resistance4`, `resistance5`, `resistance6`, `spell1`, `spell2`, `spell3`, `spell4`, `spell5`, `spell6`, `spell7`, `spell8`, `PetSpellDataId`, `VehicleId`, `mingold`, `maxgold`, `AIName`, `MovementType`, `InhabitType`, `HoverHeight`, `Health_mod`, `Mana_mod`, `Mana_mod_extra`, `Armor_mod`, `RacialLeader`, `questItem1`, `questItem2`, `questItem3`, `questItem4`, `questItem5`, `questItem6`, `movementId`, `RegenHealth`, `equipment_id`, `mechanic_immune_mask`, `flags_extra`, `ScriptName`, `WDBVerified`) VALUES (555002, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 24978, 0, 0, 0, 'Transmogrification Vendor', 'Dark-Portal', NULL, 0, 90, 90, 0, 0, 35, 35, 1, 0, 1, 1.14286, 1.14286, 1, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 2048, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 3, 1, 12, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 'npc_transmogvendor', 1);*/
#include "ScriptPCH.h"
#include "AccountMgr.h"
#include "Config.h"

enum Options
{
    MAIN_MENU,
    CHOOSE_ITEM,
	CHOOSE_WEAPON = 5000,
	CHOOSE_OFFHAND_WEAPON = 5001,
	CHOOSE_OFFHAND = 5002,
	CHOOSE_SHIELD = 5003,
	CHOOSE_RANGED = 5004,
	CHOOSE_HELM = 5005,
	CHOOSE_SHOULDER = 5006,
	CHOOSE_CLOAK = 5007,
	CHOOSE_CHEST = 5008,
	CHOOSE_LEGS = 5009,
	CHOOSE_BELT = 5010,
	CHOOSE_WRIST = 5011,
	CHOOSE_GLOVES = 5012,
	CHOOSE_BOOTS = 5013,
	CHOOSE_TABARD = 5014,
    PURCHASE_ITEM,
    GOODBYE,
    MAIN_MENU_TEXT = 13371900 // Send a friendly gossip menu text
};

bool TransmogItemisValid(Player* player, Creature* creature, const char* code, Item* item) {
	//Item * item;
	ItemTemplate const* itemCheck = new ItemTemplate;

	ChatHandler handler(player->GetSession());

	uint32 entry = atoi(code);
	uint32 allowedRace = itemCheck->AllowableRace;
	uint32 allowedClass = itemCheck->AllowableClass;

	itemCheck = sObjectMgr->GetItemTemplate(entry);
	//item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND);

	if (!entry)
	{
		creature->MonsterWhisper("|cff00FF7FYou have entered an invalid item entry. Please try again with a valid entry.", player->GetGUID(), true);
		return false;
	}


	if (!itemCheck)
	{
		handler.SendSysMessage("|cff00FF7FAn item with the specified entry does not exist. Please try again with a valid entry.");
		return false;
	}

	if (!item)
	{
		creature->MonsterWhisper("|cff00FF7FYou must equip a weapon first!", player->GetGUID(), true);
		return false;
	}

	/*if (itemCheck->CanBeTransmogrified(itemCheck)) {
		handler.SendSysMessage("|cff00FF7FThe item id you've entered can not be transmogrified.");
		return false;
	}*/

	if (item != player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_TABARD)) {
		if (!itemCheck->CanTransmogrifyItemWithItem(itemCheck, item->GetTemplate())) {
			handler.SendSysMessage("|cff00FF7FThe item id you've entered can not be transmogrified to the selected slot.");
			return false;
		}
	}

	/*if (allowedRace = player->getRace()) { //Absolutely no idea how this works
		handler.SendSysMessage("|cff00FF7FThe item id you've entered is not compatible with your current race.");
		return false;
	}

	if (allowedClass = player->getClass()) {
		handler.SendSysMessage("|cff00FF7FThe item id you've entered is not compatible with your current class.");
		return false;
	}*/

	return true;
}

typedef std::set<uint32> DisabledItems;

class npc_transmogvendor : public CreatureScript
{
private:
    uint32 m_arenaCoins;
    DisabledItems m_disabledItems;

public:
    npc_transmogvendor() : CreatureScript("npc_transmogvendor")
    {
    	
    }

	bool OnGossipHello(Player* player, Creature* creature)
    {
        player->PlayerTalkClass->ClearMenus();
        player->PlayerTalkClass->SendCloseGossip();

		player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_axe_2h_cataclysm_c_01.png:28:28:-16|t[-Transmog Weapon-]", GOSSIP_SENDER_MAIN, 2);
		player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_helmet_142.png:28:28:-16|t[-Transmog Armor-]", GOSSIP_SENDER_MAIN, 3);
		player->PlayerTalkClass->SendGossipMenu(100001, creature->GetGUID());
        return true;
    }

    bool OnGossipSelect(Player* player, Creature* creature, uint32 uiSender, uint32 uiAction)
    {
        player->PlayerTalkClass->ClearMenus();
        player->PlayerTalkClass->SendCloseGossip();

        if (!uiAction)
            return OnGossipHello(player, creature);

		switch (uiAction)
		{
		case 1: //<-Back menu
		{
					  player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_axe_2h_cataclysm_c_01.png:28:28:-16|t[-Transmog Weapon-]", GOSSIP_SENDER_MAIN, 2);
					  player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/inv_helmet_142.png:28:28:-16|t[-Transmog Armor-]", GOSSIP_SENDER_MAIN, 3);

					  //player->ADD_GOSSIP_ITEM(1, "|TInterface/ICONS/ability_iyyokuk_sword_red.png:28:28:-16|t[-I wish to remove my transmog-]", GOSSIP_SENDER_MAIN, 4);
					  player->PlayerTalkClass->SendGossipMenu(100001, creature->GetGUID());
					  return true;
		}
			break;

		case 2: {
					player->ADD_GOSSIP_ITEM_EXTENDED(1, "|TInterface/ICONS/inv_axe_2h_cataclysm_c_01.png:28:28:-16|t[-Transmog Weapon-]", 0, CHOOSE_WEAPON, "", 0, true);
					player->ADD_GOSSIP_ITEM_EXTENDED(1, "|TInterface/ICONS/inv_knife_1h_bwdraid_d_01.png:28:28:-16|t[-Transmog Off-Hand Weapon-]", 0, CHOOSE_OFFHAND_WEAPON, "", 0, true);
					player->ADD_GOSSIP_ITEM_EXTENDED(1, "|TInterface/ICONS/inv_shield_55.png:28:28:-16|t[-Transmog Off-Hand]", 0, CHOOSE_OFFHAND, "", 0, true);
					player->ADD_GOSSIP_ITEM_EXTENDED(1, "|TInterface/ICONS/inv_shield_pvppandarias3_c_01.png:28:28:-16|t[-Transmog Shield]", 0, CHOOSE_SHIELD, "", 0, true);
					player->ADD_GOSSIP_ITEM_EXTENDED(1, "|TInterface/ICONS/inv_weapon_crossbow_27.png:28:28:-16|t[-Transmog Ranged Weapon]", 0, CHOOSE_RANGED, "", 0, true);

					player->ADD_GOSSIP_ITEM(1, "<-Back", GOSSIP_SENDER_MAIN, 1);
					player->PlayerTalkClass->SendGossipMenu(100002, creature->GetGUID());
					break;
		}

		case 3: {
					player->ADD_GOSSIP_ITEM_EXTENDED(1, "|TInterface/ICONS/inv_helmet_142.png:28:28:-16|t[-Transmog Helmet-]", 0, CHOOSE_HELM, "", 0, true);
					player->ADD_GOSSIP_ITEM_EXTENDED(1, "|TInterface/ICONS/inv_shoulder_129.png:28:28:-16|t[-Transmog Shoulders-]", 0, CHOOSE_SHOULDER, "", 0, true);
					player->ADD_GOSSIP_ITEM_EXTENDED(1, "|TInterface/ICONS/inv_cape_pandaria_dragoncaster_d_02.png:28:28:-16|t[-Transmog Cloak-]", 0, CHOOSE_CLOAK, "", 0, true);
					player->ADD_GOSSIP_ITEM_EXTENDED(1, "|TInterface/ICONS/inv_chest_cloth_raidwarlock_l_01.png:28:28:-16|t[-Transmog Chest-]", 0, CHOOSE_CHEST, "", 0, true);
					player->ADD_GOSSIP_ITEM_EXTENDED(1, "|TInterface/ICONS/inv_tabard_a_01pvptabard_s14.png:28:28:-16|t[-Transmog Tabard-]", 0, CHOOSE_TABARD, "", 0, true);
					player->ADD_GOSSIP_ITEM_EXTENDED(1, "|TInterface/ICONS/inv_pant_leather_pvpmonk_g_01.png:28:28:-16|t[-Transmog Legs]", 0, CHOOSE_LEGS, "", 0, true);
					player->ADD_GOSSIP_ITEM_EXTENDED(1, "|TInterface/ICONS/inv_belt_62.png:28:28:-16|t[-Transmog Belt]", 0, CHOOSE_BELT, "", 0, true);
					player->ADD_GOSSIP_ITEM_EXTENDED(1, "|TInterface/ICONS/inv_bracer_43.png:28:28:-16|t[-Transmog Wrist-]", 0, CHOOSE_WRIST, "", 0, true);
					player->ADD_GOSSIP_ITEM_EXTENDED(1, "|TInterface/ICONS/inv_gloves_cloth_panda_b_01_green.png:28:28:-16|t[-Transmog Gloves-]", 0, CHOOSE_GLOVES, "", 0, true);
					player->ADD_GOSSIP_ITEM_EXTENDED(1, "|TInterface/ICONS/inv_boots_cloth_29.png:28:28:-16|t[-Transmog Boots-]", 0, CHOOSE_BOOTS, "", 0, true);

					player->ADD_GOSSIP_ITEM(1, "<-Back", GOSSIP_SENDER_MAIN, 1);
					player->PlayerTalkClass->SendGossipMenu(100002, creature->GetGUID());
					break;
		}

		default:
			break;
		}
		return true;
    }

    bool OnGossipSelectCode(Player* player, Creature* creature, uint32 uiSender, uint32 uiAction, const char* code)
    {
        player->PlayerTalkClass->ClearMenus();
        player->PlayerTalkClass->SendCloseGossip();

		Item * item;

        ChatHandler handler(player->GetSession());

        if(!uiAction)
            return OnGossipHello(player, creature);

        switch (uiAction)
        {

	    //Weapon transmog
		case CHOOSE_WEAPON: {
		item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND);
		uint32 entry = atoi(code);
		if (TransmogItemisValid(player, creature, code, item)) {
			if (item->GetTemplate()->InventoryType == INVTYPE_WEAPON || item->GetTemplate()->InventoryType == INVTYPE_2HWEAPON)
			{
				player->CustomTransmog(player, entry, EQUIPMENT_SLOT_MAINHAND);
				player->GetSession()->SendNotification("|cff800080%s |cffFF0000succesfully transmogrified!", item->GetTemplate()->Name1.c_str());
				player->PlayerTalkClass->SendCloseGossip();
			}
			else {
				creature->MonsterWhisper("|cff00FF7FYou do not have a main-hand weapon equip!", player->GetGUID(), true);	
			}
		}
		}
		break;

		case CHOOSE_OFFHAND_WEAPON: {
		item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_OFFHAND);
		uint32 entry = atoi(code);
		if (TransmogItemisValid(player, creature, code, item)) {
			if (item->GetTemplate()->InventoryType == INVTYPE_WEAPON || item->GetTemplate()->InventoryType == INVTYPE_2HWEAPON)
			{
				player->CustomTransmog(player, entry, EQUIPMENT_SLOT_OFFHAND);
				player->GetSession()->SendNotification("|cff800080%s |cffFF0000succesfully transmogrified!", item->GetTemplate()->Name1.c_str());
				player->PlayerTalkClass->SendCloseGossip();
			}
			else {
				creature->MonsterWhisper("|cff00FF7FYou do not have a off-hand weapon equip!", player->GetGUID(), true);
			}
		}
		}
		break;

		case CHOOSE_OFFHAND: {
		item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_OFFHAND);
		uint32 entry = atoi(code);
		if (TransmogItemisValid(player, creature, code, item)) {
			if (item->GetTemplate()->InventoryType == INVTYPE_HOLDABLE)
			{
				player->CustomTransmog(player, entry, EQUIPMENT_SLOT_OFFHAND);
				player->GetSession()->SendNotification("|cff800080%s |cffFF0000succesfully transmogrified!", item->GetTemplate()->Name1.c_str());
				player->PlayerTalkClass->SendCloseGossip();
			}
			else {
				creature->MonsterWhisper("|cff00FF7FYou do not have a off-hand weapon equip!", player->GetGUID(), true);
			}
		}
		}
		break;

		case CHOOSE_SHIELD: {
		item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_OFFHAND);
		uint32 entry = atoi(code);
		if (TransmogItemisValid(player, creature, code, item)) {
			 if (item->GetTemplate()->InventoryType == INVTYPE_SHIELD)
			 {
				 player->CustomTransmog(player, entry, EQUIPMENT_SLOT_OFFHAND);
				 player->GetSession()->SendNotification("|cff800080%s |cffFF0000succesfully transmogrified!", item->GetTemplate()->Name1.c_str());
				 player->PlayerTalkClass->SendCloseGossip();
			 }
			 else {
				 creature->MonsterWhisper("|cff00FF7FYou do not have a shield equip!", player->GetGUID(), true);
			 }
		}
		}
		break;

		case CHOOSE_RANGED: {
		item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_MAINHAND);
		uint32 entry = atoi(code);
		if (TransmogItemisValid(player, creature, code, item)) {
			if (item->GetTemplate()->InventoryType == INVTYPE_RANGED)
			{
				player->CustomTransmog(player, entry, EQUIPMENT_SLOT_MAINHAND);
				player->GetSession()->SendNotification("|cff800080%s |cffFF0000succesfully transmogrified!", item->GetTemplate()->Name1.c_str());
				player->PlayerTalkClass->SendCloseGossip();
			}
			else {
				creature->MonsterWhisper("|cff00FF7FYou do not have a ranged weapon equip!", player->GetGUID(), true);
			}
		}
		}
		break;

		//Armor transmog
		case CHOOSE_HELM: {
		item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_HEAD);
		uint32 entry = atoi(code);
		if (TransmogItemisValid(player, creature, code, item)) {
			if (item->GetTemplate()->InventoryType == INVTYPE_HEAD)
			{
				player->CustomTransmog(player, entry, EQUIPMENT_SLOT_HEAD);
				player->GetSession()->SendNotification("|cff800080%s |cffFF0000succesfully transmogrified!", item->GetTemplate()->Name1.c_str());
				player->PlayerTalkClass->SendCloseGossip();
			}
			else {
				creature->MonsterWhisper("|cff00FF7FYou do not have a helmet equip!", player->GetGUID(), true);
			}
		}
		}
		break;

		case CHOOSE_SHOULDER: {
		 item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_SHOULDERS);
		 uint32 entry = atoi(code);
		 if (TransmogItemisValid(player, creature, code, item)) {
			  if (item->GetTemplate()->InventoryType == INVTYPE_SHOULDERS)
			  {
				  player->CustomTransmog(player, entry, EQUIPMENT_SLOT_SHOULDERS);
				  player->GetSession()->SendNotification("|cff800080%s |cffFF0000succesfully transmogrified!", item->GetTemplate()->Name1.c_str());
				  player->PlayerTalkClass->SendCloseGossip();
			  }
			  else {
				  creature->MonsterWhisper("|cff00FF7FYou do not have shoulders equip!", player->GetGUID(), true);
			  }
		 }
		}
		break;

		case CHOOSE_CLOAK: {
		 item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_BACK);
		 uint32 entry = atoi(code);
		 if (TransmogItemisValid(player, creature, code, item)) {
			  if (item->GetTemplate()->InventoryType == INVTYPE_CLOAK)
			  {
				  player->CustomTransmog(player, entry, EQUIPMENT_SLOT_BACK);
				  player->GetSession()->SendNotification("|cff800080%s |cffFF0000succesfully transmogrified!", item->GetTemplate()->Name1.c_str());
				  player->PlayerTalkClass->SendCloseGossip();
			  }
			  else {
				  creature->MonsterWhisper("|cff00FF7FYou do not have a cloak equip!", player->GetGUID(), true);
			  }
		 }
		}
		break;

		case CHOOSE_CHEST: {
		item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_CHEST);
		uint32 entry = atoi(code);
		if (TransmogItemisValid(player, creature, code, item)) {
			if (item->GetTemplate()->InventoryType == INVTYPE_CHEST || item->GetTemplate()->InventoryType == INVTYPE_ROBE)
		   {
			   player->CustomTransmog(player, entry, EQUIPMENT_SLOT_CHEST);
			   player->GetSession()->SendNotification("|cff800080%s |cffFF0000succesfully transmogrified!", item->GetTemplate()->Name1.c_str());
			   player->PlayerTalkClass->SendCloseGossip();
		   }
		   else {
			   creature->MonsterWhisper("|cff00FF7FYou do not have a chest equip!", player->GetGUID(), true);
		   }
		}
		}
		break;

		case CHOOSE_TABARD: {
		item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_TABARD);
		uint32 entry = atoi(code);
		if (TransmogItemisValid(player, creature, code, item)) {
		   if (item->GetTemplate()->InventoryType == INVTYPE_TABARD)
		   {
			   player->CustomTransmog(player, entry, EQUIPMENT_SLOT_TABARD);
			   player->GetSession()->SendNotification("|cff800080%s |cffFF0000succesfully transmogrified!", item->GetTemplate()->Name1.c_str());
			   player->PlayerTalkClass->SendCloseGossip();
		   }
		   else {
			   creature->MonsterWhisper("|cff00FF7FYou do not have a tabard equip!", player->GetGUID(), true);
		   }
		}
		}
		break;

		case CHOOSE_LEGS: {
		item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_LEGS);
		uint32 entry = atoi(code);
		if (TransmogItemisValid(player, creature, code, item)) {
		   if (item->GetTemplate()->InventoryType == INVTYPE_LEGS)
		   {
			   player->CustomTransmog(player, entry, EQUIPMENT_SLOT_LEGS);
			   player->GetSession()->SendNotification("|cff800080%s |cffFF0000succesfully transmogrified!", item->GetTemplate()->Name1.c_str());
			   player->PlayerTalkClass->SendCloseGossip();
		   }
		   else {
			   creature->MonsterWhisper("|cff00FF7FYou do not have legs equip!", player->GetGUID(), true);
		   }
		}
		}
		break;

		case CHOOSE_BELT: {
		item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_WAIST);
		uint32 entry = atoi(code);
		if (TransmogItemisValid(player, creature, code, item)) {
		  if (item->GetTemplate()->InventoryType == INVTYPE_WAIST)
		  {
			  player->CustomTransmog(player, entry, EQUIPMENT_SLOT_WAIST);
			  player->GetSession()->SendNotification("|cff800080%s |cffFF0000succesfully transmogrified!", item->GetTemplate()->Name1.c_str());
			  player->PlayerTalkClass->SendCloseGossip();
		  }
		  else {
			  creature->MonsterWhisper("|cff00FF7FYou do not have a belt equip!", player->GetGUID(), true);
		  }
		}
		}
		break;

		case CHOOSE_WRIST: {
		item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_WRISTS);
		uint32 entry = atoi(code);
		if (TransmogItemisValid(player, creature, code, item)) {
		  if (item->GetTemplate()->InventoryType == INVTYPE_WRISTS)
		  {
			  player->CustomTransmog(player, entry, EQUIPMENT_SLOT_WRISTS);
			  player->GetSession()->SendNotification("|cff800080%s |cffFF0000succesfully transmogrified!", item->GetTemplate()->Name1.c_str());
			  player->PlayerTalkClass->SendCloseGossip();
		  }
		  else {
			  creature->MonsterWhisper("|cff00FF7FYou do not have wrists equip!", player->GetGUID(), true);
		  }
		}
		}
		break;

		case CHOOSE_GLOVES: {
		item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_HANDS);
		uint32 entry = atoi(code);
		if (TransmogItemisValid(player, creature, code, item)) {
		   if (item->GetTemplate()->InventoryType == INVTYPE_HANDS)
		   {
			   player->CustomTransmog(player, entry, EQUIPMENT_SLOT_HANDS);
			   player->GetSession()->SendNotification("|cff800080%s |cffFF0000succesfully transmogrified!", item->GetTemplate()->Name1.c_str());
			   player->PlayerTalkClass->SendCloseGossip();
		   }
		   else {
			   creature->MonsterWhisper("|cff00FF7FYou do not have gloves equip!", player->GetGUID(), true);
		   }
		}
		}
		break;

		case CHOOSE_BOOTS: {
		item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_FEET);
		uint32 entry = atoi(code);
		if (TransmogItemisValid(player, creature, code, item)) {
			if (item->GetTemplate()->InventoryType == INVTYPE_FEET)
			{
				player->CustomTransmog(player, entry, EQUIPMENT_SLOT_FEET);
				player->GetSession()->SendNotification("|cff800080%s |cffFF0000succesfully transmogrified!", item->GetTemplate()->Name1.c_str());
				player->PlayerTalkClass->SendCloseGossip();
			}
			else {
				creature->MonsterWhisper("|cff00FF7FYou do not have boots equip!", player->GetGUID(), true);
			}
		}
		}
		break;

            default:
                break;
        }

        return OnGossipHello(player, creature);
    }
	
    /*void AddItemToPlayer(Player* player, uint32 itemId)
    {
        ChatHandler handler(player->GetSession());

        // Sanity checks just to handle any unexpected surprises
        if (!itemId)
        {
            handler.SendSysMessage("An unexpected error has occurred. Please try again later.");
            return;
        }

        ItemTemplate const* itemTemplate = sObjectMgr->GetItemTemplate(itemId);
        if (!itemTemplate)
        {
            handler.SendSysMessage("An unexpected error has occurred. Please try again later.");
            return;
        }
		
        ItemPosCountVec dest;
        InventoryResult msg = player->CanStoreNewItem(NULL_BAG, NULL_SLOT, dest, itemId, 1);
        if (msg == EQUIP_ERR_OK)
        {

            if (Item* item = player->StoreNewItem(dest, itemId, true, Item::GenerateItemRandomPropertyId(itemId)))
            {
                player->SendNewItem(item, 1, true, false);
                player->GetSession()->SendAreaTriggerMessage("You have successfully purchased\n%s.",
                                                             GetItemDesc(itemTemplate).c_str());
															 
                return;
            }
        }

        player->SendEquipError(msg, NULL, NULL);
        return;
    }

    std::string GetItemDesc(ItemTemplate const* itemTemplate, bool withQualityColor = true)
    {
        std::ostringstream os;
        os << "|Hitem:" << itemTemplate->ItemId << ":0:0:0:0:0:0:0:0:0|h";
        if (withQualityColor)
            os << "|c" << std::hex << ItemQualityColors[itemTemplate->Quality] << std::dec;
        else
            os  << "|cff363636";
        os << "[" << itemTemplate->Name1 << "]|r|h";

        return os.str();
    }

    bool IsValidItemForTransmog(ItemTemplate const* itemTemplate)
    {
        // Check disabled items first
        if (m_disabledItems.find(itemTemplate->ItemId) != m_disabledItems.end())
            return false;

        // Check item class/subclass
        switch (itemTemplate->Class)
        {
            case ITEM_CLASS_WEAPON:
            {
                switch (itemTemplate->SubClass)
                {
                    case ITEM_SUBCLASS_WEAPON_AXE:
                    case ITEM_SUBCLASS_WEAPON_AXE2:
                    case ITEM_SUBCLASS_WEAPON_BOW:
                    case ITEM_SUBCLASS_WEAPON_GUN:
                    case ITEM_SUBCLASS_WEAPON_MACE:
                    case ITEM_SUBCLASS_WEAPON_MACE2:
                    case ITEM_SUBCLASS_WEAPON_POLEARM:
                    case ITEM_SUBCLASS_WEAPON_SWORD:
                    case ITEM_SUBCLASS_WEAPON_SWORD2:
                    case ITEM_SUBCLASS_WEAPON_STAFF:
                    case ITEM_SUBCLASS_WEAPON_FIST_WEAPON:
                    case ITEM_SUBCLASS_WEAPON_DAGGER:
                    case ITEM_SUBCLASS_WEAPON_THROWN:
                    case ITEM_SUBCLASS_WEAPON_CROSSBOW:
                        break;
                    default:
                        return false;
                }
                break;
            }
            case ITEM_CLASS_ARMOR:
            {
                switch (itemTemplate->SubClass)
                {
                    case ITEM_SUBCLASS_ARMOR_CLOTH:
                    case ITEM_SUBCLASS_ARMOR_LEATHER:
                    case ITEM_SUBCLASS_ARMOR_MAIL:
                    case ITEM_SUBCLASS_ARMOR_PLATE:
                    case ITEM_SUBCLASS_ARMOR_SHIELD:
                        break;
                    default:
                        return false;
                }
                break;
            }
            default:
                return false;
        }

        // Check inventory type
        switch (itemTemplate->InventoryType)
        {
            case INVTYPE_HEAD:
            case INVTYPE_SHOULDERS:
            case INVTYPE_BODY:
            case INVTYPE_CHEST:
            case INVTYPE_ROBE:
            case INVTYPE_WAIST:
            case INVTYPE_LEGS:
            case INVTYPE_FEET:
            case INVTYPE_WRISTS:
            case INVTYPE_HANDS:
            case INVTYPE_CLOAK:
            case INVTYPE_WEAPON:
            case INVTYPE_SHIELD:
            case INVTYPE_RANGED:
            case INVTYPE_2HWEAPON:
            case INVTYPE_WEAPONMAINHAND:
            case INVTYPE_WEAPONOFFHAND:
            case INVTYPE_THROWN:
            case INVTYPE_RANGEDRIGHT:
                break;
            default:
                return false;
        }

        // Item quality check
        switch (itemTemplate->Quality)
        {
            case ITEM_QUALITY_POOR:
            case ITEM_QUALITY_NORMAL:
            case ITEM_QUALITY_UNCOMMON:
            case ITEM_QUALITY_RARE:
            case ITEM_QUALITY_EPIC:
            case ITEM_QUALITY_HEIRLOOM:
                break;
            default:
                return false;
        }

        // Item level check
        if (itemTemplate->ItemLevel > 521)
            return false;

        return true;
    }*/

};

void AddSC_NPC_TransmogVendor()
{
    new npc_transmogvendor;
}