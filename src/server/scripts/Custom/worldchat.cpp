//modified better accomodate common.h roles in the vip system - Dog
//modified for the serenity core (left a few cases in just in case, but commented out.)
#include "ScriptPCH.h"
#include "Chat.h"
 
class chat : public CommandScript
{
        public:
                chat() : CommandScript("chat"){}
 
        ChatCommand * GetCommands() const
        {
                static ChatCommand ChatCommandTable[] =
                {
                        {"world",        SEC_PLAYER,             true,           &HandleChatCommand,     "", NULL},
                        {NULL,          0,                              false,          NULL,                                           "", NULL}
                };
 
                return ChatCommandTable;
        }
 
        static bool HandleChatCommand(ChatHandler * handler, const char * args)
        {
                if (!args)
                        return false;
 
                std::string msg = "";
                Player * player = handler->GetSession()->GetPlayer();
 
                switch(player->GetSession()->GetSecurity())
                {
                        case SEC_PLAYER:
							   msg += "|cffFFC125[|cff00FF7FWorld|cffFFC125]";
                               msg += "|cffffffff[";
                               msg += player->GetName();
                               msg += "] |cFFA9A9A9";

                                break;
						
                        case SEC_VIP:
								if (player->GetTeam() == ALLIANCE)
								{
										msg += "|cffFFC125[|cff00FF7FWorld|cffFFC125]";
										msg += "|TInterface/PvPRankBadges/PvPRank14:20:20|t";
										msg += "|cff4169e1[VIP] |cffffffff[";
										msg += player->GetName();
										msg += "] |cFFA9A9A9";
								}
								else // I'm lazy
								{
										msg += "|cffFFC125[|cff00FF7FWorld|cffFFC125]";
										msg += "|TInterface/PvPRankBadges/PvPRank14:20:20|t";
										msg += "|cffff0000[VIP] |cffffffff[";
										msg += player->GetName();
										msg += "] |cFFA9A9A9";
								}
                                break;
						case SEC_EVENT_MASTER:
								msg += "|cffFFC125[|cff00FF7FWorld|cffFFC125]";
                                msg += "|cffFFC125[|cffffcc00Mod|cffFFC125] |cffffffff[";
                                msg += player->GetName();
                                msg += "] |cFF87CEEB";
                                break;
                        case SEC_MODERATOR:
								msg += "|cffFFC125[|cff00FF7FWorld|cffFFC125]";
								//msg += "|TINTERFACE/CHATFRAME/UI-CHATICON-BLIZZ:15:15:0:-1|t";
                                msg += "|cffFFC125[|cffffcc00Mod|cffFFC125] |cffffffff[";
                                msg += player->GetName();
                                msg += "] |cFF87CEEB";
                                break;
                        case SEC_TRIAL_GAMEMASTER:
								msg += "|cffFFC125[|cff00FF7FWorld|cffFFC125]";
								//msg += "|TINTERFACE/CHATFRAME/UI-CHATICON-BLIZZ:15:15:0:-1|t";
                                msg += "|cffFFC125[|cff00ccffGame Master|cffFFC125] |cffffffff[";
                                msg += player->GetName();
                                msg += "] |cFF87CEEB";
                                break;
                        case SEC_GAMEMASTER:
								msg += "|cffFFC125[|cff00FF7FWorld|cffFFC125]";
								//msg += "|TINTERFACE/CHATFRAME/UI-CHATICON-BLIZZ:15:15:0:-1|t";
                                msg += "|cffFFC125[|cff00ccffGame Master|cffFFC125] |cffffffff[";
                                msg += player->GetName();
                                msg += "] |cFF87CEEB";
                                break;
                        case SEC_HEAD_GAMEMASTER:
								msg += "|cffFFC125[|cff00FF7FWorld|cffFFC125]";
								//msg += "|TINTERFACE/CHATFRAME/UI-CHATICON-BLIZZ:15:15:0:-1|t";
                                msg += "|cffFFC125[|cff00ccffHead GM|cffFFC125] |cffffffff[";
                                msg += player->GetName();
                                msg += "] |cFF87CEEB";
                                break;
						/*case SEC_COMMUNITY_MANAGER:
							msg += "|cffFFC125[|cff00FF7FWorld|cffFFC125]";
							msg += "|TINTERFACE/CHATFRAME/UI-CHATICON-BLIZZ:15:15:0:-1|t";
							msg += "|cffFFC125[|cffff6060CM|cffFFC125] |cffffffff[";
							msg += player->GetName();
							msg += "] |cFF87CEEB";
							break;*/
                        case SEC_DEVELOPER:
								msg += "|cffFFC125[|cff00FF7FWorld|cffFFC125]";
								//msg += "|TINTERFACE/CHATFRAME/UI-CHATICON-BLIZZ:15:15:0:-1|t";
                                msg += "|cffFFC125[|cffDA70D6Developer|cffFFC125] |cffffffff[";
                                msg += player->GetName();
                                msg += "] |cFF87CEEB";
                                break;
                        case SEC_ADMINISTRATOR:
								msg += "|cffFFC125[|cff00FF7FWorld|cffFFC125]";
								//msg += "|TINTERFACE/CHATFRAME/UI-CHATICON-BLIZZ:15:15:0:-1|t";
                                msg += "|cffFFC125[Management|cffFFC125] |cffffffff[";
                                msg += player->GetName();
                                msg += "] |cFF87CEEB";
                                break;
						/*
                        case SEC_CO:
								msg += "|cffFFC125[|cff00FF7FWorld|cffFFC125]";
								msg += "|TINTERFACE/CHATFRAME/UI-CHATICON-BLIZZ:15:15:0:-1|t";
                                msg += "|cFF87CEEB[Executive] |cffffffff[";
                                msg += player->GetName();
                                msg += "] |cFF87CEEB";
                                break;
                        case SEC_O:
								msg += "|cffFFC125[|cff00FF7FWorld|cffFFC125]";
								msg += "|TINTERFACE/CHATFRAME/UI-CHATICON-BLIZZ:15:15:0:-1|t";
                                msg += "|cFF87CEEB[Owner] |cffffffff[";
                                msg += player->GetName();
                                msg += "] |cFF87CEEB";
                                break;
						*/
                        case SEC_CONSOLE:
                                msg += "|cffFFC125[ROOT|cffFFC125] |cffffffff[";
                                msg += player->GetName();
                                msg += "] |cffB400B4";
                                break;
 
                }
                       
                msg += args;
                sWorld->SendServerMessage(SERVER_MSG_STRING, msg.c_str(), 0);  
 
                return true;
        }
};
 
void AddSC_chat()
{
        new chat();
}